﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserRolesDAL
/// </summary>
public class UserRolesDAL
{
    private bmwusedcarsdbEntities1 dbContext = new bmwusedcarsdbEntities1();

    public UserRolesDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public bool AddItem(string roleName, int userId, string ChassisNO)
    {
        bool flag = false;

        try
        {
            var role = dbContext.Roles.FirstOrDefault(r => r.RoleName == roleName);
            if (role != null)
            {
                dbContext.UsersRoles.Add(new UsersRole { RoleId = role.RoleId, UserId = userId, ChassisNO = ChassisNO, AddDate = DateTime.UtcNow.AddHours(2) });
                this.dbContext.SaveChanges();
            }
            flag = true;
        }
        catch (Exception)
        {
        }
        return flag;
    }

    public List<UsersRole> GetAllItems(int maximumRows, int startRowIndex)
    {
        List<UsersRole> list = new List<UsersRole>();
        try
        {
            list = (from c in this.dbContext.UsersRoles.Include("Role").Include("User")
                    orderby c.Id
                    select c).Skip<UsersRole>(startRowIndex).Take(maximumRows).ToList();
        }
        catch (Exception)
        {
        }
        return list;
    }

    public int GetRecordsCount()
    {
        return this.dbContext.UsersRoles.Count();
    }


    public List<UsersRole> GetAllItems()
    {
        List<UsersRole> list = new List<UsersRole>();
        try
        {
            list = dbContext.UsersRoles.ToList();
        }
        catch (Exception)
        {
        }
        return list;
    }

}