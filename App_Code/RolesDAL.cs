﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RolesDAL
/// </summary>
public class RolesDAL
{
    private bmwusedcarsdbEntities1 dbContext = new bmwusedcarsdbEntities1();

    public RolesDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<Role> GetRolesAll()
    {
        List<Role> list = new List<Role>();
        try
        {
            list = this.dbContext.Roles.ToList();
        }
        catch (Exception)
        {
        }
        return list;
    }
}