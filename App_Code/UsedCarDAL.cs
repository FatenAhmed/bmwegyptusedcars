﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UsedCarsDAL
/// </summary>
public class UsedCarDAL
{
    private bmwusedcarsdbEntities1 dbContext = new bmwusedcarsdbEntities1();

    public UsedCarDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<UsedCar> Search(string modelText, string keywordsText, string series)
    {
        List<UsedCar> list = new List<UsedCar>();
        try
        {
            list = dbContext.UsedCars.ToList();
            if (!string.IsNullOrEmpty(modelText))
                list = list.Where(s => s.Model.ToLower().Contains(modelText.ToLower().Trim())).ToList();
            if (!string.IsNullOrEmpty(keywordsText))
                list = list.Where(s => s.Keywords.ToLower().Contains(keywordsText.ToLower().Trim())).ToList();
            if (!string.IsNullOrEmpty(series))
                list = list.Where(s => s.Series.ToLower().Contains(series.ToLower())).ToList();
        }
        catch (Exception)
        {
        }
        return list;
    }

    public bool DeleteItem(int Id)
    {
        bool success = false;

        UsedCar loadedObject = dbContext.UsedCars.First(x => x.Id == Id);

        try
        {
            dbContext.UsedCars.Remove(loadedObject);
            dbContext.SaveChanges();
            success = true;
        }
        catch (Exception)
        {

        }
        return success;
    }
}