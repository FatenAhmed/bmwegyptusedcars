﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Data.Common;
using System.Security.Cryptography;
using Microsoft.Practices.EnterpriseLibrary.Data;


/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    #region "Image Resizing Methods"

    public static string ResizeImage(string path, string filename, int lnWidth, int lnHeight, bool keepRatio, bool overwrite)
    {
        string newfilename = string.Empty;
        //    Bitmap srcbmp = new Bitmap(path + filename);
        Bitmap srcbmp = new Bitmap("D:/Icon Creations/MEK/resources/messi.jpg");
        if (srcbmp.Width > lnWidth || srcbmp.Height > lnHeight)
        {
            Bitmap bmpOut = null;

            decimal lnRatio;
            int lnNewWidth = 0;
            int lnNewHeight = 0;

            if (keepRatio)
            {
                if (srcbmp.Width > srcbmp.Height)
                {
                    lnRatio = (decimal)lnWidth / srcbmp.Width;
                    lnNewWidth = lnWidth;
                    decimal lnTemp = srcbmp.Height * lnRatio;
                    lnNewHeight = (int)lnTemp <= lnHeight ? (int)lnTemp : lnHeight;
                }
                else
                {
                    lnRatio = (decimal)lnHeight / srcbmp.Height;
                    lnNewHeight = lnHeight;
                    decimal lnTemp = srcbmp.Width * lnRatio;
                    lnNewWidth = (int)lnTemp <= lnWidth ? (int)lnTemp : lnWidth;
                }
            }
            else
            {
                lnNewWidth = lnWidth;
                lnNewHeight = lnHeight;
            }

            try
            {
                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(srcbmp, 0, 0, lnNewWidth, lnNewHeight);
                srcbmp.Dispose();

                EncoderParameters parameters = new EncoderParameters(1);
                System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Quality;
                parameters.Param[0] = new EncoderParameter(enc, 100L);
                ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/jpeg");

                if (!overwrite)
                {
                    newfilename = Path.GetFileNameWithoutExtension(path + filename) + "_lg.jpg";
                    bmpOut.Save(path + newfilename, imageCodecInfo, parameters);
                }
                else
                    bmpOut.Save(path + filename, imageCodecInfo, parameters);

                bmpOut.Dispose();
            }
            catch { }
        }

        srcbmp.Dispose();
        return newfilename;
    }

    public static string CreateThumbnail(string path, string filename, int width, int height)
    {
        Bitmap srcbmp = new Bitmap(path + filename);
        Image outimg = srcbmp.GetThumbnailImage(width, height, null, IntPtr.Zero);

        EncoderParameters parameters = new EncoderParameters(1);
        System.Drawing.Imaging.Encoder enc = System.Drawing.Imaging.Encoder.Quality;
        parameters.Param[0] = new EncoderParameter(enc, 100L);
        ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/jpeg");

        string thumbname = Path.GetFileNameWithoutExtension(path + filename) + "TH.jpg";
        //thumbname = SetUniqueFilename(path, thumbname);
        outimg.Save(path + thumbname, imageCodecInfo, parameters);

        srcbmp.Dispose();
        outimg.Dispose();

        return thumbname;
    }

    public static ImageCodecInfo GetEncoderInfo(String mimeType)
    {
        ImageCodecInfo Result = null;
        ImageCodecInfo[] Encoders = ImageCodecInfo.GetImageEncoders();
        for (int i = 0; Result == null && i < Encoders.Length; i++)
        {
            if (Encoders[i].MimeType == mimeType)
            {
                Result = Encoders[i];
            }
        }
        return Result;
    }

    #endregion

    public static string SetUniqueFilename(string path, string filename)
    {
        int sequence = 1;
        string newFilename = filename;

        while (File.Exists(path + newFilename))
        {
            newFilename = Path.GetFileNameWithoutExtension(path + filename) +
                sequence.ToString() + Path.GetExtension(path + filename);
            sequence++;
        }
        return newFilename;
    }

    public static void DeleteFileFromDisk(string filepath)
    {
        try
        {
            if (File.Exists(filepath))
                File.Delete(filepath);
        }
        catch { }
    }

    public static string HashData(string Data)
    {
        byte[] dataBytes = Encoding.UTF8.GetBytes(Data);
        SHA1 sha1Hash = SHA1.Create();
        byte[] dataHash = sha1Hash.ComputeHash(dataBytes);
        string dataNew = Convert.ToBase64String(dataHash);
        return System.Text.RegularExpressions.Regex.Replace(dataNew, "\\W", string.Empty);
    }

    public static void ShowError(Page pg, string msg)
    {
        ClientScriptManager csm = pg.ClientScript;
        if (!csm.IsStartupScriptRegistered(Type.GetType("System.String"), "error"))
            csm.RegisterStartupScript(Type.GetType("System.String"), "error", "alert('" + msg + "');", true);
    }

    #region "Application Settings Methods"

    public static string GetSettings(string key)
    {
        // Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();

        //return Convert.ToString(db.ExecuteScalar("AppSettings_Get", null, key));
        return Convert.ToString(db.ExecuteScalar("AppSettings_Get", key));
    }

    public static Hashtable GetSettings(byte typeId)
    {
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();

        Hashtable settings = new Hashtable();

        using (IDataReader reader = db.ExecuteReader("AppSettings_Get", typeId, null))
        {
            while (reader.Read())
                settings.Add(reader[0], reader[1]);
        }

        return settings;
    }

    public static void UpdateSettings(string key, string value)
    {
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();
        DbCommand cmd = db.GetStoredProcCommand("AppSettings_Update");

        db.AddInParameter(cmd, "key", DbType.String, key);
        db.AddInParameter(cmd, "value", DbType.String, value);

        db.ExecuteNonQuery(cmd);
    }

    public static void UpdateSettings(DataSet dsSettings)
    {
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();
        DbCommand cmd = db.GetStoredProcCommand("AppSettings_Update");

        db.AddInParameter(cmd, "key", DbType.String, "Key", DataRowVersion.Current);
        db.AddInParameter(cmd, "value", DbType.String, "Value", DataRowVersion.Current);

        db.UpdateDataSet(dsSettings, dsSettings.Tables[0].TableName, cmd, null, null, UpdateBehavior.Standard, dsSettings.Tables[0].Rows.Count);
    }

    #endregion

    public static DataTable GetNewsletterSubscribers()
    {
        // Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();
        Microsoft.Practices.EnterpriseLibrary.Data.Database db = DatabaseFactory.CreateDatabase();
        return db.ExecuteDataSet("NewsletterSubscribers_GetList").Tables[0];
    }

    #region "Send Mail Methods"

    public static string MailServer
    {
        get
        {
            return GetSettings("MailServer");
        }
    }

    public static string MailStyle
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<style>.mailbody{font-family:Tahoma;font-size:12px}");
            sb.AppendLine(".mailbody table{font-family:Tahoma;font-size:12px}");
            sb.AppendLine(".mailbody a{font-family:Tahoma;font-size:12px;font-weight:bold;text-decoration:none;color:#000000;}");
            sb.AppendLine(".mailbody a:hover{font-family:Tahoma;font-size:12px;font-weight:bold;text-decoration:underline;color:#000000;}");
            sb.AppendLine(".mailbody a.DO{font-family:Tahoma;font-size:12px;text-decoration:underline;color:#18A8D8;}");
            sb.AppendLine(".mailbody a.DO:hover{font-family:Tahoma;font-size:12px;text-decoration:none;color:#18A8D8;}</style>");
            return sb.ToString();
        }
    }

  
    #endregion
}