﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for BackEndUser
/// </summary>
public class BackEndUser
{
    public static void SetCookie(Hashtable user)
    {
        HttpCookie cookie = new HttpCookie("GhabourBackendUser");

        cookie.Values.Add("UserId", user["UserId"].ToString());
        cookie.Values.Add("FirstName", user["FirstName"].ToString());
        cookie.Values.Add("LastName", user["LastName"].ToString());
        cookie.Values.Add("Role", user["Role"].ToString());
        cookie.Values.Add("Sections", user["Sections"].ToString());

        cookie.Expires = DateTime.Now.AddDays(1);
        HttpContext.Current.Response.Cookies.Add(cookie);
    }

    public static void RemoveCookie()
    {
        HttpCookie cookie = HttpContext.Current.Request.Cookies["GhabourBackendUser"];
        if (cookie != null)
        {
            cookie.Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }

    public static string GetFullName()
    {
        string fullname = string.Empty;
        HttpCookie cookie = HttpContext.Current.Request.Cookies["GhabourBackendUser"];
        if (cookie != null)
            fullname = cookie["FirstName"] + " " + cookie["LastName"];

        return fullname;
    }

    public static int GetUserId()
    {
        int UserId = 0;
        HttpCookie cookie = HttpContext.Current.Request.Cookies["GhabourBackendUser"];
        if (cookie != null)
            UserId = Convert.ToInt32(cookie["UserId"]);

        return UserId;
    }

    public static int GetUserRole()
    {
        int RoleId = 0;
        HttpCookie cookie = HttpContext.Current.Request.Cookies["GhabourBackendUser"];
        if (cookie != null)
            RoleId = Convert.ToInt32(cookie["Role"]);

        return RoleId;
    }

    public static ArrayList GetUserRoleAndSections()
    {
        ArrayList sections = new ArrayList();

        HttpCookie cookie = HttpContext.Current.Request.Cookies["GhabourBackendUser"];
        if (cookie != null)
        {
            string[] arr = (cookie["Role"] + "," + cookie["Sections"]).Split(',');
            foreach (string s in arr)
                sections.Add(s);
        }

        return sections;
    }

    public enum Sections
    {
        //Backgrounds = 1, Videos, Banners,
        //NewsTickers, Polls, Sponsors, Formulas, Teams, Banks, BanksNews,
        //Insurances, InsurancesNews, News, MotorSports, Opinions, Tests,
        //FuturesCars, CarKnowledge, Tags, Brands, NewsPhotos, MotorSportsPhotos,
        //OpinionsPhotos, TestsPhotos, FuturesCarsPhotos, CarKnowledgePhotos,
        //ServicesCenters, ShowRooms, Models, ModelsPhotos
        Project = 1, Users, Page
    };
}