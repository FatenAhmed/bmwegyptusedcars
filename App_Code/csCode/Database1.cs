﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.CSharp;
using System.Configuration;

/// <summary>
/// Summary description for Database1
/// </summary>
public class Database1
{
    public Database1()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Variables
    static string ConnectionString = ConfigurationManager.ConnectionStrings["db_connection"].ConnectionString;
    SqlConnection Myconnection = new SqlConnection(ConnectionString);
    DataSet ds = new DataSet();
    SqlDataAdapter Adapter = new SqlDataAdapter();
    SqlCommand command = new SqlCommand();
    #endregion

    #region Functions

    public string chkString(string pString)
    {
        string fString = pString.Trim();
        fString = fString.Replace("#", "");
        fString = fString.Replace("%", "");
        fString = fString.Replace(";", "");
        fString = fString.Replace("<script", "");
        fString = fString.Replace("</script>", "");
        fString = fString.Replace("script", "");
        fString = fString.Replace("javascript", "");
        fString = fString.Replace("insert", "");
        fString = fString.Replace("select", "");
        fString = fString.Replace("update", "");
        fString = fString.Replace("create", "");
        fString = fString.Replace("alter", "");

        return fString;
    }

    public string doStoredProcedure2(string[] valuesArray, string[] paramsArray, string StoredProcedureName)
    {
        string returnMsg = "";
        int i = 0;

        command = new SqlCommand(StoredProcedureName, Myconnection);
        for (i = 0; i < valuesArray.Length; i++)
        {
            command.Parameters.Add(new SqlParameter(paramsArray[i], chkString(valuesArray[i])));
        }
        command.CommandType = CommandType.StoredProcedure;
        try
        {
            Myconnection.Open();
            command.ExecuteNonQuery();
            returnMsg = "Success";
        }
        catch (Exception e)
        {
            returnMsg = e.Message;
        }
        return returnMsg;
    }

    public DataSet doStoredProcedure(string[] valuesArray, string[] paramsArray, string StoredProcedureName)
    {
        int i;
        command = new SqlCommand(StoredProcedureName, Myconnection);
        if (valuesArray.Length > 0)
        {
            for (i = 0; i <= valuesArray.Length - 1; i++)
            {
                command.Parameters.Add(new SqlParameter(paramsArray[i], chkString(valuesArray[i])));
            }
        }
        command.CommandType = CommandType.StoredProcedure;
        ds = new DataSet();
        Adapter = new SqlDataAdapter(command);
        Myconnection.Open();
        Adapter.Fill(ds);
        try { }
        catch (Exception e) { }
        finally
        {
            if (Myconnection.State == ConnectionState.Open)
            {
                Myconnection.Close();
            }
        }
        return ds;
    }


    #endregion
}