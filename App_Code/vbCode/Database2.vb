﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Configuration
Imports System

Public Class Database2
#Region "variables"
    Dim ConnectionStirng As String = ConfigurationManager.ConnectionStrings("db_connection").ConnectionString
    Dim Myconnection As SqlConnection = New SqlConnection(ConnectionStirng)
    Dim ds As DataSet
    Dim Adapter As New SqlDataAdapter
    Dim command As New SqlCommand
#End Region
#Region "functions"
    Public Function chkString(ByVal pString As String)
        Dim fString = Trim(pString)
        If fString <> "" Then
            fString = Replace(fString, "#", "")
            fString = Replace(fString, "%", "")
            fString = Replace(fString, ";", "")
            fString = Replace(fString, "<script", "")
            fString = Replace(fString, "</script>", "")
            fString = Replace(fString, "script", "")
            fString = Replace(fString, "javascript", "")
            fString = Replace(fString, "insert", "", , , CompareMethod.Text)
            fString = Replace(fString, "select", "", , , CompareMethod.Text)
            fString = Replace(fString, "update", "", , , CompareMethod.Text)
            fString = Replace(fString, "create", "", , , CompareMethod.Text)
            fString = Replace(fString, "alter", "", , , CompareMethod.Text)
        End If
        chkString = fString
    End Function
    Public Function doStoredProcedure2(ByVal valuesArray As String(), ByVal paramsArray As String(), ByVal StoredProcedureName As String) As String
        Dim returnMsg As String = ""
        Dim i As Integer
        command = New SqlCommand(StoredProcedureName, Myconnection)

        For i = 0 To valuesArray.Length - 1
            command.Parameters.Add(New SqlParameter(paramsArray(i), chkString(valuesArray(i))))
        Next
        command.CommandType = CommandType.StoredProcedure
        Try

            Myconnection.Open()
            command.ExecuteNonQuery()
            returnMsg = "Success"
        Catch ex As Exception
            returnMsg = ex.Message
            'Throw ex
        Finally
            If Myconnection.State = ConnectionState.Open Then
                Myconnection.Close()
            End If
        End Try
        Return returnMsg
    End Function

    Public Function doStoredProcedure(ByVal valuesArray As String(), ByVal paramsArray As String(), ByVal StoredProcedureName As String) As DataSet
        Dim i As Integer
        command = New SqlCommand(StoredProcedureName, Myconnection)
        If valuesArray.Length > 0 Then
            For i = 0 To valuesArray.Length - 1
                command.Parameters.Add(New SqlParameter(paramsArray(i), chkString(valuesArray(i))))
            Next
        End If
        command.CommandType = CommandType.StoredProcedure

        ds = New DataSet
        Adapter = New SqlDataAdapter(command)

        Myconnection.Open()
        Adapter.Fill(ds)
        Try
        Catch ex As Exception
            'Throw ex
        Finally
            If Myconnection.State = ConnectionState.Open Then
                Myconnection.Close()
            End If
        End Try

        Return ds
    End Function

#End Region
End Class
