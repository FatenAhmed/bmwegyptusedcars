﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SearchCarsDAL
/// </summary>
public class SearchCarDAL
{
    private bmwusedcarsdbEntities1 dbContext = new bmwusedcarsdbEntities1();

    public SearchCarDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<SearchCar> Search(string modelText, string keywordsText, string series)
    {
        List<SearchCar> list = new List<SearchCar>();
        try
        {
            list = dbContext.SearchCars.ToList();
            if (!string.IsNullOrEmpty(modelText))
                list = list.Where(s => s.Model.ToLower().Contains(modelText.ToLower().Trim())).ToList();
            if (!string.IsNullOrEmpty(keywordsText))
                list = list.Where(s => s.Keywords.ToLower().Contains(keywordsText.ToLower().Trim())).ToList();
            if (!string.IsNullOrEmpty(series))
                list = list.Where(s => s.Series.ToLower().Contains(series.ToLower())).ToList();
        }
        catch (Exception)
        {
        }
        return list;
    }

    public bool DeleteItem(int Id)
    {
        bool success = false;

        SearchCar loadedObject = dbContext.SearchCars.First(x => x.Id == Id);

        try
        {
            dbContext.SearchCars.Remove(loadedObject);
            dbContext.SaveChanges();
            success = true;
        }
        catch (Exception)
        {

        }
        return success;
    }
}