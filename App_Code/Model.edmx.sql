
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/10/2019 14:22:17
-- Generated from EDMX file: E:\Work\bmwegyptusedcars\App_Code\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [bmwusedcarsdb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UsersRoleUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsersRoles] DROP CONSTRAINT [FK_UsersRoleUser];
GO
IF OBJECT_ID(N'[dbo].[FK_UsersRoleRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UsersRoles] DROP CONSTRAINT [FK_UsersRoleRole];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[SearchCars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SearchCars];
GO
IF OBJECT_ID(N'[dbo].[UsedCars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsedCars];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[UsersRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsersRoles];
GO
IF OBJECT_ID(N'[dbo].[UsersSections]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsersSections];
GO
IF OBJECT_ID(N'[dbo].[WantCars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WantCars];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'SearchCars'
CREATE TABLE [dbo].[SearchCars] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Series] nvarchar(50)  NOT NULL,
    [Model] nvarchar(50)  NOT NULL,
    [Color] nvarchar(100)  NOT NULL,
    [DoorsNumber] nvarchar(50)  NOT NULL,
    [EngineSize] nvarchar(50)  NOT NULL,
    [IYear] nvarchar(50)  NOT NULL,
    [Price] nvarchar(50)  NOT NULL,
    [Specs] nvarchar(4000)  NOT NULL,
    [Governarate] nvarchar(50)  NOT NULL,
    [Dealer] nvarchar(100)  NULL,
    [Keywords] nvarchar(50)  NOT NULL,
    [ImagesNO] nvarchar(50)  NOT NULL,
    [ImagesThumb] nvarchar(50)  NOT NULL,
    [AddDate] datetime  NULL,
    [ChassisNO] nvarchar(50)  NOT NULL,
    [PriceInt] nvarchar(50)  NULL,
    [EngineSizeInt] nvarchar(50)  NULL,
    [Milage] nvarchar(50)  NULL,
    [Show] bit  NULL
);
GO

-- Creating table 'UsedCars'
CREATE TABLE [dbo].[UsedCars] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Series] nvarchar(50)  NOT NULL,
    [Model] nvarchar(50)  NOT NULL,
    [DoorsNumber] nvarchar(50)  NOT NULL,
    [EngineSize] nvarchar(50)  NOT NULL,
    [IYear] nvarchar(50)  NOT NULL,
    [Price] nvarchar(50)  NOT NULL,
    [Specs] nvarchar(4000)  NOT NULL,
    [Governarate] nvarchar(100)  NOT NULL,
    [Dealer] nvarchar(100)  NOT NULL,
    [Keywords] nvarchar(100)  NOT NULL,
    [ImagesNo] nvarchar(50)  NOT NULL,
    [ImagesThumb] nvarchar(150)  NOT NULL,
    [AddDate] datetime  NULL,
    [ChassisNo] nvarchar(50)  NOT NULL,
    [PriceInt] nvarchar(max)  NULL,
    [EngineSizeInt] nvarchar(50)  NOT NULL,
    [Milage] nvarchar(50)  NOT NULL,
    [Hide] nvarchar(50)  NOT NULL,
    [Color] nvarchar(200)  NOT NULL,
    [Show] bit  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [UserName] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NOT NULL,
    [Email] nvarchar(100)  NOT NULL,
    [LastLoginDate] datetime  NULL,
    [IsLockedOut] bit  NOT NULL,
    [LastLockoutDate] datetime  NULL,
    [FailedPasswordAttemptCount] int  NULL,
    [FailedPasswordAttemptWindowStart] datetime  NULL,
    [RoleId] tinyint  NULL,
    [Sections] varchar(50)  NULL
);
GO

-- Creating table 'UsersRoles'
CREATE TABLE [dbo].[UsersRoles] (
    [Id] bigint IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [RoleId] bigint  NOT NULL,
    [ChassisNO] nvarchar(50)  NOT NULL,
    [AddDate] datetime  NULL
);
GO

-- Creating table 'UsersSections'
CREATE TABLE [dbo].[UsersSections] (
    [SectionId] tinyint IDENTITY(1,1) NOT NULL,
    [SectionName] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'WantCars'
CREATE TABLE [dbo].[WantCars] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Series] nvarchar(50)  NOT NULL,
    [NYear] nvarchar(50)  NOT NULL,
    [Color] nvarchar(50)  NOT NULL,
    [Chassis] nvarchar(50)  NOT NULL,
    [UName] nvarchar(50)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Phone] nvarchar(50)  NOT NULL,
    [CurrentBMW] nvarchar(50)  NOT NULL,
    [RegisterNO] nvarchar(50)  NOT NULL,
    [MeansContact] nvarchar(50)  NOT NULL,
    [Dealer] nvarchar(50)  NOT NULL,
    [Additional] nvarchar(3000)  NULL,
    [NoPost] nvarchar(5)  NOT NULL,
    [NoPhone] nvarchar(5)  NOT NULL,
    [AddDate] datetime  NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [RoleId] bigint IDENTITY(1,1) NOT NULL,
    [RoleName] varchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'SearchCars'
ALTER TABLE [dbo].[SearchCars]
ADD CONSTRAINT [PK_SearchCars]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UsedCars'
ALTER TABLE [dbo].[UsedCars]
ADD CONSTRAINT [PK_UsedCars]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UserId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [Id] in table 'UsersRoles'
ALTER TABLE [dbo].[UsersRoles]
ADD CONSTRAINT [PK_UsersRoles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [SectionId] in table 'UsersSections'
ALTER TABLE [dbo].[UsersSections]
ADD CONSTRAINT [PK_UsersSections]
    PRIMARY KEY CLUSTERED ([SectionId] ASC);
GO

-- Creating primary key on [Id] in table 'WantCars'
ALTER TABLE [dbo].[WantCars]
ADD CONSTRAINT [PK_WantCars]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [RoleId] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([RoleId] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'UsersRoles'
ALTER TABLE [dbo].[UsersRoles]
ADD CONSTRAINT [FK_UsersRoleUser]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersRoleUser'
CREATE INDEX [IX_FK_UsersRoleUser]
ON [dbo].[UsersRoles]
    ([UserId]);
GO

-- Creating foreign key on [RoleId] in table 'UsersRoles'
ALTER TABLE [dbo].[UsersRoles]
ADD CONSTRAINT [FK_UsersRoleRole]
    FOREIGN KEY ([RoleId])
    REFERENCES [dbo].[Roles]
        ([RoleId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UsersRoleRole'
CREATE INDEX [IX_FK_UsersRoleRole]
ON [dbo].[UsersRoles]
    ([RoleId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------