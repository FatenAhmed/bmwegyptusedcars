﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;

public partial class bmwusedcarsdbEntities1 : DbContext
{
    public bmwusedcarsdbEntities1()
        : base("name=bmwusedcarsdbEntities1")
    {
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }

    public virtual DbSet<SearchCar> SearchCars { get; set; }
    public virtual DbSet<UsedCar> UsedCars { get; set; }
    public virtual DbSet<User> Users { get; set; }
    public virtual DbSet<UsersRole> UsersRoles { get; set; }
    public virtual DbSet<UsersSection> UsersSections { get; set; }
    public virtual DbSet<WantCar> WantCars { get; set; }
    public virtual DbSet<Role> Roles { get; set; }

    public virtual int Add_SearchCar(string series, string model, string color, string doorsnumber, string enginesize, string iyear, string price, string specs, string governarate, string dealer, string keywords, Nullable<int> imagesno, string imagesthumb, string chassisno, string priceint, string enginesizeint, string milage)
    {
        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var modelParameter = model != null ?
            new ObjectParameter("model", model) :
            new ObjectParameter("model", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var enginesizeParameter = enginesize != null ?
            new ObjectParameter("enginesize", enginesize) :
            new ObjectParameter("enginesize", typeof(string));

        var iyearParameter = iyear != null ?
            new ObjectParameter("iyear", iyear) :
            new ObjectParameter("iyear", typeof(string));

        var priceParameter = price != null ?
            new ObjectParameter("price", price) :
            new ObjectParameter("price", typeof(string));

        var specsParameter = specs != null ?
            new ObjectParameter("specs", specs) :
            new ObjectParameter("specs", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var keywordsParameter = keywords != null ?
            new ObjectParameter("keywords", keywords) :
            new ObjectParameter("keywords", typeof(string));

        var imagesnoParameter = imagesno.HasValue ?
            new ObjectParameter("imagesno", imagesno) :
            new ObjectParameter("imagesno", typeof(int));

        var imagesthumbParameter = imagesthumb != null ?
            new ObjectParameter("imagesthumb", imagesthumb) :
            new ObjectParameter("imagesthumb", typeof(string));

        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        var priceintParameter = priceint != null ?
            new ObjectParameter("priceint", priceint) :
            new ObjectParameter("priceint", typeof(string));

        var enginesizeintParameter = enginesizeint != null ?
            new ObjectParameter("enginesizeint", enginesizeint) :
            new ObjectParameter("enginesizeint", typeof(string));

        var milageParameter = milage != null ?
            new ObjectParameter("milage", milage) :
            new ObjectParameter("milage", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Add_SearchCar", seriesParameter, modelParameter, colorParameter, doorsnumberParameter, enginesizeParameter, iyearParameter, priceParameter, specsParameter, governarateParameter, dealerParameter, keywordsParameter, imagesnoParameter, imagesthumbParameter, chassisnoParameter, priceintParameter, enginesizeintParameter, milageParameter);
    }

    public virtual int Add_UsedCar(string series, string model, string doorsnumber, string enginesize, string iyear, string price, string specs, string governarate, string dealer, string keywords, Nullable<int> imagesno, string imagesthumb, string chassisno, string priceint, string enginesizeint, string milage, string color)
    {
        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var modelParameter = model != null ?
            new ObjectParameter("model", model) :
            new ObjectParameter("model", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var enginesizeParameter = enginesize != null ?
            new ObjectParameter("enginesize", enginesize) :
            new ObjectParameter("enginesize", typeof(string));

        var iyearParameter = iyear != null ?
            new ObjectParameter("iyear", iyear) :
            new ObjectParameter("iyear", typeof(string));

        var priceParameter = price != null ?
            new ObjectParameter("price", price) :
            new ObjectParameter("price", typeof(string));

        var specsParameter = specs != null ?
            new ObjectParameter("specs", specs) :
            new ObjectParameter("specs", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var keywordsParameter = keywords != null ?
            new ObjectParameter("keywords", keywords) :
            new ObjectParameter("keywords", typeof(string));

        var imagesnoParameter = imagesno.HasValue ?
            new ObjectParameter("imagesno", imagesno) :
            new ObjectParameter("imagesno", typeof(int));

        var imagesthumbParameter = imagesthumb != null ?
            new ObjectParameter("imagesthumb", imagesthumb) :
            new ObjectParameter("imagesthumb", typeof(string));

        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        var priceintParameter = priceint != null ?
            new ObjectParameter("priceint", priceint) :
            new ObjectParameter("priceint", typeof(string));

        var enginesizeintParameter = enginesizeint != null ?
            new ObjectParameter("enginesizeint", enginesizeint) :
            new ObjectParameter("enginesizeint", typeof(string));

        var milageParameter = milage != null ?
            new ObjectParameter("milage", milage) :
            new ObjectParameter("milage", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Add_UsedCar", seriesParameter, modelParameter, doorsnumberParameter, enginesizeParameter, iyearParameter, priceParameter, specsParameter, governarateParameter, dealerParameter, keywordsParameter, imagesnoParameter, imagesthumbParameter, chassisnoParameter, priceintParameter, enginesizeintParameter, milageParameter, colorParameter);
    }

    public virtual int Add_WantCars(string series, string nyear, string color, string chassis, string name, string email, string phone, string currentbmw, string registerno, string meanscontact, string dealer, string additional, string nopost, string nophone)
    {
        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var nyearParameter = nyear != null ?
            new ObjectParameter("nyear", nyear) :
            new ObjectParameter("nyear", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var chassisParameter = chassis != null ?
            new ObjectParameter("chassis", chassis) :
            new ObjectParameter("chassis", typeof(string));

        var nameParameter = name != null ?
            new ObjectParameter("name", name) :
            new ObjectParameter("name", typeof(string));

        var emailParameter = email != null ?
            new ObjectParameter("email", email) :
            new ObjectParameter("email", typeof(string));

        var phoneParameter = phone != null ?
            new ObjectParameter("phone", phone) :
            new ObjectParameter("phone", typeof(string));

        var currentbmwParameter = currentbmw != null ?
            new ObjectParameter("currentbmw", currentbmw) :
            new ObjectParameter("currentbmw", typeof(string));

        var registernoParameter = registerno != null ?
            new ObjectParameter("registerno", registerno) :
            new ObjectParameter("registerno", typeof(string));

        var meanscontactParameter = meanscontact != null ?
            new ObjectParameter("meanscontact", meanscontact) :
            new ObjectParameter("meanscontact", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var additionalParameter = additional != null ?
            new ObjectParameter("additional", additional) :
            new ObjectParameter("additional", typeof(string));

        var nopostParameter = nopost != null ?
            new ObjectParameter("nopost", nopost) :
            new ObjectParameter("nopost", typeof(string));

        var nophoneParameter = nophone != null ?
            new ObjectParameter("nophone", nophone) :
            new ObjectParameter("nophone", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Add_WantCars", seriesParameter, nyearParameter, colorParameter, chassisParameter, nameParameter, emailParameter, phoneParameter, currentbmwParameter, registernoParameter, meanscontactParameter, dealerParameter, additionalParameter, nopostParameter, nophoneParameter);
    }

    public virtual int Delete_SearchCar(string chassisno)
    {
        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Delete_SearchCar", chassisnoParameter);
    }

    public virtual int Delete_UsedCar(string chassisno)
    {
        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Delete_UsedCar", chassisnoParameter);
    }

    public virtual ObjectResult<Get_CarDetails_Result> Get_CarDetails(string id)
    {
        var idParameter = id != null ?
            new ObjectParameter("id", id) :
            new ObjectParameter("id", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_CarDetails_Result>("Get_CarDetails", idParameter);
    }

    public virtual ObjectResult<Get_Thumbs_Result> Get_Thumbs(string series, string governarate, string dealer, string doorsnumber, string color, string year_min, string year_max, string price_min, string price_max, string engine_min, string engine_max)
    {
        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var year_minParameter = year_min != null ?
            new ObjectParameter("year_min", year_min) :
            new ObjectParameter("year_min", typeof(string));

        var year_maxParameter = year_max != null ?
            new ObjectParameter("year_max", year_max) :
            new ObjectParameter("year_max", typeof(string));

        var price_minParameter = price_min != null ?
            new ObjectParameter("price_min", price_min) :
            new ObjectParameter("price_min", typeof(string));

        var price_maxParameter = price_max != null ?
            new ObjectParameter("price_max", price_max) :
            new ObjectParameter("price_max", typeof(string));

        var engine_minParameter = engine_min != null ?
            new ObjectParameter("engine_min", engine_min) :
            new ObjectParameter("engine_min", typeof(string));

        var engine_maxParameter = engine_max != null ?
            new ObjectParameter("engine_max", engine_max) :
            new ObjectParameter("engine_max", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_Thumbs_Result>("Get_Thumbs", seriesParameter, governarateParameter, dealerParameter, doorsnumberParameter, colorParameter, year_minParameter, year_maxParameter, price_minParameter, price_maxParameter, engine_minParameter, engine_maxParameter);
    }

    public virtual ObjectResult<Get_ThumbsUsed_Result> Get_ThumbsUsed(string series, string governarate, string dealer, string doorsnumber, string color, string year_min, string year_max, string price_min, string price_max, string engine_min, string engine_max)
    {
        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var year_minParameter = year_min != null ?
            new ObjectParameter("year_min", year_min) :
            new ObjectParameter("year_min", typeof(string));

        var year_maxParameter = year_max != null ?
            new ObjectParameter("year_max", year_max) :
            new ObjectParameter("year_max", typeof(string));

        var price_minParameter = price_min != null ?
            new ObjectParameter("price_min", price_min) :
            new ObjectParameter("price_min", typeof(string));

        var price_maxParameter = price_max != null ?
            new ObjectParameter("price_max", price_max) :
            new ObjectParameter("price_max", typeof(string));

        var engine_minParameter = engine_min != null ?
            new ObjectParameter("engine_min", engine_min) :
            new ObjectParameter("engine_min", typeof(string));

        var engine_maxParameter = engine_max != null ?
            new ObjectParameter("engine_max", engine_max) :
            new ObjectParameter("engine_max", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_ThumbsUsed_Result>("Get_ThumbsUsed", seriesParameter, governarateParameter, dealerParameter, doorsnumberParameter, colorParameter, year_minParameter, year_maxParameter, price_minParameter, price_maxParameter, engine_minParameter, engine_maxParameter);
    }

    public virtual ObjectResult<Get_UsedCarDetails_Result> Get_UsedCarDetails(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("id", id) :
            new ObjectParameter("id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_UsedCarDetails_Result>("Get_UsedCarDetails", idParameter);
    }

    public virtual ObjectResult<Get_WantCars_Result> Get_WantCars()
    {
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Get_WantCars_Result>("Get_WantCars");
    }

    public virtual int SearchCar_Delete(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SearchCar_Delete", idParameter);
    }

    public virtual ObjectResult<SearchCar_GetById_Result> SearchCar_GetById(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SearchCar_GetById_Result>("SearchCar_GetById", idParameter);
    }

    public virtual ObjectResult<SearchCar_GetByPage_Result> SearchCar_GetByPage(Nullable<bool> withHidden, Nullable<int> startIndex, Nullable<int> maximumRows, string sortColumn, ObjectParameter recordsCount)
    {
        var withHiddenParameter = withHidden.HasValue ?
            new ObjectParameter("withHidden", withHidden) :
            new ObjectParameter("withHidden", typeof(bool));

        var startIndexParameter = startIndex.HasValue ?
            new ObjectParameter("startIndex", startIndex) :
            new ObjectParameter("startIndex", typeof(int));

        var maximumRowsParameter = maximumRows.HasValue ?
            new ObjectParameter("maximumRows", maximumRows) :
            new ObjectParameter("maximumRows", typeof(int));

        var sortColumnParameter = sortColumn != null ?
            new ObjectParameter("sortColumn", sortColumn) :
            new ObjectParameter("sortColumn", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SearchCar_GetByPage_Result>("SearchCar_GetByPage", withHiddenParameter, startIndexParameter, maximumRowsParameter, sortColumnParameter, recordsCount);
    }

    public virtual int SearchCar_ShowHide(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SearchCar_ShowHide", idParameter);
    }

    public virtual int SearchCar_Update(Nullable<int> id, string series, string model, string color, string doorsnumber, string enginesize, string iyear, string price, string specs, string governarate, string dealer, string keywords, Nullable<int> imagesno, string imagesthumb, string chassisno, string priceint, string enginesizeint, string milage)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var modelParameter = model != null ?
            new ObjectParameter("model", model) :
            new ObjectParameter("model", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var enginesizeParameter = enginesize != null ?
            new ObjectParameter("enginesize", enginesize) :
            new ObjectParameter("enginesize", typeof(string));

        var iyearParameter = iyear != null ?
            new ObjectParameter("iyear", iyear) :
            new ObjectParameter("iyear", typeof(string));

        var priceParameter = price != null ?
            new ObjectParameter("price", price) :
            new ObjectParameter("price", typeof(string));

        var specsParameter = specs != null ?
            new ObjectParameter("specs", specs) :
            new ObjectParameter("specs", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var keywordsParameter = keywords != null ?
            new ObjectParameter("keywords", keywords) :
            new ObjectParameter("keywords", typeof(string));

        var imagesnoParameter = imagesno.HasValue ?
            new ObjectParameter("imagesno", imagesno) :
            new ObjectParameter("imagesno", typeof(int));

        var imagesthumbParameter = imagesthumb != null ?
            new ObjectParameter("imagesthumb", imagesthumb) :
            new ObjectParameter("imagesthumb", typeof(string));

        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        var priceintParameter = priceint != null ?
            new ObjectParameter("priceint", priceint) :
            new ObjectParameter("priceint", typeof(string));

        var enginesizeintParameter = enginesizeint != null ?
            new ObjectParameter("enginesizeint", enginesizeint) :
            new ObjectParameter("enginesizeint", typeof(string));

        var milageParameter = milage != null ?
            new ObjectParameter("milage", milage) :
            new ObjectParameter("milage", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SearchCar_Update", idParameter, seriesParameter, modelParameter, colorParameter, doorsnumberParameter, enginesizeParameter, iyearParameter, priceParameter, specsParameter, governarateParameter, dealerParameter, keywordsParameter, imagesnoParameter, imagesthumbParameter, chassisnoParameter, priceintParameter, enginesizeintParameter, milageParameter);
    }

    public virtual ObjectResult<SP_GetAllSearchCars_Result> SP_GetAllSearchCars()
    {
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetAllSearchCars_Result>("SP_GetAllSearchCars");
    }

    public virtual ObjectResult<SP_GetAllUsedCars_Result> SP_GetAllUsedCars()
    {
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<SP_GetAllUsedCars_Result>("SP_GetAllUsedCars");
    }

    public virtual int UsedCar_Delete(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UsedCar_Delete", idParameter);
    }

    public virtual ObjectResult<UsedCar_GetById_Result> UsedCar_GetById(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<UsedCar_GetById_Result>("UsedCar_GetById", idParameter);
    }

    public virtual ObjectResult<UsedCar_GetByPage_Result> UsedCar_GetByPage(Nullable<bool> withHidden, Nullable<int> startIndex, Nullable<int> maximumRows, string sortColumn, ObjectParameter recordsCount)
    {
        var withHiddenParameter = withHidden.HasValue ?
            new ObjectParameter("withHidden", withHidden) :
            new ObjectParameter("withHidden", typeof(bool));

        var startIndexParameter = startIndex.HasValue ?
            new ObjectParameter("startIndex", startIndex) :
            new ObjectParameter("startIndex", typeof(int));

        var maximumRowsParameter = maximumRows.HasValue ?
            new ObjectParameter("maximumRows", maximumRows) :
            new ObjectParameter("maximumRows", typeof(int));

        var sortColumnParameter = sortColumn != null ?
            new ObjectParameter("sortColumn", sortColumn) :
            new ObjectParameter("sortColumn", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<UsedCar_GetByPage_Result>("UsedCar_GetByPage", withHiddenParameter, startIndexParameter, maximumRowsParameter, sortColumnParameter, recordsCount);
    }

    public virtual int UsedCar_ShowHide(Nullable<int> id)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UsedCar_ShowHide", idParameter);
    }

    public virtual int UsedCar_Update(Nullable<int> id, string series, string model, string color, string doorsnumber, string enginesize, string iyear, string price, string specs, string governarate, string dealer, string keywords, Nullable<int> imagesno, string imagesthumb, string chassisno, string priceint, string enginesizeint, string milage)
    {
        var idParameter = id.HasValue ?
            new ObjectParameter("Id", id) :
            new ObjectParameter("Id", typeof(int));

        var seriesParameter = series != null ?
            new ObjectParameter("series", series) :
            new ObjectParameter("series", typeof(string));

        var modelParameter = model != null ?
            new ObjectParameter("model", model) :
            new ObjectParameter("model", typeof(string));

        var colorParameter = color != null ?
            new ObjectParameter("color", color) :
            new ObjectParameter("color", typeof(string));

        var doorsnumberParameter = doorsnumber != null ?
            new ObjectParameter("doorsnumber", doorsnumber) :
            new ObjectParameter("doorsnumber", typeof(string));

        var enginesizeParameter = enginesize != null ?
            new ObjectParameter("enginesize", enginesize) :
            new ObjectParameter("enginesize", typeof(string));

        var iyearParameter = iyear != null ?
            new ObjectParameter("iyear", iyear) :
            new ObjectParameter("iyear", typeof(string));

        var priceParameter = price != null ?
            new ObjectParameter("price", price) :
            new ObjectParameter("price", typeof(string));

        var specsParameter = specs != null ?
            new ObjectParameter("specs", specs) :
            new ObjectParameter("specs", typeof(string));

        var governarateParameter = governarate != null ?
            new ObjectParameter("governarate", governarate) :
            new ObjectParameter("governarate", typeof(string));

        var dealerParameter = dealer != null ?
            new ObjectParameter("dealer", dealer) :
            new ObjectParameter("dealer", typeof(string));

        var keywordsParameter = keywords != null ?
            new ObjectParameter("keywords", keywords) :
            new ObjectParameter("keywords", typeof(string));

        var imagesnoParameter = imagesno.HasValue ?
            new ObjectParameter("imagesno", imagesno) :
            new ObjectParameter("imagesno", typeof(int));

        var imagesthumbParameter = imagesthumb != null ?
            new ObjectParameter("imagesthumb", imagesthumb) :
            new ObjectParameter("imagesthumb", typeof(string));

        var chassisnoParameter = chassisno != null ?
            new ObjectParameter("chassisno", chassisno) :
            new ObjectParameter("chassisno", typeof(string));

        var priceintParameter = priceint != null ?
            new ObjectParameter("priceint", priceint) :
            new ObjectParameter("priceint", typeof(string));

        var enginesizeintParameter = enginesizeint != null ?
            new ObjectParameter("enginesizeint", enginesizeint) :
            new ObjectParameter("enginesizeint", typeof(string));

        var milageParameter = milage != null ?
            new ObjectParameter("milage", milage) :
            new ObjectParameter("milage", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UsedCar_Update", idParameter, seriesParameter, modelParameter, colorParameter, doorsnumberParameter, enginesizeParameter, iyearParameter, priceParameter, specsParameter, governarateParameter, dealerParameter, keywordsParameter, imagesnoParameter, imagesthumbParameter, chassisnoParameter, priceintParameter, enginesizeintParameter, milageParameter);
    }

    public virtual ObjectResult<Users_Authenticate_Result> Users_Authenticate(string userName, string password, Nullable<int> maxInvalidPasswordAttempts, Nullable<int> passwordAttemptWindow, Nullable<System.DateTime> currentTime, ObjectParameter errorCode)
    {
        var userNameParameter = userName != null ?
            new ObjectParameter("userName", userName) :
            new ObjectParameter("userName", typeof(string));

        var passwordParameter = password != null ?
            new ObjectParameter("password", password) :
            new ObjectParameter("password", typeof(string));

        var maxInvalidPasswordAttemptsParameter = maxInvalidPasswordAttempts.HasValue ?
            new ObjectParameter("maxInvalidPasswordAttempts", maxInvalidPasswordAttempts) :
            new ObjectParameter("maxInvalidPasswordAttempts", typeof(int));

        var passwordAttemptWindowParameter = passwordAttemptWindow.HasValue ?
            new ObjectParameter("passwordAttemptWindow", passwordAttemptWindow) :
            new ObjectParameter("passwordAttemptWindow", typeof(int));

        var currentTimeParameter = currentTime.HasValue ?
            new ObjectParameter("currentTime", currentTime) :
            new ObjectParameter("currentTime", typeof(System.DateTime));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Users_Authenticate_Result>("Users_Authenticate", userNameParameter, passwordParameter, maxInvalidPasswordAttemptsParameter, passwordAttemptWindowParameter, currentTimeParameter, errorCode);
    }

    public virtual int Users_Delete(Nullable<int> userId)
    {
        var userIdParameter = userId.HasValue ?
            new ObjectParameter("userId", userId) :
            new ObjectParameter("userId", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Users_Delete", userIdParameter);
    }

    public virtual ObjectResult<Users_GetById_Result> Users_GetById(Nullable<int> userId)
    {
        var userIdParameter = userId.HasValue ?
            new ObjectParameter("userId", userId) :
            new ObjectParameter("userId", typeof(int));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Users_GetById_Result>("Users_GetById", userIdParameter);
    }

    public virtual ObjectResult<Users_GetList_Result> Users_GetList()
    {
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Users_GetList_Result>("Users_GetList");
    }

    public virtual int Users_Insert(string firstName, string lastName, string userName, string password, string email, Nullable<bool> isLockedOut, Nullable<byte> roleId, string sections, ObjectParameter errorCode)
    {
        var firstNameParameter = firstName != null ?
            new ObjectParameter("firstName", firstName) :
            new ObjectParameter("firstName", typeof(string));

        var lastNameParameter = lastName != null ?
            new ObjectParameter("lastName", lastName) :
            new ObjectParameter("lastName", typeof(string));

        var userNameParameter = userName != null ?
            new ObjectParameter("userName", userName) :
            new ObjectParameter("userName", typeof(string));

        var passwordParameter = password != null ?
            new ObjectParameter("password", password) :
            new ObjectParameter("password", typeof(string));

        var emailParameter = email != null ?
            new ObjectParameter("email", email) :
            new ObjectParameter("email", typeof(string));

        var isLockedOutParameter = isLockedOut.HasValue ?
            new ObjectParameter("isLockedOut", isLockedOut) :
            new ObjectParameter("isLockedOut", typeof(bool));

        var roleIdParameter = roleId.HasValue ?
            new ObjectParameter("roleId", roleId) :
            new ObjectParameter("roleId", typeof(byte));

        var sectionsParameter = sections != null ?
            new ObjectParameter("sections", sections) :
            new ObjectParameter("sections", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Users_Insert", firstNameParameter, lastNameParameter, userNameParameter, passwordParameter, emailParameter, isLockedOutParameter, roleIdParameter, sectionsParameter, errorCode);
    }

    public virtual int Users_Update(Nullable<int> userId, string firstName, string lastName, string password, string email, Nullable<bool> isLockedOut, Nullable<byte> roleId, string sections, ObjectParameter errorCode)
    {
        var userIdParameter = userId.HasValue ?
            new ObjectParameter("userId", userId) :
            new ObjectParameter("userId", typeof(int));

        var firstNameParameter = firstName != null ?
            new ObjectParameter("firstName", firstName) :
            new ObjectParameter("firstName", typeof(string));

        var lastNameParameter = lastName != null ?
            new ObjectParameter("lastName", lastName) :
            new ObjectParameter("lastName", typeof(string));

        var passwordParameter = password != null ?
            new ObjectParameter("password", password) :
            new ObjectParameter("password", typeof(string));

        var emailParameter = email != null ?
            new ObjectParameter("email", email) :
            new ObjectParameter("email", typeof(string));

        var isLockedOutParameter = isLockedOut.HasValue ?
            new ObjectParameter("isLockedOut", isLockedOut) :
            new ObjectParameter("isLockedOut", typeof(bool));

        var roleIdParameter = roleId.HasValue ?
            new ObjectParameter("roleId", roleId) :
            new ObjectParameter("roleId", typeof(byte));

        var sectionsParameter = sections != null ?
            new ObjectParameter("sections", sections) :
            new ObjectParameter("sections", typeof(string));

        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Users_Update", userIdParameter, firstNameParameter, lastNameParameter, passwordParameter, emailParameter, isLockedOutParameter, roleIdParameter, sectionsParameter, errorCode);
    }

    public virtual ObjectResult<UsersSections_GetList_Result> UsersSections_GetList()
    {
        return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<UsersSections_GetList_Result>("UsersSections_GetList");
    }
}
