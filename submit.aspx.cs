﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;

public partial class submit : System.Web.UI.Page
{
    Rectangle oRectangle1, oRectangle2, oRectangle_thumb;
    static string series;
    static string model;
    static string color;
    static string doors;
    static string engine, engine_int;
    static string iyear;
    static string milage;
    static string price, price_int;
    static string govern;
    static string dealer;
    static string keys;
    static string automatic;
    static int imageNO = 0;
    static string specs;
    static string chassisno;
    static HttpFileCollection hfc;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        series = tab1.Value;
        model = input_model.Value;
        color = input_color.Value;
        doors = tab_doors.Value;
        engine = tab_engine.Value;
        iyear = tab_year.Value;
        price = input_price.Value;
        govern = tab_govern.Value;
        dealer = tabs_dealer.Value;
        keys = input_keys.Value;
        specs = TestDetails.Value;
        chassisno = input_chassis.Value;

        milage = input_milage.Value;
        engine_int = input_engine_cc.Value;
        price_int = chkString(price);

        try
        {
            // Get the HttpFileCollection
            hfc = Request.Files;
            imageNO = hfc.Count;

            #region thumb
            float WidthPer_thumb, HeightPer_thumb;
            int NewWidth_thumb, NewHeight_thumb;

            hfc[0].SaveAs(Server.MapPath("~/MyThumbs") + "\\" + Path.GetFileName(hfc[0].FileName));

            System.Drawing.Image img_thumb = System.Drawing.Image.FromFile(Server.MapPath("~/MyThumbs") + "\\" + Path.GetFileName(hfc[0].FileName));

            if (img_thumb.Width > img_thumb.Height)
            {
                NewHeight_thumb = 150;
                HeightPer_thumb = 150 / (float)img_thumb.Height;
                NewWidth_thumb = Convert.ToInt32(img_thumb.Width * HeightPer_thumb);
                oRectangle_thumb = new Rectangle(-(NewWidth_thumb - 200) / 2, 0, NewWidth_thumb, NewHeight_thumb);
            }
            else
            {
                NewWidth_thumb = 200;
                WidthPer_thumb = 200 / (float)img_thumb.Width;
                NewHeight_thumb = Convert.ToInt32(img_thumb.Height * WidthPer_thumb);
                oRectangle_thumb = new Rectangle(0, 0, NewWidth_thumb, NewHeight_thumb);
            }

            System.Drawing.Image image_Thumb = new Bitmap(200, 150, img_thumb.PixelFormat);
            Graphics oGraphic_thumb = Graphics.FromImage(image_Thumb);
            oGraphic_thumb.CompositingQuality = CompositingQuality.HighQuality;
            oGraphic_thumb.SmoothingMode = SmoothingMode.HighQuality;
            oGraphic_thumb.InterpolationMode = InterpolationMode.HighQualityBicubic;
            oGraphic_thumb.DrawImage(img_thumb, oRectangle_thumb);

            image_Thumb.Save(Server.MapPath("~/MyThumbs") + "\\" + chassisno + "_thumb" + Path.GetExtension(hfc[0].FileName));

            img_thumb.Dispose();
            #endregion

            for (int i = 0; i < hfc.Count; i++)
            {
                HttpPostedFile hpf = hfc[i];
                if (hpf.ContentLength > 0)
                {
                    hpf.SaveAs(Server.MapPath("~/MyFiles") + "\\" + Path.GetFileName(hpf.FileName));

                    System.Drawing.Image img1 = System.Drawing.Image.FromFile(Server.MapPath("~/MyFiles") + "\\" + Path.GetFileName(hpf.FileName));

                    float WidthPer1, HeightPer1;
                    int NewWidth1, NewHeight1;
                    float WidthPer2, HeightPer2;
                    int NewWidth2, NewHeight2;

                    if (img1.Width > img1.Height)
                    {
                        NewHeight1 = 480;
                        HeightPer1 = 480 / (float)img1.Height;
                        NewWidth1 = Convert.ToInt32(img1.Width * HeightPer1);
                        oRectangle1 = new Rectangle(-(NewWidth1 - 640) / 2, 0, NewWidth1, NewHeight1);

                        NewHeight2 = 100;
                        HeightPer2 = 100 / (float)img1.Height;
                        NewWidth2 = Convert.ToInt32(img1.Width * HeightPer2);
                        oRectangle2 = new Rectangle(-(NewWidth2 - 140) / 2, 0, NewWidth2, NewHeight2);
                    }
                    else
                    {
                        NewWidth1 = 640;
                        WidthPer1 = 640 / (float)img1.Width;
                        NewHeight1 = Convert.ToInt32(img1.Height * WidthPer1);
                        oRectangle1 = new Rectangle(0, 0, NewWidth1, NewHeight1);

                        NewWidth2 = 140;
                        WidthPer2 = 140 / (float)img1.Width;
                        NewHeight2 = Convert.ToInt32(img1.Height * WidthPer2);
                        oRectangle2 = new Rectangle(0, 0, NewWidth2, NewHeight2);
                    }


                    System.Drawing.Image imageBig = new Bitmap(640, 480, img1.PixelFormat);
                    Graphics oGraphic1 = Graphics.FromImage(imageBig);
                    oGraphic1.CompositingQuality = CompositingQuality.HighQuality;
                    oGraphic1.SmoothingMode = SmoothingMode.HighQuality;
                    oGraphic1.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    oGraphic1.DrawImage(img1, oRectangle1);

                    System.Drawing.Image imageTiny = new Bitmap(140, 100, img1.PixelFormat);
                    Graphics oGraphic2 = Graphics.FromImage(imageTiny);
                    oGraphic2.CompositingQuality = CompositingQuality.HighQuality;
                    oGraphic2.SmoothingMode = SmoothingMode.HighQuality;
                    oGraphic2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    oGraphic2.DrawImage(img1, oRectangle2);

                    imageBig.Save(Server.MapPath("~/MyImages") + "\\" + chassisno + "_" + i.ToString() + "_b" + Path.GetExtension(hpf.FileName));
                    imageTiny.Save(Server.MapPath("~/MyImages") + "\\" + chassisno + "_" + i.ToString() + "_t" + Path.GetExtension(hpf.FileName));

                    img1.Dispose();
                }
            }
        }
        catch (Exception ex)
        {
            // Handle your exception here
        }

        Database datab = new Database();
        string thumbPathString = "MyThumbs/" + chassisno + "_thumb" + Path.GetExtension(hfc[0].FileName);

        string[] ParamsArray = new string[17] { "@Series", "@Model", "@Color", "@DoorsNumber", "@EngineSize", "@IYear", "@Price", 
          "@Specs",  "@Governarate", "@Dealer", "@Keywords", "@ImagesNO", "@ImagesThumb", "@ChassisNO", "PriceInt", "EngineSizeInt", "Milage" };
        string[] valuesArray = new string[17] { series, model, color, doors, engine, iyear, price,
            specs, govern, dealer, keys, imageNO.ToString(), thumbPathString, chassisno, price_int, engine_int, milage };
        string result = datab.doStoredProcedure2(valuesArray, ParamsArray, "Add-SearchCar");

        if (result == "Success")
            Label1.Visible = true;
        else
        {
            Label1.Text = result;
            Label1.Visible = true;
        }

        UserRolesDAL dal = new UserRolesDAL();

        var ghabourBackendUserCookie = Request.Cookies["GhabourBackendUser"];
        var hasKeys = ghabourBackendUserCookie.HasKeys;
        if (hasKeys)
        {
            var value = ghabourBackendUserCookie.Value;
            var paramsSplitted = value.Split('&');
            if (paramsSplitted.Length > 0)
            {
                var userIdParam = paramsSplitted[0];
                var paramKeyValue = userIdParam.Split('=');
                if (paramKeyValue.Length > 0)
                {
                    var userIdValueStr = paramKeyValue[1];
                    var userIdInt = Convert.ToInt32(userIdValueStr);
                    dal.AddItem("Add Premuim Selection Model", userIdInt, chassisno);
                }
            }
        }
    }

    public string chkString(string value)
    {
        string str;
        str = value.Replace(",", "");
        str = str.Replace(".", "");
        return str;
    }


    public string calcSize(string value)
    {
        string str;
        int i;
        str = value.Replace("L", "");
        str = value.Replace(" ", "");
        i = int.Parse(str);
        i = (i * 1000);
        str = i.ToString();
        return str;
    }

    protected void btn_delete_Click(object sender, EventArgs e)
    {
        chassisno = inp_del_chassis.Value;
        Database datab = new Database();

        string[] ParamsArray = new string[1] { "@ChassisNo" };
        string[] valuesArray = new string[1] { chassisno };
        string result = datab.doStoredProcedure2(valuesArray, ParamsArray, "Delete-SearchCar");

        if (result == "Success")
        {
            Label2.Visible = true;
            UserRolesDAL dal = new UserRolesDAL();
            var ghabourBackendUserCookie = Request.Cookies["GhabourBackendUser"];
            var hasKeys = ghabourBackendUserCookie.HasKeys;
            if (hasKeys)
            {
                var value = ghabourBackendUserCookie.Value;
                var paramsSplitted = value.Split('&');
                if (paramsSplitted.Length > 0)
                {
                    var userIdParam = paramsSplitted[0];
                    var paramKeyValue = userIdParam.Split('=');
                    if (paramKeyValue.Length > 0)
                    {
                        var userIdValueStr = paramKeyValue[1];
                        var userIdInt = Convert.ToInt32(userIdValueStr);
                        dal.AddItem("Delete Premuim Selection Model", userIdInt, chassisno);
                    }
                }
            }
        }
        else
        {
            Label2.Text = result;
            Label2.Visible = true;
        }
    }

    protected void btn_goToDelete_Click(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 1;
    }
    protected void btn_goToSubmit_Click(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 0;
    }
}
