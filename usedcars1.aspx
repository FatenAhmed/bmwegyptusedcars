﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="usedcars.aspx.cs" Inherits="usedcars" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Welcome to Approved Used BMW</title>
    <link type="text/css" rel="stylesheet" href="css/main.css" />

    <script language="JavaScript" src="js/common5.js"></script>



    <script type="text/javascript" src="js/cufon.js"></script>
    <script type="text/javascript" src="js/bmwfont.js"></script>
    <!-- SLIDER RANGE SCRIPTS  -->
    <script src="js/jquery-1.js"></script>
    <script src="js/jquery-ui-1.js"></script>

    <script>
        var formatNumber = function (number) {
            number += "";
            var parts = number.split('.');
            var integer = parts[0];
            var decimal = parts.length > 1 ? '.' + parts[1] : '';
            var regex = /(\d+)(\d{3})/;
            while (regex.test(integer)) {
                integer = integer.replace(regex, '$1' + ',' + '$2');
            }
            return integer + decimal;
        };

    </script>

    <script type="text/javascript"> 
        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 100000,
                step: 5000,
                values: [75, 100000],
                slide: function (event, ui) {
                    $("#amount-min").val("" + formatNumber(ui.values[0]));
                    $("#amount-max").val("" + formatNumber(ui.values[1]));
                }
            });
            $("#amount-min").val("" + formatNumber($("#slider-range").slider("values", 0)));
            $("#amount-max").val("" + formatNumber($("#slider-range").slider("values", 1)));
        });


    </script>

    <!-- CUFON TYPEFACE -->
    <script type="text/javascript">
        Cufon.replace('h1,h2');

    </script>

</head>
<body onload="init_makemodel(document.form1);">
    <form id="Form1" name="form1" class="sfForm" runat="server">

        <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BMW_UsedCars_LandingPage
URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2011
-->
        <script type="text/javascript">
            var axel = Math.random() + "";
            var a = axel * 10000000000000;
            document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_u866;ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
        </script>
        <noscript>
            <iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_u866;ord=1?" width="1" height="1" frameborder="0"></iframe>
        </noscript>

        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->


        <div class="bmw-overview-wrapper">
            <!-- contenthead -->
            <div class="bmwauc-banner">
                <img border="0" alt="Welcome to Approved Used BMW" src="images/banner_1024.jpg" />
            </div>
            <!-- contenthead end -->
            <!-- BMW AUC CONTENT -->
            <div id="bmwauc-content">
                <div id="bmwauc-content-left" class="bmwauc-content-left">

                    <div id="bmwauc-content-left-content-2" class="bmwauc-content-left-content-2">
                        <h2>JOY IS TIMELESS.</h2>
                        <p>When you are looking for a used BMW you should search no further than a BMW Premium Selection Approved Used Car.</p>
                        <p>Every car undergoes our 360 degree technical and optical inspection with full road test and our customers receive an extensive array of specific programme benefits such as <strong>12 Month unlimited mileage BMW warranty</strong>.</p>


                        <p class="bmwauc-content-intro"><a target="_top" class="bmwauc-button-blue-arrow-left" href="http://www.bmw-eg.com/eg/en/usedvehicles/premiumselection/bps/benefits.html">Discover more of the benefits</a></p>

                    </div>
                </div>



                <input name="MakeID" value="3" type="hidden">
                <input name="xMakeID" value="" type="Hidden">

                <!-- BMW AUC CONTENT END-->
                <div class="bmwauc-content-right">

                    <div style="margin-top: 30px;"></div>
                    <!-- tab -->
                    <div class="bmwauc-tab-bar-container"><span class="auc-tab"><a href="##" class="auc-tab">Quick search</a> </span><a target="_top" href="http://www.bmw-eg.com/eg/en/usedvehicles/premiumselection/bps/search.html" class="simple-link">Make a detailed search</a> </div>
                    <!-- Quick Search Content -->
                    <div class="bmwauc-tab-content-container">

                        <div class="controls-block">
                            <div class="controls">
                                <div class="controls-column controls-column-1">
                                    <div class="auc-select">
                                        <h5>BMW Series</h5>
                                        <select name="ModelID" class="select" title="Select model" id="input_series" runat="server">
                                            <option value="any" selected>Any Series</option>
                                            <option value="1 Series">1 Series</option>
                                            <option value="3 Series">3 Series</option>
                                            <option value="5 Series">5 Series</option>
                                            <option value="6 Series">6 Series</option>
                                            <option value="7 Series">7 Series</option>
                                            <option value="M3">M3</option>
                                            <option value="M5">M5</option>
                                            <option value="M6">M6</option>
                                            <option value="X1">X1</option>
                                            <option value="X3">X3</option>
                                            <option value="X5">X5</option>
                                            <option value="X6">X6</option>
                                            <option value="Z4">Z4</option>
                                            <option value="I Series">I</option>
                                            <option value="X7">X7</option>
                                            <option value="8 Series">8 Series</option>
                                        </select>

                                        <input name="xModelID" value="" type="Hidden">
                                    </div>
                                    <br />
                                    <div class="auc-select">
                                        <h5>Year</h5>
                                        <select name="Year" title="Select year" class="select" id="input_year" runat="server">
                                            <option value="Any year">Any year</option>

                                            <option value="2005">2005</option>
                                            <option value="2006">2006</option>
                                            <option value="2007">2007</option>
                                            <option value="2008">2008</option>
                                            <option value="2009">2009</option>
                                            <option value="2010">2010</option>
                                            <option value="2011">2011</option>

                                        </select>
                                    </div>
                                    <div class="input-text-required"></div>

                                </div>


                                <div class="controls-column controls-column-2">
                                    <div class="auc-select">
                                        <h5>No. of Doors</h5>
                                        <select name="bodyID" title="Select body" class="select" id="input_doors" runat="server">
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>


                                    <br />
                                    <div class="auc-select">
                                        <h5>Your budget range</h5>
                                        <div id="slider-range"></div>
                                        <input type="text" name="Price_Min" id="amount-min" />

                                        <input type="text" name="Price_Max" id="amount-max" />
                                    </div>
                                </div>
                                <div class="controls-column controls-column-3">
                                    <div class="auc-select">
                                        <h5>Dealer</h5>
                                        <select name="countyID" title="Select county" class="select" id="input_dealer" runat="server">
                                            <option value="Any">Any</option>
                                            <option value="Bavarian Auto Company - Alex Branch">Bavarian Auto Company - Alex Branch</option>
                                            <option value="Bavarian Auto - Almaza">Bavarian Auto - Almaza</option>
                                            <option value="Bavarian Auto - Desert Road">Bavarian Auto - Desert Road</option>
                                            <option value="Bavarian Auto - Katameya4">Bavarian Auto - Katameya4</option>
                                            <option value="Bavarian Auto - Mohandessin">Bavarian Auto - Mohandessin</option>
                                            <option value="Bavarian Automotive company">Bavarian Automotive company</option>
                                        </select>
                                    </div>
                                    <br />

                                    <div class="quick-view-now">
                                        <!--   <input class="auc-blue-button" value="View now" onclick="submit_makemodel(document.form1);" type="Submit">  -->
                                        <asp:Button ID="btn_submit" class="auc-blue-button" runat="server"
                                            type="Submit" Text="View Now" OnClick="btn_submit_Click" />
                                    </div>
                                </div>
                                <div class="control-block-separator" style="height: 10px;"></div>
                            </div>
                            <!--bmwauc-content-right END-->
                        </div>
                    </div>
                </div>


                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <!-- end of content -->
            <div id="content-big-nav-stripe" class="content-big-nav-stripe-wide-homepage"></div>
            <div id="content-big-nav" class="content-big-nav-container-wide">



                <div id="sub-nav" class="sub-nav-container-wide">
                    <h3><a href="http://www.bmw-eg.com/eg/en/general/ecom_uib/dealer_locator/content.html" target="_top">Dealer Locator</a> </h3>
                    <div class="menu-item short-menu-item"><a class="menuitem" href="http://www.bmw-eg.com/eg/en/general/ecom_uib/dealer_locator/content.html" target="_top">Find your dealer</a> </div>
                </div>

                <div id="sub-nav" class="sub-nav-container-wide">
                    <h3><a href="http://www.bmw-eg.com/eg/en/usedvehicles/premiumselection/bps/wewantyourbmw.html" target="_top">We Want Your BMW</a> </h3>

                    <div class="menu-item short-menu-item"><a class="menuitem" href="http://www.bmw-eg.com/eg/en/usedvehicles/premiumselection/bps/wewantyourbmw.html" target="_top">BMW dealers want your BMW</a> </div>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</body>

</html>
