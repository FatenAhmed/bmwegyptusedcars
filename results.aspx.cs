﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class results : System.Web.UI.Page
{
    DataSet Ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        string[] ParamsArray = { };
        string[] valuesArray = {};
        Database datab = new Database();
        Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-WantCars");

        if (Ds.Tables.Count > 0)
        {
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        else
        { Label1.Visible = true; }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btn_export_Click(object sender, EventArgs e)
    {
        ExportToExcel("Requests.xls", GridView1);
    }

    public void ExportToExcel( string filename , GridView gv)
    {
       
         Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        Response.Charset = "utf-8";
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        this.EnableViewState = false;
        
        StringWriter sw = new StringWriter();
         HtmlTextWriter htw = new HtmlTextWriter(sw);
        gv.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
}
