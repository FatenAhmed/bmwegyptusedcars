﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class searchresults_used : System.Web.UI.Page
{
    string series;
    string model;
    string keywords;
    string color;
    string engine_min;
    string engine_max;
    string year_min;
    string year_max;
    string governarate;
    string dealer;
    string sort;
    string perPage;
    string price_min;
    string price_max;
    string doorsnumber;
    DataSet Ds;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.PreviousPage != null)
        {

            DropDownList dropSeries = (DropDownList)Page.PreviousPage.FindControl("drop_series");
            if (dropSeries != null) series = dropSeries.SelectedValue;
            else series = "any";

            DropDownList dropColor = (DropDownList)Page.PreviousPage.FindControl("drop_color");
            if (dropColor != null) color = dropColor.SelectedValue;
            else color = "any";

            DropDownList dropDoors = (DropDownList)Page.PreviousPage.FindControl("drop_doors");
            if (dropDoors != null) doorsnumber = dropDoors.SelectedValue;
            else doorsnumber = "any";

            DropDownList dropEngineMin = (DropDownList)Page.PreviousPage.FindControl("drop_engine_min");
            if (dropEngineMin != null) engine_min = dropEngineMin.SelectedValue;
            else engine_min = "0";

            DropDownList dropEngineMax = (DropDownList)Page.PreviousPage.FindControl("drop_engine_max");
            if (dropEngineMax != null) engine_max = dropEngineMax.SelectedValue;
            else engine_max = "9999999";

            DropDownList dropYearMin = (DropDownList)Page.PreviousPage.FindControl("drop_year_min");
            if (dropYearMin != null) year_min = dropYearMin.SelectedValue;
            else year_min = "1950";

            DropDownList dropYearMax = (DropDownList)Page.PreviousPage.FindControl("drop_year_max");
            if (dropYearMax != null) year_max = dropYearMax.SelectedValue;
            else year_max = "2050";

            HtmlInputText InputPriceMin = (HtmlInputText)Page.PreviousPage.FindControl("amount_min");
            if (InputPriceMin != null)
            {
                if (InputPriceMin.Value != "") price_min = ChkString(InputPriceMin.Value);
                else price_min = "0";
                HtmlInputText InputPriceMax = (HtmlInputText)Page.PreviousPage.FindControl("amount_max");
                if (InputPriceMax.Value != "") price_max = ChkString(InputPriceMax.Value);
                else price_max = "9999999";
            }
            else
            {
                TextBox txtboxPriceMin = (TextBox)Page.PreviousPage.FindControl("txt_price_min");
                if (txtboxPriceMin.Text != "") price_min = txtboxPriceMin.Text;
                else price_min = "0";
                TextBox txtboxPriceMax = (TextBox)Page.PreviousPage.FindControl("txt_price_max");
                if (txtboxPriceMax.Text != "") price_max = txtboxPriceMax.Text;
                else price_max = "9999999";
            }

            DropDownList dropCountry = (DropDownList)Page.PreviousPage.FindControl("drop_country");
            if (dropCountry != null) governarate = dropCountry.SelectedValue;
            else governarate = "any";

            DropDownList dropDealer = (DropDownList)Page.PreviousPage.FindControl("drop_dealer");
            if (dropDealer != null) dealer = dropDealer.SelectedValue;
            else dealer = "any";


            string[] ParamsArray = { "@Series", "@Governarate", "@Dealer", "@DoorsNumber", "@Color", "Year_Min", "Year_Max", "Price_Min", "Price_Max", "Engine_Min", "Engine_Max" };
            string[] valuesArray = { series, governarate, dealer, doorsnumber, color, year_min, year_max, price_min, price_max, engine_min, engine_max };

            Database datab = new Database();

            Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-ThumbsUsed");
            //Get-Thumbs
            if (Ds.Tables.Count > 0)
            {
                HomeSearchPics.DataSource = Ds;
                if (Ds.Tables[0].Rows.Count == 1)
                    td_noOfRows.InnerText = "Displaying 1 result";
                else
                    td_noOfRows.InnerText = "Displaying " + Ds.Tables[0].Rows.Count.ToString() + " results";

                HomeSearchPics.DataBind();
            }
        }


    }

    public string ChkString(string word)
    {
        string number = word.Trim();
        number = number.Replace(",", "");
        return number;
    }
}



