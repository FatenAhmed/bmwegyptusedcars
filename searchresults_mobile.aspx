﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="searchresults_mobile.aspx.cs" Inherits="searchresults" %>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Results</title>
    <link rel="stylesheet" href="search_files/BMW_usedcars_Responsive.css">
    <link href="searchResults_files/interestform.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jquery-1.11.2.min.js"></script>

<script type="text/javascript" src="contentFrameResizer.js"></script>
<script type="text/javascript" src="iframeIntegrationLib.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29788102-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <script type="text/javascript" src="searchResults_files/flashobject.js"></script>
    <!-- [39] external.bmw.ie/usedcars/index.cfm -->
    <!-- {sxml:d406,p125,t531 URL: MakeID=3&Year_Min=2005&CategoryID=1&GroupID=890&UpdatePeriod=30&SortBy=MakeAsc&ResultFormat=gallery&maxrows=50&startRow=1 }-->
    <!-- base href="http://external.bmw.ie/usedcars/index.cfm" -->
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>

<body>

    <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: BMW_Search_Tag
    URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 03/11/2011
    -->
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[not defined];u2=[not defined];u4=[not defined];u3=[not defined];ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
    </script>
    <iframe src="searchResults_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
    <noscript>
        <iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[not defined];u2=[not defined];u4=[not defined];u3=[not defined];ord=1?" width="1" height="1" frameborder="0"></iframe>
    </noscript>
    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->







    <br>
    <br>

    <div class="maincontainer">
        <br>


        <h1>Search Results</h1>
        <div class="rlcriteria">

            <div style="float: left" class="rlcriteria_details">
                You searched for: BMW Cars <a href="#"></a>

            </div>
            <div style="float: right" class="style_critera_form_div" align="right">
            </div>

        </div>
        <br clear="all">

        <div class="search-results-container">
        <table class="rldisplaying" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>

                    <td class="rldisplaying_left" id="td_noOfRows" runat="server">Displaying records 1 to 50 of 367 records found
		
	&nbsp;</td>


                    <td class="rldisplaying_right">

                        <!--		<span class="unavailable">&lt; Previous</span>
		
			
			<span class="unavailable">|</span>
		
			
			<a href="#">Next &gt;</a>
	-->


                    </td>



                </tr>
            </tbody>
        </table>

        <br>
        <div class="rl">

            <!--
<div class="rlSort">Re-sort by: 
	<a title="click to re-sort by Make/Model" href="#" class="rlSort">Make/Model</a>
    	| <a title="click to re-sort by Year" href="#" class="rlSort">Year</a>
    	| <a title="click to re-sort by Engine" href="#" class="rlSort">Engine</a>
    	| <a title="#" class="rlSort">Colour</a>
    	| <a title="click to re-sort by Mileage" href="#" class="rlSort">Mileage</a>
		| <a title="click to re-sort by Dealer" href="#" class="rlSort">Dealer</a>
		| <a title="click to re-sort by cars that have Photos" href="#" class="rlSort">Photo</a> 
		| <a title="click to re-sort by Price" href="#" class="rlSort">Price</a>  
</div>-->

            <div class="ucGalleryContainer">

                <asp:Repeater ID="HomeSearchPics" runat="server">

                    <ItemTemplate>
                        <div class="ucGalleryItem">
                            <a class="ucImageLink" href="car.aspx?Id=<%# Eval("Id")%>">
                                <img alt="<%# Eval("Series")%>  <%# Eval("Model")%>  <%# Eval("IYear")%>" src="<%# Eval("ImagesThumb")%>" border="0">
                                <div class="ucGalleryPicCount"><small>&nbsp;6 images</small></div>
                            </a>

                            <div class="ucGalleryTitle">

                                <a class="ucDealerTitle"><%# Eval("Series")%>  <%# Eval("Model")%>  <%# Eval("IYear")%></a>


                                <a href="car.aspx?Id=<%# Eval("Id")%>">View Details</a>
                            </div>

                            <div class="ucGalleryPrice">
                                <%# Eval("Price")%> EGP
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>



                <!--
	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 118d Sport" src="searchResults_files/media_042.jpg" border="0"/>
            <div class="ucGalleryPicCount"><small>&nbsp;6 images</small></div>
        </a>
			
			
			
			<div class="ucGalleryTitle">		
                <a class="ucDealerTitle">Murphy and Gunn (BMW) Dublin</a>
                <a href="#"> (2011) BMW 1 Series 118d Sport</a>
			</div>

		
        	<div class="ucGalleryPrice"> L.E.29,950 </div>
	</div>		
	-->
                <!--
	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 118d Sport" src="searchResults_files/media_048.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>
        <div class="ucGalleryTitle">		
            <a class="ucDealerTitle">Kearys BMW Cork</a>
            <a href="#"> (2011) BMW 1 Series 118d Sport</a>
        </div>

    
        <div class="ucGalleryPrice">L.E.28,900 </div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 118d Sport" src="searchResults_files/media_013.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
        </a>

        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Kearys BMW Cork</a>
            <a href="#"> (2011) BMW 1 Series 118d Sport</a>
        </div>
        <div class="ucGalleryPrice">L.E.28,500</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 118d Sport" src="searchResults_files/media_004.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>
        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Frank Keane BMW Dublin</a>
            <a href=""> (2011) BMW 1 Series 118d Sport</a>
        </div>
        <div class="ucGalleryPrice">L.E.27,995 </div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 1 Series 118d Sport" src="searchResults_files/media_027.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
        </a>
        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Murphy and Gunn (BMW) Dublin</a>
            <a href="#"> (2011) BMW 1 Series 118d Sport</a>
        </div>
        <div class="ucGalleryPrice">L.E.27,750</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 118d Sport" src="searchResults_files/media.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
        </a>
			
        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Kearys BMW Cork</a>
            <a href="#"> (2011) BMW 1 Series 118d Sport</a>
        </div>
        <div class="ucGalleryPrice">L.E.27,500</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 1 Series 116i Sport 5-door" src="searchResults_files/media_012.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
				
        </a>

        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Murphy and Gunn (BMW) Dublin</a>
            <a href="#"> (2011) BMW 1 Series 116i Sport</a>
        </div>
        <div class="ucGalleryPrice"> L.E.24,950 </div>
	</div>		

	<div class="ucGalleryItem">
    	<a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 116d Sport 5-door" src="searchResults_files/media_034.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>
        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Joe Duffy BMW Dublin</a>
            <a href="#"> (2011) BMW 1 Series 116d Sport</a>
        </div>

        <div class="ucGalleryPrice"> L.E.23,995</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 1 Series 116d Sport 5-door" src="searchResults_files/media_046.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>

        <div class="ucGalleryTitle">		
			<a class="ucDealerTitle">Joe Duffy BMW Dublin</a>
            <a href="#"> (2011) BMW 1 Series 116d Sport</a>
        </div>
        <div class="ucGalleryPrice"> L.E.23,995 </div>
		

	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 3 Series 318d Exclusive Edition" src="searchResults_files/media_035.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>
        
        <div class="ucGalleryTitle">		
            <a class="ucDealerTitle">Morrison BMW Tipperary</a>
            <a href="#">(2011) BMW 3 Series 318d Exclu</a>
        </div>

        <div class="ucGalleryPrice">L.E.35,995</div>
	</div>		

	<div class="ucGalleryItem">
            <a class="ucImageLink" href="#">
                <img alt="BMW 3 Series 320d Exclusive Edition" src="searchResults_files/media_007.jpg" border="0">
                <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
            </a>

			<div class="ucGalleryTitle">		
                <a class="ucDealerTitle">Kearys BMW Cork</a>
                <a href="#">(2011) BMW 3 Series 320d Exclu</a>
			</div>

		
        	<div class="ucGalleryPrice"> L.E.35,900</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 3 Series 320d Efficient Dynamics" src="searchResults_files/media_031.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>

        <div class="ucGalleryTitle">		
            <a class="ucDealerTitle">Joe Duffy BMW Dublin</a>
            <a href="#">(2011) BMW 3 Series 320d Effic</a>
        </div>

        <div class="ucGalleryPrice"> L.E.34,995 </div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 3 Series 318d Exclusive Edition" src="searchResults_files/media_005.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;8 images</small></div>
        </a>
        
        <div class="ucGalleryTitle">		
            <a class="ucDealerTitle">Joe Duffy BMW Dublin</a>
            <a href="#">(2011) BMW 3 Series 318d Exclu</a>
        </div>

    
        <div class="ucGalleryPrice">L.E.34,995</div>
	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
			<img alt="BMW 3 Series 320d Exclusive Edition" src="searchResults_files/media_024.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
        </a>
			
			
			
			<div class="ucGalleryTitle">		
                <a class="ucDealerTitle">J Donohoe BMW Wexford</a>
                <a href="#"> (2011) BMW 3 Series 320d Exclu</a>
			</div>

		
        	<div class="ucGalleryPrice"> L.E.33,950 </div>
		

	</div>		

	<div class="ucGalleryItem">
        <a class="ucImageLink" href="#">
            <img alt="BMW 3 Series 316d Edition" src="searchResults_files/media_019.jpg" border="0">
            <div class="ucGalleryPicCount"><small>&nbsp;9 images</small></div>
        </a>
        <div class="ucGalleryTitle">		
            <a class="ucDealerTitle">N. Conlan &amp; Sons Limerick BMW Limerick</a>
            <a href="#"> (2011) BMW 3 Series 316d Editi</a>
        </div>

        <div class="ucGalleryPrice"> L.E.33,950 </div>
	</div>		

	<div class="ucGalleryItem">
 	  	    	<a class="ucImageLink" href="#">
                    <img alt="BMW 3 Series 316d Edition" src="searchResults_files/media_025.jpg" border="0">
                    <div class="ucGalleryPicCount"><small>&nbsp;6 images</small></div>
                </a>
			
			
			
			<div class="ucGalleryTitle">		
                <a class="ucDealerTitle">N. Conlan and Sons BMW Kildare</a>
                <a href="#"> (2011) BMW 3 Series 316d Editi</a>
			</div>

		
        	<div class="ucGalleryPrice">L.E.33,950</div>
		

	</div>		
-->

            </div>
        </div>
        </div>
        <br>


        <!--
<table class="rldisplaying" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
      <tr> 
    
        <td class="rldisplaying_left">&nbsp;
            
        </td>
        <td class="rldisplaying_right">
            <span class="unavailable">&lt; Previous</span>
            <span class="unavailable">|</span>
            <a href="#">Next &gt;</a>
        </td>
      </tr>
</tbody></table>
-->

    </div>
</body>
</html>
<!---->
