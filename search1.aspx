﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="search" %>


<html>
<head>
    <title>Search Used Cars</title>

    <link rel="stylesheet" href="search_files/BMW_usedcars.css" />

    <link href="search_files/interestform.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" src="search_files/flashobject.js"></script>
    <!-- [40] external.bmw.ie/usedcars/index.cfm -->
    <script language="JavaScript" src="search_files/common5.js"></script>

    <script language="JavaScript" src="search_files/BaseColours.js"></script>

    <!-- base href="http://external.bmw.ie/usedcars/index.cfm" -->
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>

<body onload="init_makemodel(document.form1);FillBaseColours(); init_countygarage(document.form1,true);">
    <!--
<div style="width:100%; height:98px; background:url(images/top-nav.png) no-repeat; position:absolute; top:0; left:0;"></div>
-->
    <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: BMW_Search_Tag
    URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 03/11/2011
    -->
    <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[not defined];u2=[not defined];u4=[not defined];u3=[not defined];ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
    </script>
    <iframe src="search_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
    <noscript>
        <iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[not defined];u2=[not defined];u4=[not defined];u3=[not defined];ord=1?" width="1" height="1" frameborder="0"></iframe>
    </noscript>
    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

    <br>
    <br>

    <div class="maincontainer">
        <br>

        <h1>BMW Premium Selection Search </h1>
        <p>
        </p>
        <div class="bps-text">

            <!--The BMW National Used Car Event is ON from Thursday 14 - Saturday 16 May!  Along with the widest choice of the best used cars, your BMW dealer will also include BMW's 2 year unlimited mileage warranty and, as a first for Ireland, 2 years BMW Service Inclusive - ensuring you have the best possible peace of mind with your BMW Premium Selection approved used car purchase!-->

            BMW Premium Selection Approved Used Cars offers you the choice of the 
best used cars from Egypt. Enter specific criteria such as 
model, colour, fuel type for a detailed search of all BMW Premium 
Selection cars nationwide or from preferred dealers.
        </div>
        <p></p>
        <br>

        <!-- begin used cars advanced search form -->

        <div class="sfOuterContainer">

            <div class="sfContainer">


                <form id="Form1" name="form1" class="sfForm" runat="server">
                    <!-- action="/usedcars/index.cfm?fuseaction=search" method="post" -->
                    <div class="sfElementTitle">CAR DESCRIPTION</div>

                    <input name="MakeID" value="3" type="hidden">
                    <input name="xMakeID" value="" type="Hidden">
                    <div class="sfElement">
                        <div class="sfCaption">Series:</div>
                        <div class="sfInput">

                            <asp:DropDownList name="ModelID" TabIndex="3" class="sfmenu" ID="drop_series" runat="server">
                                <asp:ListItem Value="any">Any Series</asp:ListItem>
                                <asp:ListItem Value="1 Series">1 Series</asp:ListItem>
                                <asp:ListItem Value="3 Series">3 Series</asp:ListItem>
                                <asp:ListItem Value="5 Series">5 Series</asp:ListItem>
                                <asp:ListItem Value="6 Series">6 Series</asp:ListItem>
                                <asp:ListItem Value="7 Series">7 Series</asp:ListItem>
                                <asp:ListItem Value="M3">M3</asp:ListItem>
                                <asp:ListItem Value="M5">M5</asp:ListItem>
                                <asp:ListItem Value="M6">M6</asp:ListItem>
                                <asp:ListItem Value="X1">X1</asp:ListItem>
                                <asp:ListItem Value="X3">X3</asp:ListItem>
                                <asp:ListItem Value="X4">X5</asp:ListItem>
                                <asp:ListItem Value="X5">X6</asp:ListItem>
                                <asp:ListItem Value="Z4">Z4</asp:ListItem>
                                <asp:ListItem Value="I Series">I</asp:ListItem>
                                <asp:ListItem Value="X7">X7</asp:ListItem>
                                <asp:ListItem Value="8 Series">8 Series</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Model:</div>
                        <div class="sfInput">
                            <asp:TextBox ID="txtbox_model" TabIndex="4" class="sfmenu" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Keyword:</div>
                        <div class="sfInput">

                            <asp:TextBox ID="txtbox_keyword" TabIndex="4" class="sfmenu" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Colour:</div>
                        <div class="sfInput">
                            <!--		<select name="ColourID" tabindex="5" class="sfmenu">
						<option selected="selected" value="">Any</option>
					<option value="1">BLACK</option><option value="4">BLUE</option><option value="10">BROWN</option><option value="12">GOLD</option><option value="5">GREEN</option><option value="6">GREY</option><option value="9">ORANGE</option><option value="8">PURPLE</option><option value="3">RED</option><option value="7">SILVER</option><option value="2">WHITE</option><option value="11">YELLOW</option></select>
					<input name="xColourID" value="" type="Hidden"/>
					-->
                            <asp:DropDownList name="ColourID" TabIndex="5" class="sfmenu" ID="drop_color" runat="server">
                                <asp:ListItem Value="any">Any</asp:ListItem>
                                <asp:ListItem Value="Carbon Black metallic">Carbon Black metallic</asp:ListItem>
                                <asp:ListItem Value="Titanium Silver metallic">Titanium Silver metallic</asp:ListItem>
                                <asp:ListItem Value="Sparkling Graphite metallic">Sparkling Graphite metallic</asp:ListItem>
                                <asp:ListItem Value="Blue Water metallic">Blue Water metallic</asp:ListItem>
                                <asp:ListItem Value="Black Sapphire metallic">Black Sapphire metallic</asp:ListItem>
                                <asp:ListItem Value="Silver Grey metallic">Silver Grey metallic</asp:ListItem>
                                <asp:ListItem Value="Imperial Blue Brillant Effect metallic">Imperial Blue Brillant Effect metallic</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="sfElement">
                        <div class="sfCaption">No. of Doors:</div>
                        <div class="sfInput">

                            <!--	<select name="Doors" tabindex="6" class="sfmenu">
						<option selected="selected" value="">Any</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
					</select>
				-->
                            <asp:DropDownList name="Doors" TabIndex="6" class="sfmenu" ID="drop_doors" runat="server">
                                <asp:ListItem Value="any">Any</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>


                    <div class="sfElementTitle">ENGINE SIZE</div>

                    <div class="sfElement">
                        <div class="sfCaption">Minimum:</div>
                        <div class="sfInput">

                            <%--<select name="Engine_Min" tabindex="10" class="sfmenu">
						<option selected="selected" value="">Any</option>
								<option value="1600">1.6L </option>
                                <option value="1700">1.7L </option>
                                <option value="1800">1.8L </option>
                                <option value="1900">1.9L </option>
                                <option value="2000">2.0L </option>
                                <option value="2100">2.1L </option>
                                <option value="2200">2.2L </option>
                                <option value="2300">2.3L </option>
                                <option value="2400">2.4L </option>
                                <option value="2500">2.5L </option>
                                <option value="2600">2.6L </option>
                                <option value="2700">2.7L </option>
                                <option value="2800">2.8L </option>
                                <option value="2900">2.9L </option>
                                <option value="3000">3.0L </option>
                            </select>--%>
                            <asp:DropDownList ID="drop_engine_min" runat="server" name="Engine_Min" TabIndex="10" class="sfmenu">
                                <asp:ListItem Value="0">any</asp:ListItem>
                                <asp:ListItem Value="1600">1.6L</asp:ListItem>
                                <asp:ListItem Value="1700">1.7L</asp:ListItem>
                                <asp:ListItem Value="1800">1.8L</asp:ListItem>
                                <asp:ListItem Value="1900">1.9L</asp:ListItem>
                                <asp:ListItem Value="2000">2.0L</asp:ListItem>
                                <asp:ListItem Value="2100">2.1L</asp:ListItem>
                                <asp:ListItem Value="2200">2.2L</asp:ListItem>
                                <asp:ListItem Value="2300">2.3L</asp:ListItem>
                                <asp:ListItem Value="2400">2.4L</asp:ListItem>
                                <asp:ListItem Value="2500">2.5L</asp:ListItem>
                                <asp:ListItem Value="2600">2.6L</asp:ListItem>
                                <asp:ListItem Value="2700">2.7L</asp:ListItem>
                                <asp:ListItem Value="2800">2.8L</asp:ListItem>
                                <asp:ListItem Value="2900">2.9L</asp:ListItem>
                                <asp:ListItem Value="3000">3.0L</asp:ListItem>
                                <asp:ListItem Value="3100">3.1L</asp:ListItem>
                                <asp:ListItem Value="3200">3.2L</asp:ListItem>
                                <asp:ListItem Value="3300">3.3L</asp:ListItem>
                                <asp:ListItem Value="3400">3.4L</asp:ListItem>
                                <asp:ListItem Value="3500">3.5L</asp:ListItem>
                                <asp:ListItem Value="3600">3.6L</asp:ListItem>
                                <asp:ListItem Value="3700">3.7L</asp:ListItem>
                                <asp:ListItem Value="3800">3.8L</asp:ListItem>
                                <asp:ListItem Value="3900">3.9L</asp:ListItem>
                                <asp:ListItem Value="4000">4.0L</asp:ListItem>
                                <asp:ListItem Value="4100">4.1L</asp:ListItem>
                                <asp:ListItem Value="4200">4.2L</asp:ListItem>
                                <asp:ListItem Value="4300">4.3L</asp:ListItem>
                                <asp:ListItem Value="4400">4.4L</asp:ListItem>
                                <asp:ListItem Value="4500">4.5L</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="sfElement">
                        <div class="sfCaption">Maximum:</div>
                        <div class="sfInput">
                            <%--<select name="Engine_Max" tabindex="11" class="sfmenu">
						<option selected="selected" value="">Any</option>
								<option value="1600">1.6L</option>
								<option value="1700">1.7L</option>
								<option value="1800">1.8L</option>
								<option value="1900">1.9L</option>
								<option value="2000">2.0L</option>
								<option value="2100">2.1L</option>
								<option value="2200">2.2L</option>
								<option value="2300">2.3L</option>
								<option value="2400">2.4L</option>
								<option value="2500">2.5L</option>
								<option value="2600">2.6L</option>
								<option value="2700">2.7L</option>
								<option value="2800">2.8L</option>
								<option value="2900">2.9L</option>
								<option value="3000">3.0L</option>
					</select>--%>

                            <asp:DropDownList ID="drop_engine_max" runat="server" name="Engine_Min" TabIndex="10" class="sfmenu">
                                <asp:ListItem Value="9999999">any</asp:ListItem>
                                <asp:ListItem Value="1600">1.6L</asp:ListItem>
                                <asp:ListItem Value="1700">1.7L</asp:ListItem>
                                <asp:ListItem Value="1800">1.8L</asp:ListItem>
                                <asp:ListItem Value="1900">1.9L</asp:ListItem>
                                <asp:ListItem Value="2000">2.0L</asp:ListItem>
                                <asp:ListItem Value="2100">2.1L</asp:ListItem>
                                <asp:ListItem Value="2200">2.2L</asp:ListItem>
                                <asp:ListItem Value="2300">2.3L</asp:ListItem>
                                <asp:ListItem Value="2400">2.4L</asp:ListItem>
                                <asp:ListItem Value="2500">2.5L</asp:ListItem>
                                <asp:ListItem Value="2600">2.6L</asp:ListItem>
                                <asp:ListItem Value="2700">2.7L</asp:ListItem>
                                <asp:ListItem Value="2800">2.8L</asp:ListItem>
                                <asp:ListItem Value="2900">2.9L</asp:ListItem>
                                <asp:ListItem Value="3000">3.0L</asp:ListItem>
                                <asp:ListItem Value="3100">3.1L</asp:ListItem>
                                <asp:ListItem Value="3200">3.2L</asp:ListItem>
                                <asp:ListItem Value="3300">3.3L</asp:ListItem>
                                <asp:ListItem Value="3400">3.4L</asp:ListItem>
                                <asp:ListItem Value="3500">3.5L</asp:ListItem>
                                <asp:ListItem Value="3600">3.6L</asp:ListItem>
                                <asp:ListItem Value="3700">3.7L</asp:ListItem>
                                <asp:ListItem Value="3800">3.8L</asp:ListItem>
                                <asp:ListItem Value="3900">3.9L</asp:ListItem>
                                <asp:ListItem Value="4000">4.0L</asp:ListItem>
                                <asp:ListItem Value="4100">4.1L</asp:ListItem>
                                <asp:ListItem Value="4200">4.2L</asp:ListItem>
                                <asp:ListItem Value="4300">4.3L</asp:ListItem>
                                <asp:ListItem Value="4400">4.4L</asp:ListItem>
                                <asp:ListItem Value="4500">4.5L</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="sfElementTitle">YEAR</div>

                    <div class="sfElement">
                        <div class="sfCaption">Oldest:</div>
                        <div class="sfInput">

                            <%--<select name="Year_Min" tabindex="13" class="sfmenu">
						<option selected="selected" value="">Any</option>
							<option value="1996">1996</option>
							<option value="1997">1997</option>
							<option value="1998">1998</option>
							<option value="1999">1999</option>
							<option value="2000">2000</option>
							<option value="2001">2001</option>
							<option value="2002">2002</option>
							<option value="2003">2003</option>
							<option value="2004">2004</option>
							<option value="2005">2005</option>
							<option value="2006">2006</option>
							<option value="2007">2007</option>
							<option value="2008">2008</option>
							<option value="2009">2009</option>
							<option value="2010">2010</option>
							<option value="2011">2011</option>
						
					</select>--%>

                            <asp:DropDownList ID="drop_year_min" runat="server" name="Year_Min" TabIndex="13" class="sfmenu">
                                <asp:ListItem Value="1950">any</asp:ListItem>
                                <asp:ListItem Value="2005">2005</asp:ListItem>
                                <asp:ListItem Value="2006">2006</asp:ListItem>
                                <asp:ListItem Value="2007">2007</asp:ListItem>
                                <asp:ListItem Value="2008">2008</asp:ListItem>
                                <asp:ListItem Value="2009">2009</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
                                <asp:ListItem Value="2011">2011</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Newest:</div>
                        <div class="sfInput">

                            <%--<select name="Year_Max" tabindex="14" class="sfmenu">
						<option selected="selected" value="">Any</option>
							<option value="2011">2011</option>
							<option value="2010">2010</option>
							<option value="2009">2009</option>
							<option value="2008">2008</option>
							<option value="2007">2007</option>
							<option value="2006">2006</option>
							<option value="2005">2005</option>
							<option value="2004">2004</option>
							<option value="2003">2003</option>
							<option value="2002">2002</option>
							<option value="2001">2001</option>
							<option value="2000">2000</option>
							<option value="1999">1999</option>
							<option value="1998">1998</option>
							<option value="1997">1997</option>
							<option value="1996">1996</option>
					</select> --%>

                            <asp:DropDownList ID="drop_year_max" runat="server" name="Year_Max" TabIndex="13" class="sfmenu">
                                <asp:ListItem Value="2050">any</asp:ListItem>
                                <asp:ListItem Value="2011">2011</asp:ListItem>
                                <asp:ListItem Value="2010">2010</asp:ListItem>
                                <asp:ListItem Value="2009">2009</asp:ListItem>
                                <asp:ListItem Value="2008">2008</asp:ListItem>
                                <asp:ListItem Value="2007">2007</asp:ListItem>
                                <asp:ListItem Value="2006">2006</asp:ListItem>
                                <asp:ListItem Value="2005">2005</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="sfElementTitle">PRICE L.E.</div>

                    <div class="sfElement">
                        <div class="sfCaption">Minimum:</div>
                        <div class="sfInput">
                            <%--<input name="Price_Min" tabindex="16" class="sfmenu" size="10" type="Text" id="input_price_min" runat="server" value="">
					<input name="Price_Min_integer" value="Minimum price entered was not an integer." type="hidden">--%>
                            <asp:TextBox ID="txt_price_min" TabIndex="16" class="sfmenu" size="10" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Maximum:</div>
                        <div class="sfInput">
                            <%--<input name="Price_Max" tabindex="17" class="sfmenu" size="10" type="Text" id="input_price_max" runat="server" value="">
					<input name="Price_Max_integer" value="Maximum price entered was not an integer." type="hidden">--%>
                            <asp:TextBox ID="txt_price_max" TabIndex="16" class="sfmenu" size="10" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="sfElementTitle">LOCATION</div>

                    <div class="sfElement">
                        <div class="sfCaption">Governarate:</div>
                        <div class="sfInput">

                            <%--	<select name="CountyID" tabindex="19" onChange="business_countygarage(document.form1, true);" class="sfmenu">
					    <option selected="selected" value="">Any Governarate</option>
                        <option value="6">Cairo</option>
                        <option value="10">Giza</option>
                        <option value="13">Alexanderia</option>
                        <option value="14">Suez</option>
                        <option value="18">El Menia</option>
                        <option value="26">Luxor</option>
                        <option value="27">El Sharkia</option>
                    </select>
					<input name="xCountyID" value="0" type="Hidden">--%>

                            <asp:DropDownList ID="drop_country" runat="server" name="CountryID" TabIndex="19" class="sfmenu">
                                <asp:ListItem Value="any">Any Governarate</asp:ListItem>
                                <asp:ListItem Value="Cairo">Cairo</asp:ListItem>
                                <asp:ListItem Value="Giza">Giza</asp:ListItem>
                                <asp:ListItem Value="Alexanderia">Alexanderia</asp:ListItem>
                                <asp:ListItem Value="Suez">Suez</asp:ListItem>
                                <asp:ListItem Value="El Menia">El Menia</asp:ListItem>
                                <asp:ListItem Value="Luxor">Luxor</asp:ListItem>
                                <asp:ListItem Value="El Sharkia">El Sharkia</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="sfElement">
                        <div class="sfCaption">Dealer:</div>
                        <div class="sfInput">

                            <%--<select name="BusinessID" tabindex="20" class="sfmenu">
					    <option selected="selected" value="">Any Dealer</option>
                        <option value="2278">Bavarian Auto Company - Alex Branch</option>    
                        <option value="2281">Bavarian Auto - Almaza</option>
                        <option value="2346">Bavarian Auto - Desert Road </option>
                        <option value="2282">Bavarian Auto - Katameya4</option>
                        <option value="2283">Bavarian Auto - Mohandessin</option>
                        <option value="2235">Bavarian Automotive company</option>
                    </select>
					<input name="xBusinessID" value="0" type="Hidden">--%>

                            <asp:DropDownList ID="drop_dealer" runat="server" name="BusinessID" TabIndex="20" class="sfmenu">
                                <asp:ListItem Value="any">Any Dealer</asp:ListItem>
                                <asp:ListItem Value="Bavarian Auto Company - Alex Branch">Bavarian Auto Company - Alex Branch</asp:ListItem>
                                <asp:ListItem Value="Bavarian Auto - Almaza">Bavarian Auto - Almaza</asp:ListItem>
                                <asp:ListItem Value="Bavarian Auto - Desert Road">Bavarian Auto - Desert Road</asp:ListItem>
                                <asp:ListItem Value="Bavarian Auto - Katameya4">Bavarian Auto - Katameya4</asp:ListItem>
                                <asp:ListItem Value="Bavarian Auto - Mohandessin">Bavarian Auto - Mohandessin</asp:ListItem>
                                <asp:ListItem Value="Bavarian Automotive company">Bavarian Automotive company</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>




                    <!-- <div class="sfElementTitle">RESULTS</div> -->

                    <!--	<div class="sfElement">
		<div class="sfCaption">Sort By:</div>
		<div class="sfInput">

				<%--	<select name="SortBy" tabindex="22" class="sfmenu">
						<option value="MakeAsc" selected="selected">Make/Model</option>
						<option value="priceAsc">Price - Low &gt; High</option>
						<option value="priceDesc">Price - High &gt; Low</option>
						<option value="yearAsc">Year - Old &gt; New</option>
						<option value="yearDesc">Year - New &gt; Old</option>
					</select>--%>
            <asp:DropDownList ID="drop_sort" runat="server" name="SortBy" tabindex="22" class="sfmenu">
            <asp:ListItem>Make/Model</asp:ListItem>
            <asp:ListItem>Price - Low &gt; High</asp:ListItem>
            <asp:ListItem>Price - High &gt; Low</asp:ListItem>
            <asp:ListItem>Year - Old &gt; New</asp:ListItem>
            <asp:ListItem>Year - New &gt; Old</asp:ListItem>
            </asp:DropDownList>

		</div>
	</div>
	-->

                    <!--	<div class="sfElement">
		<div class="sfCaption">Per Page:</div>
		<div class="sfInput">

					<%--<select name="MaxRows" tabindex="23" class="sfmenu">
						<option value="10">10</option>
						<option value="20">20</option>
						<option value="30">30</option>
						<option value="50" selected="selected">50</option>		
						<option value="100">100</option>		
					</select>--%>
					
            <asp:DropDownList ID="drop_perPage" runat="server"  name="MaxRows" tabindex="23" class="sfmenu">
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem Selected="True">50</asp:ListItem>
            <asp:ListItem>100</asp:ListItem>
            </asp:DropDownList>
		</div>
	</div>
	-->
                    <div class="sfSubmitButton">

                        <!--   <a href="searchResults.htm" style=" display:block; width:25px;-moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: url('UsedCars/images/auc-button-bg.gif') no-repeat scroll 100% 0 #4264AA;
    border-color: -moz-use-text-color #476CB5 #476CB5;  border-right: 1px solid #476CB5; border-style: none solid solid; border-width: 0 1px 1px; color: #FFFFFF; cursor: pointer; font-size: 11px; font-weight: bold; line-height: 14px; margin-top: 10px; padding: 5px 30px 4px 9px;" class="sfSubmitButton">Search</a>
      -->
                        <asp:Button ID="btn_search" runat="server" Text="Search"
                            OnClick="btn_search_Click" PostBackUrl="~/searchresults.aspx" />
                    </div>

                </form>

                <!-- end used cars advanced search form -->

            </div>

        </div>



    </div>
</body>
</html>
<!---->
