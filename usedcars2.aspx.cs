﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class usedcars : System.Web.UI.Page
{
    string series;
    string doors;
    string dealer;
    string year;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        series = input_series.Value;
        doors = input_doors.Value;
        dealer = input_dealer.Value.ToLower();
        year = input_year.Value;

        Session["Series"] = series;
        Session["DoorsNo"] = doors;
        Session["Dealer"] = dealer;
        Session["YearMin"] = year;

        Response.Redirect("searchresults.aspx");
    }
}
