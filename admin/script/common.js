﻿
    function showPopup(title, msg, icon)
    {
	    $get("ctl00_PageContentPlaceHolder_PopupMessage_Title").innerHTML = title;
	    $get("ctl00_PageContentPlaceHolder_PopupMessage_Content").innerHTML = msg;
	    $get("ctl00_PageContentPlaceHolder_PopupMessage_Icon").src = "images/dialog/" + icon + ".png";
	    $find('modalPopup').show();
    }

    // keeps track of the delete button for the row that is going to be removed
    var _source;
    // keep track of the popup div
    var _popup;
            
    function showConfirm(source, item, name)
    {
        this._source = source;
        this._popup = $find('confirmPopup');
            
        $get("ctl00_PageContentPlaceHolder_ConfirmMessage_Message").innerHTML = "Are you sure you want to delete the " + item + ": " + name + " ?";
        //  find the confirm ModalPopup and show it    
        this._popup.show();
    }

    function showConfirm2(source, item, enName, arName)
    {
        this._source = source;
        this._popup = $find('confirmPopup');
        
        var name;
        
        if(enName != "")
            name = enName;
        else
            name = arName;
            
        $get("ctl00_PageContentPlaceHolder_ConfirmMessage_Message").innerHTML = "Are you sure you want to delete the " + item + ": " + name + " ?";
        //  find the confirm ModalPopup and show it    
        this._popup.show();
    }
    
    function showConfirm3(source, msg)
    {
        this._source = source;
        this._popup = $find('confirmPopup');
            
        $get("ctl00_PageContentPlaceHolder_ConfirmMessage_Message").innerHTML = msg;
        //  find the confirm ModalPopup and show it    
        this._popup.show();
    }

    function okClick()
    {
        //  find the confirm ModalPopup and hide it    
        this._popup.hide();
        //  use the cached button as the postback source
        if(this._source.name == "")
        {
            name = this._source.id.replace(/_/g,"$");
            __doPostBack(name, '');
        }
        else
            __doPostBack(this._source.name, '');
    }
            
    function cancelClick()
    {
        //  find the confirm ModalPopup and hide it 
        this._popup.hide();
        //  clear the event source
        this._source = null;
        this._popup = null;
    }

    function validateUrl(sender, args)
    {
        var ptrn = /^(ht|f)tp(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?$/;
        if(args.Value != "http://" && !ptrn.test(args.Value))
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    function validateYouTubeUrl(sender, args)
    {
        var ptrn = /^http:\/\/(?:www\.)?youtube.com\/watch\?(?=.*v=\w+)(?:\S+)?$/;
        if(args.Value != "http://" && !ptrn.test(args.Value))
            args.IsValid = false;
        else
            args.IsValid = true;
    }
    
    function validateImage(sender, args)
    {
        var ptrn_ie = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
        var ptrn_ff = /^(\w[\w].*)+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
        
        if(ptrn_ie.test(args.Value) || ptrn_ff.test(args.Value))
            args.IsValid = true;
        else
            args.IsValid = false;
    }
    
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function isDecimalKey(tb, evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode;
       
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        
        if(charCode == 46 && tb.value.indexOf(".") > -1)
            return false;
         
        return true;
    }
    
    function validateNum(f) {
        if (!/^\d*$/.test(f.value)) {
            f.value = f.value.replace(/[^\d]/g,"");
        }
    } 
    
    function validateDec(f) {
        if (!/^\d*(\.\d+)?$/.test(f.value)) {
            f.value = f.value.replace(/[^\d]/g,"");
        }
    } 
    
    function removeZeros(f)
    {
        if(/^0+$/.test(f.value))
            f.value = "";
    }
    
    function ChangeAllCheckBoxStates(checkState,container)
    {
        var ct = document.getElementById(container);
        var checkboxes = ct.getElementsByTagName("input");
        
        for (var i = 1; i < checkboxes.length; i++)
            checkboxes[i].checked = checkState;
    }
   
    function ChangeHeader(container)
    {
        var ct = document.getElementById(container);
        var checkboxes = ct.getElementsByTagName("input");
        
        for (var i = 1; i < checkboxes.length; i++)
        {
            if (!checkboxes[i].checked)
            {
                checkboxes[0].checked = false;
                return;
            }
        }
            
        checkboxes[0].checked = true;
    }
            
    function checkUrl(sender, args)
    {
        if(args.Value == "" || args.Value == "http://")
            args.IsValid = false;
        else
            args.IsValid = true;
    }
    
    function selectTab(index)
    {
        $find('ctl00_PageContentPlaceHolder_PageTabs').set_activeTabIndex(index);    
    }