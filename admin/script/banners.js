﻿ /*********************************** Add Banner Functions *************************************/
        function bannerTypeChanged(value)
        {
            var image = $get("ctl00_PageContentPlaceHolder_BannerForm_trImage");
            var flash = $get("ctl00_PageContentPlaceHolder_BannerForm_trFlash");
            var script = $get("ctl00_PageContentPlaceHolder_BannerForm_trScript");
            
            switch(value)
            {
                case "1":
                    image.style.display = "table-row";
                    flash.style.display = "none";
                    
                    script.style.display = "none";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_Script").value = "";
                    break;
                case "2": 
                    image.style.display = "none";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_BannerUrl").value = "http://";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_UrlValidator").style.visibility = "hidden";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_NewWindow").checked = false;
                    
                    flash.style.display = "table-row";
                    
                    script.style.display = "none";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_Script").value = "";
                    break;
                case "3":
                    image.style.display = "none";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_BannerUrl").value = "http://";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_UrlValidator").style.visibility = "hidden";
                    $get("ctl00_PageContentPlaceHolder_BannerForm_NewWindow").checked = false;
                    
                    flash.style.display = "none";
                    script.style.display = "table-row";
                    break;
            }            
        }
        
        function validateImage(sender, args)
        {
            if($get("ctl00_PageContentPlaceHolder_BannerForm_BannerTypes").value == 1)
            {
                var ie = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
                var ff = /^(\w[\w].*)+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
                
                if(args.Value == "")
                {
                    showPopup("Image Banner Required", "Please upload the image for the banner.", "stop");
                    args.IsValid = false;
                }
                else if(!ie.test(args.Value) && !ff.test(args.Value))
                {
                    showPopup("Invalid Image Banner", "Only jpg, gif and png extensions are allowed.", "stop");
                    args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }
            else
                args.IsValid = true;
        }
        
        function validateFlash(sender, args)
        {
            if($get("ctl00_PageContentPlaceHolder_BannerForm_BannerTypes").value == 2)
            {
                var ie = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.swf|.SWF)$/;
                var ff = /^(\w[\w].*)+(.swf|.SWF)$/;
                
                if(args.Value == "")
                {
                    showPopup("Flash Banner Required", "Please upload the flash for the banner.", "stop");
                    args.IsValid = false;
                }
                else if(!ie.test(args.Value) && !ff.test(args.Value))
                {
                    showPopup("Invalid Flash Banner ", "Only swf extension is allowed.", "stop");
                    args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }
            else
                args.IsValid = true;
        }
        
        function validateScript(sender, args)
        {
            if($get("ctl00_PageContentPlaceHolder_BannerForm_BannerTypes").value == 3 && args.Value == "")
            {
               showPopup("Banner Script Required", "Please enter the script for the banner.", "stop");
               args.IsValid = false;
            }
            else
                args.IsValid = true;
        }
        
/*********************************** Edit Banner Functions *************************************/

        function validateImage2(sender, args)
        {
            if($get("ctl00_PageContentPlaceHolder_BannerForm_BannerTypes").value == 1)
            {
                var ie = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
                var ff = /^(\w[\w].*)+(.jpg|.JPG|.gif|.GIF|.png|.PNG)$/;
                
                if(!ie.test(args.Value) && !ff.test(args.Value))
                {
                    showPopup("Invalid Image Banner", "Only jpg, gif and png extensions are allowed.", "stop");
                    args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }
            else
                args.IsValid = true;
        }
        
        function validateFlash2(sender, args)
        {
            if($get("ctl00_PageContentPlaceHolder_BannerForm_BannerTypes").value == 2)
            {
                var ie = /^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))+(.swf|.SWF)$/;
                var ff = /^(\w[\w].*)+(.swf|.SWF)$/;
                
                if(!ie.test(args.Value) && !ff.test(args.Value))
                {
                    showPopup("Invalid Flash Banner ", "Only swf extension is allowed.", "stop");
                    args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }
            else
                args.IsValid = true;
        }
        