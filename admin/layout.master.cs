﻿using System;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class admin_layout : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoggedInUser.Text += BackEndUser.GetFullName();

            ArrayList sections = BackEndUser.GetUserRoleAndSections();
            //if (!sections[0].Equals("1"))
            //    ChangePassword.Visible = true;
        }
    }

    protected void btnSignout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        FormsAuthentication.RedirectToLoginPage();
        BackEndUser.RemoveCookie();
    }

    protected void SM_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        SM.AsyncPostBackErrorMessage = e.Exception.Message;
        if (e.Exception.InnerException != null)
            SM.AsyncPostBackErrorMessage += e.Exception.InnerException.Message;
    }
}
