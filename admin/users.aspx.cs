﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BMWUsedCarsLib;

public partial class admin_users : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int roleNo = BackEndUser.GetUserRole();
            // ArrayList sections = BackEndUser.GetUserRoleAndSections();
            if (roleNo != 1)
                PageView.ActiveViewIndex = 2;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ShowPopup(string title, string message, string icon)
    {
        PopupMessage.PopupTitle = title;
        PopupMessage.PopupContent = message;
        PopupMessage.PopupIcon = icon;
        PopupMessage.ShowBox();
    }

    protected void ObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }

    /**************************************************************************
                                PageListView Methods
     **************************************************************************/

    #region List View Events (DataBound, ItemCommand, ItemDeleted, SelectedIndexChanged)

    protected void UsersList_DataBound(object sender, EventArgs e)
    {
        btnAdd2.Visible = UsersList.Items.Count > 0;
    }

    protected void UsersList_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception != null)
            {
                ShowPopup("User Delete Error", "An error occurred, could not delete user. " + e.Exception.Message, "error");
                e.ExceptionHandled = true;
            }
            else
                ShowPopup("User Delete Error", "An error occurred, could not delete user.", "error");
        }
    }

    protected void UsersList_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*Literal role = UsersList.Items[UsersList.SelectedIndex].FindControl("Role") as Literal;
        Literal username = UsersList.Items[UsersList.SelectedIndex].FindControl("Username") as Literal;
        if (role.Text == "Administrator" && username.Text != User.Identity.Name)
        {
            ShowPopup("Edit User Canceled", "You are not allowed to edit this account.", "stop");
            UsersList.SelectedIndex = -1;
        }
        else
        {*/
        PageView.ActiveViewIndex = 1;
        UserForm.ChangeMode(FormViewMode.Edit);
        // }
    }

    #endregion

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 1;
        UserForm.ChangeMode(FormViewMode.Insert);
    }

    /**************************************************************************
                                PageFormView Methods
     **************************************************************************/

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 0;
        UserForm.ChangeMode(FormViewMode.ReadOnly);
    }

    protected void UserForm_DataBound(object sender, EventArgs e)
    {
        if (UserForm.CurrentMode == FormViewMode.Edit)
        {
            BMWUsedCarsLib.User user = UserForm.DataItem as BMWUsedCarsLib.User;

            /* if (user.RoleId != 1)
             {
                 ListBox lb = UserForm.Controls[0].FindControl("Sections") as ListBox;
                 for (int i = 0; i < user.Sections.Count; i++)
                     lb.Items.FindByValue(user.Sections.Get(i)).Selected = true;
             }*/
        }
    }

    protected void UserForm_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        if (e.CancelingEdit)
            PageView.ActiveViewIndex = 0;
    }

    #region ************* Form ItemInserting & ItemInserted Events *************

    protected void UserForm_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
        /*if (Convert.ToByte(e.Values["RoleId"]) != 1)
        {
            ListBox lb = UserForm.Controls[0].FindControl("Sections") as ListBox;
            NameValueCollection sections = new NameValueCollection();

            foreach (ListItem item in lb.Items)
            {
                if (item.Selected)
                    sections.Add(null, item.Value);
            }

            e.Values["Sections"] = sections;
        }*/
        e.Values["RoleId"] = Convert.ToByte(1);
    }

    protected void UserForm_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception.InnerException != null)
            {
                if (e.Exception.InnerException.Message == "Duplicate UserName")
                    ShowPopup("Add User Canceled", "The username \"" + e.Values["UserName"] + "\" already exist. " +
                        "Please enter a different username and try again.", "stop");
                else if (e.Exception.InnerException.Message == "Duplicate Email")
                    ShowPopup("Add User Canceled", "The email \"" + e.Values["Email"] + "\" already exist. " +
                        "Please enter a different email and try again.", "stop");
                else
                    ShowPopup("Add User Error", "An error occurred, could not add user. " + e.Exception.InnerException.Message, "error");
            }
            else
                ShowPopup("Add User Error", "An error occurred, could not add user. " + e.Exception.Message, "error");

            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
        }
        else
        {
            PageView.ActiveViewIndex = 0;
            UsersList.DataBind();
        }
    }

    #endregion

    #region ************* Form ItemUpdating & ItemUpdated Events   *************

    protected void UserForm_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        /* if (Convert.ToByte(e.NewValues["RoleId"]) != 1)
         {
             ListBox lb = UserForm.Controls[0].FindControl("Sections") as ListBox;
             NameValueCollection sections = new NameValueCollection();

             foreach (ListItem item in lb.Items)
             {
                 if (item.Selected)
                     sections.Add(null, item.Value);
             }

             e.NewValues["Sections"] = sections;
         }*/

        if (Convert.ToString(e.NewValues["Password"]) == string.Empty)
            e.NewValues["Password"] = e.OldValues["Password"];
        else
            e.NewValues["Password"] = BMWUsedCarsLib.Utils.CalculateMD5Hash(e.NewValues["Password"].ToString());
        e.NewValues["RoleId"] = Convert.ToByte(1);
    }

    protected void UserForm_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception.InnerException != null)
            {
                if (e.Exception.InnerException.Message == "Duplicate Email")
                    ShowPopup("User Update Canceled", "The email \"" + e.NewValues["Email"] + "\" already exist. " +
                        "Please enter a different email and try again.", "stop");
                else
                    ShowPopup("User Update Error", "An error occurred, could not update user. " + e.Exception.InnerException.Message, "error");
            }
            else
                ShowPopup("User Update Error", "An error occurred, could not update user. " + e.Exception.Message, "error");

            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else
        {
            PageView.ActiveViewIndex = 0;
            UsersList.DataBind();
        }
    }

    #endregion
}
