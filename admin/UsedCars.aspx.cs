﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using BMWUsedCarsLib;

public partial class admin_UsedCars : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //DV97383_thumb.jpg
        //
        //
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterOnSubmitStatement(Page, Page.GetType(), "EditorFix", "for ( var i = 0; i < parent.frames.length; ++i ) if ( parent.frames[i].FCK ) parent.frames[i].FCK.UpdateLinkedField();");
    }

    protected void UsedCarList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandArgument.ToString() != "")
        {
            if (e.CommandName == "Delete")
            {
                string ChassisNO = e.CommandArgument.ToString();
                UserRolesDAL dal = new UserRolesDAL();
                var ghabourBackendUserCookie = Request.Cookies["GhabourBackendUser"];
                var hasKeys = ghabourBackendUserCookie.HasKeys;
                if (hasKeys)
                {
                    var value = ghabourBackendUserCookie.Value;
                    var paramsSplitted = value.Split('&');
                    if (paramsSplitted.Length > 0)
                    {
                        var userIdParam = paramsSplitted[0];
                        var paramKeyValue = userIdParam.Split('=');
                        if (paramKeyValue.Length > 0)
                        {
                            var userIdValueStr = paramKeyValue[1];
                            var userIdInt = Convert.ToInt32(userIdValueStr);
                            dal.AddItem("Delete Used Car Model", userIdInt, ChassisNO);
                        }
                    }
                }
            }
        }
    }

    protected void ShowPopup(string title, string message, string icon)
    {
        PopupMessage.PopupTitle = title;
        PopupMessage.PopupContent = message;
        PopupMessage.PopupIcon = icon;
        PopupMessage.ShowBox();
    }

    protected void ObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }

    /**************************************************************************
                                PageListView Methods
     **************************************************************************/

    #region List View Events (DataBound, ItemDataBound, ItemDeleted, SelectedIndexChanged)

    protected void UsedCarList_DataBound(object sender, EventArgs e)
    {

        if (UsedCarList.Items.Count > 0)
        {
            DataPager pager = UsedCarList.FindControl("pager") as DataPager;
            pager.Visible = (pager.PageSize < pager.TotalRowCount);

            if (pager.Visible)
            {
                ImageButton btn;

                if (pager.StartRowIndex == 0)
                {
                    btn = pager.Controls[0].FindControl("btnFirst") as ImageButton;
                    btn.Enabled = false;

                    btn = pager.Controls[0].FindControl("btnPrevious") as ImageButton;
                    btn.Enabled = false;
                }
                else if (pager.StartRowIndex == (int)Math.Floor((double)pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows)
                {
                    btn = pager.Controls[0].FindControl("btnNext") as ImageButton;
                    btn.Enabled = false;

                    btn = pager.Controls[0].FindControl("btnLast") as ImageButton;
                    btn.Enabled = false;
                }
            }
        }
    }

    private bool sortColumnHandled;

    Label lbl;
    ListViewDataItem item;
    DataRowView drv;

    protected void UsedCarList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        //  if there is no sort expression, don't bother
        if (UsedCarList.SortExpression != string.Empty)
        {
            if (!sortColumnHandled)
            {
                HtmlTableRow header = ((HtmlTable)UsedCarList.FindControl("ListHeader")).Rows[0];

                //  loop through the cells and find the one that contains the linkbutton
                //  with the commandargument of the ListView's current SortExpression
                for (int i = 3; i < header.Cells.Count - 2; i++)
                {
                    HtmlTableCell th = header.Cells[i];
                    LinkButton btn = th.Controls[1] as LinkButton;

                    if (btn != null)
                    {
                        //  add the sort classes to this column
                        if (btn.CommandArgument == UsedCarList.SortExpression)
                        {
                            th.Attributes["class"] = "sort";
                            btn.CssClass = UsedCarList.SortDirection == SortDirection.Ascending ? "sortasc" : "sortdesc";
                        }
                        else
                        {
                            btn.CssClass = string.Empty;
                            th.Attributes.Remove("class");
                        }
                    }
                }

                sortColumnHandled = true;
            }
        }

        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            item = e.Item as ListViewDataItem;
            drv = item.DataItem as DataRowView;

            if (drv != null)
            {
                if (drv["Series"].ToString().Length > 85)
                {
                    lbl = e.Item.FindControl("Series") as Label;
                    lbl.Text = drv["Series"].ToString().Substring(0, 80) + "...";
                    lbl.ToolTip = drv["Series"].ToString();
                }
            }
        }
    }

    protected void UsedCarList_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception != null)
            {
                ShowPopup("UsedCar Delete Error", "An error occurred, could not delete ads. " + e.Exception.Message, "error");
                e.ExceptionHandled = true;
            }
            else
                ShowPopup("UsedCar Delete Error", "An error occurred, could not delete ads.", "error");
        }

    }

    protected void UsedCarList_SelectedIndexChanged(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 1;

        adsform.ChangeMode(FormViewMode.Edit);
    }

    #endregion

    #region ListView Paging Methods

    protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Next":
                e.NewStartRowIndex = e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "Previous":
                //  guard against going off the begining of the list
                e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "Last":
                e.NewStartRowIndex = (int)Math.Floor((double)e.Item.Pager.TotalRowCount / e.Item.Pager.MaximumRows) * e.Item.Pager.MaximumRows;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "First":
            default:
                // first page always starts at 0
                e.NewStartRowIndex = 0;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
        }
    }

    protected void CurrentPageChanged(object sender, EventArgs e)
    {
        TextBox txtCurrentPage = sender as TextBox;

        DataPager pager = UsedCarList.FindControl("pager") as DataPager;
        int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
        pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
    }

    #endregion

    protected void UpdateHideValues(object sender, EventArgs e)
    {
        CheckBox cbOld, cbNew;
        DataTable dt = new DataTable();

        dt.Columns.Add("Id", Type.GetType("System.Int32"));

        foreach (ListViewDataItem item in UsedCarList.Items)
        {
            cbOld = item.FindControl("OldHide") as CheckBox;
            cbNew = item.FindControl("Hide") as CheckBox;

            if (cbOld.Checked != cbNew.Checked)
                dt.Rows.Add(UsedCarList.DataKeys[item.DisplayIndex].Value);
        }

        //if new selections were made, update the values
        if (dt.Rows.Count > 0)
        {
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            ds.Tables[0].TableName = "UsedCars";

            try
            {
                UsedCarsDAL.ShowHide(ds);
                UsedCarList.DataBind();
            }
            catch (Exception ex)
            {
                ShowPopup("Update Hide Error", "An error occurred, could not update the visibility of selected cars. " + ex.Message, "error");
            }
        }
    }

    /**************************************************************************
                                PageFormView Methods
     **************************************************************************/

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        PageView.ActiveViewIndex = 0;
        adsform.ChangeMode(FormViewMode.ReadOnly);
    }

    protected void adsform_DataBound(object sender, EventArgs e)
    {

    }

    protected void adsform_ModeChanging(object sender, FormViewModeEventArgs e)
    {
        if (e.CancelingEdit)
            PageView.ActiveViewIndex = 0;
    }

    #region ************* Form ItemInserting & ItemInserted Events *************

    protected void adsform_ItemInserting(object sender, FormViewInsertEventArgs e)
    {

    }

    protected void adsform_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception.InnerException != null)
            {
                if (e.Exception.InnerException.Message == "Duplicate UsedCar")
                    ShowPopup("UsedCard UsedCar Canceled", "The ads title \"" + e.Values["Title"] + "\" already exist for the selected date. " +
                        "Please enter a different title or select a different date and try again.", "stop");
                else
                    ShowPopup("UsedCard UsedCar Error", "An error occurred, could not add ads. " + e.Exception.InnerException.Message, "error");
            }
            else
                ShowPopup("UsedCard UsedCar Error", "An error occurred, could not add ads. " + e.Exception.Message, "error");

            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;


        }
        else
        {
            PageView.ActiveViewIndex = 0;
            UsedCarList.DataBind();

            RegisterScript();
        }
    }

    #endregion

    #region ************* Form ItemUpdating & ItemUpdated Events   *************

    protected void adsform_ItemUpdating(object sender, FormViewUpdateEventArgs e)
    {
        FileUpload fuMian = adsform.FindControl("ImagesThumb") as FileUpload;
        if (fuMian.HasFile != false)
        {
            e.NewValues["ImagesThumb"] = "MyThumbs/" + fuMian.FileName;
            fuMian.PostedFile.SaveAs(Server.MapPath("~/MyThumbs/" + fuMian.FileName));
        }
        else
        {
            e.NewValues["ImagesThumb"] = e.OldValues["ImagesThumb"];
        }
    }

    protected void adsform_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null || e.AffectedRows <= 0)
        {
            if (e.Exception.InnerException != null)
            {
                if (e.Exception.InnerException.Message == "Duplicate UsedCar")
                    ShowPopup("UsedCar Update Canceled", "The ads title \"" + e.NewValues["Title"] + "\" already exist for the selected date. " +
                        "Please enter a different title or select a different date and try again.", "stop");
                else
                    ShowPopup("UsedCar Update Error", "An error occurred, could not update ads. " + e.Exception.InnerException.Message, "error");
            }
            else
                ShowPopup("UsedCar Update Error", "An error occurred, could not update ads. " + e.Exception.Message, "error");

            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else
        {
            UserRolesDAL dal = new UserRolesDAL();
            var ghabourBackendUserCookie = Request.Cookies["GhabourBackendUser"];
            var hasKeys = ghabourBackendUserCookie.HasKeys;
            if (hasKeys)
            {
                var value = ghabourBackendUserCookie.Value;
                var paramsSplitted = value.Split('&');
                if (paramsSplitted.Length > 0)
                {
                    var userIdParam = paramsSplitted[0];
                    var paramKeyValue = userIdParam.Split('=');
                    if (paramKeyValue.Length > 0)
                    {
                        var userIdValueStr = paramKeyValue[1];
                        var userIdInt = Convert.ToInt32(userIdValueStr);
                        dal.AddItem("Update Used Car Model", userIdInt, e.NewValues["ChassisNO"].ToString());

                    }
                }
            }
            PageView.ActiveViewIndex = 0;
            UsedCarList.DataBind();

            RegisterScript();
        }
    }

    public void RegisterScript()
    {
        //StringBuilder sb = new StringBuilder();
        //HiddenField hf = adsform.Controls[0].FindControl("BrandsValues") as HiddenField;
        //sb.AppendLine(" var Brands = [" + hf.Value + "];");

        //hf = adsform.Controls[0].FindControl("ModelsValues") as HiddenField;
        //sb.AppendLine(" var Models = [" + hf.Value + "];");

        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "autoComplete", sb.ToString(), true);
    }

    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        UsedCarList.DataSourceID = "SearchOds";
        UsedCarList.DataBind();
    }
}