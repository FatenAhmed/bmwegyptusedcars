<%@ Page Title="" Language="C#" MasterPageFile="~/admin/layout.master" AutoEventWireup="true" CodeFile="SearchCarsPhotos.aspx.cs" Inherits="admin_SearchCarsPhotos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContentPlaceHolder" runat="Server">

    <table border="0" cellpadding="0" cellspacing="0" class="border" width="100%">
        <tr>
            <td class="formheader" colspan="2">Edit Photos >>>> Large : 640 x 480 & Thumb : 140 x 100
            </td>
        </tr>


        <tr>
            <td align="right" class="label">First image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large1st" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img1Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">First image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb1st" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img1Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="right" class="label">Second image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large2nd" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img2Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">Second image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb2nd" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img2Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="right" class="label">Third image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large3rd" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img3Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">Third image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb3rd" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img3Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="right" class="label">Forth image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large4th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img4Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">Forth image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb4th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img4Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="right" class="label">Fifth image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large5th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img5Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">Fifth image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb5th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img5Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="right" class="label">Sixth image (Big)
            </td>
            <td class="input">
                <asp:FileUpload ID="Large6th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img6Big %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>
        <tr>
            <td align="right" class="label">Sixth image (thumbnail)
            </td>
            <td class="input">
                <asp:FileUpload ID="Thumb6th" runat="server" Columns="50"></asp:FileUpload>
                <a href='../MyImages/<%= img6Thumb %>' target="_blank">
                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
            </td>
        </tr>

        <tr>
            <td align="left" colspan="2">
                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="button"
                    Text="" />
            </td>
        </tr>

    </table>
</asp:Content>

