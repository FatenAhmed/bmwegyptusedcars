﻿<%@ Control Language="C#" ClassName="confirmBox" %>

<script runat="server">

</script>

<ajaxToolkit:ModalPopupExtender ID="ConfirmPopup" runat="server" BackgroundCssClass="modalBackground"
    BehaviorID="confirmPopup" PopupControlID="ConfirmPanel" PopupDragHandleControlID="ConfirmDragPanel"
    TargetControlID="lnkConfirm" OkControlID="btnOk" CancelControlID="btnCancel"
    OnOkScript="okClick()" OnCancelScript="cancelClick()" />
<a href="javascript:void(0)" id="lnkConfirm" runat="server" style="display: none">
</a>
<asp:Panel ID="ConfirmPanel" runat="server" style="display: none; width: 450px">
    <asp:Panel ID="ConfirmDragPanel" runat="server" style="cursor: move">
        <div class="ttl">
            <div class="ttlt">
                <div class="ttlt-l">
                </div>
                <div class="ttlt-m" style="text-align: left">
                    <span>Confirm Delete</span>
                </div>
                <div class="ttlt-r">
                </div>
            </div>
            <div class="ttlb">
                <div class="ttlb-l">
                    <span></span>
                </div>
                <div class="ttlb-m">
                    <span></span>
                </div>
                <div class="ttlb-r">
                    <span></span>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="con">
        <div class="con-l">
        </div>
        <div class="con-m">
            <table width="100%" cellpadding="0" cellspacing="0" style="margin-top: 6px;">
                <tr>
                    <td align="center" valign="middle" style="padding: 5px; width: 25px">
                        <img src="images/dialog/q.png" />
                    </td>
                    <td style="padding: 5px;" align="left" valign="middle">
                        <asp:Label ID="Message" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="con-r">
        </div>
    </div>
    <div class="ftr">
        <div class="ftr-l">
        </div>
        <div class="ftr-m">
            <table cellpadding="0" cellspacing="0" align="right">
                <tr>
                    <td>
                        <asp:LinkButton ID="btnOk" runat="server" CausesValidation="false" CssClass="btn"><span>Yes</span></asp:LinkButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn" CausesValidation="false"><span>No</span></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        <div class="ftr-r">
        </div>
    </div>
</asp:Panel>
