﻿using System;

public partial class admin_controls_messageBox : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string PopupTitle
    {
        set { Title.Text = value; }
    }

    public string PopupContent
    {
        get { return Content.Text; }
        set { Content.Text = value; }
    }

    public string PopupIcon
    {
        set { Icon.Src = "../images/dialog/" + value + ".png"; }
    }

    public void ShowBox()
    {
        ModalPopup.Show();
    }
}
