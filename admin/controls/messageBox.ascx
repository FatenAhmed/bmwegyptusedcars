﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="messageBox.ascx.cs" Inherits="admin_controls_messageBox" %>

<ajaxToolkit:ModalPopupExtender ID="ModalPopup" runat="server" BackgroundCssClass="modalBackground"
    BehaviorID="modalPopup" PopupControlID="PopupPanel" PopupDragHandleControlID="PopupDragPanel"
    TargetControlID="lnkPopup" />
<a href="javascript:void(0)" id="lnkPopup" runat="server" style="display: none">
</a>
<asp:Panel ID="PopupPanel" runat="server" Style="display: none; width: 450px">
    <asp:Panel ID="PopupDragPanel" runat="server" Style="cursor: move">
        <div class="ttl">
            <div class="ttlt">
                <div class="ttlt-l">
                </div>
                <div class="ttlt-m">
                    <a href="javascript:void(0)" onclick="$find('modalPopup').hide();"></a>
                    <asp:Label ID="Title" runat="server"></asp:Label>
                </div>
                <div class="ttlt-r">
                </div>
            </div>
            <div class="ttlb">
                <div class="ttlb-l">
                    <span></span>
                </div>
                <div class="ttlb-m">
                    <span></span>
                </div>
                <div class="ttlb-r">
                    <span></span>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="con">
        <div class="con-l">
        </div>
        <div class="con-m">
            <table width="100%" cellpadding="0" cellspacing="0" style="margin-top: 6px;">
                <tr>
                    <td align="center" valign="middle" style="padding: 3px; width: 25px">
                        <img id="Icon" runat="server" align="left" />
                    </td>
                    <td style="padding: 3px;" align="left" valign="middle">
                        <asp:Label ID="Content" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="con-r">
        </div>
    </div>
    <div class="ftr">
        <div class="ftr-l">
        </div>
        <div class="ftr-m">
            <a href="javascript:void(0)" class="btn" onclick="$find('modalPopup').hide();"><span>
                OK</span></a>
        </div>
        <div class="ftr-r">
        </div>
    </div>
</asp:Panel>