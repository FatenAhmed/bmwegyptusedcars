﻿using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using BMWUsedCarsLib;
using System.Collections.Generic;
using System.Drawing;

public partial class admin_UserActivities : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        //DV97383_thumb.jpg
        //
        //
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterOnSubmitStatement(Page, Page.GetType(), "EditorFix", "for ( var i = 0; i < parent.frames.length; ++i ) if ( parent.frames[i].FCK ) parent.frames[i].FCK.UpdateLinkedField();");

        UserRolesDAL dal = new UserRolesDAL();

        var query = dal.GetAllItems();
        List<Details> list = new List<Details>();
        foreach (var itm in query)
        {
            Details obj = new Details();
            obj.UserName = itm.User.UserName;
            obj.RoleName = itm.Role.RoleName;
            obj.ChassisNO = itm.ChassisNO;
            list.Add(obj);
        }
        ExcelSheet.DataSource = list;
        ExcelSheet.DataBind();
    }

    protected void ShowPopup(string title, string message, string icon)
    {
        PopupMessage.PopupTitle = title;
        PopupMessage.PopupContent = message;
        PopupMessage.PopupIcon = icon;
        PopupMessage.ShowBox();
    }

    protected void ObjectDataSource_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }

    /**************************************************************************
                                PageListView Methods
     **************************************************************************/

    #region List View Events (DataBound, ItemDataBound, ItemDeleted, SelectedIndexChanged)

    protected void UsedCarList_DataBound(object sender, EventArgs e)
    {

        if (UsedCarList.Items.Count > 0)
        {
            DataPager pager = UsedCarList.FindControl("pager") as DataPager;
            pager.Visible = (pager.PageSize < pager.TotalRowCount);

            if (pager.Visible)
            {
                ImageButton btn;

                if (pager.StartRowIndex == 0)
                {
                    btn = pager.Controls[0].FindControl("btnFirst") as ImageButton;
                    btn.Enabled = false;

                    btn = pager.Controls[0].FindControl("btnPrevious") as ImageButton;
                    btn.Enabled = false;
                }
                else if (pager.StartRowIndex == (int)Math.Floor((double)pager.TotalRowCount / pager.MaximumRows) * pager.MaximumRows)
                {
                    btn = pager.Controls[0].FindControl("btnNext") as ImageButton;
                    btn.Enabled = false;

                    btn = pager.Controls[0].FindControl("btnLast") as ImageButton;
                    btn.Enabled = false;
                }
            }
        }
    }

    private bool sortColumnHandled;

    Label lbl;
    ListViewDataItem item;
    DataRowView drv;

    protected void UsedCarList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

    }

    #endregion

    #region ListView Paging Methods

    protected void PagerCommand(object sender, DataPagerCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Next":
                e.NewStartRowIndex = e.Item.Pager.StartRowIndex + e.Item.Pager.MaximumRows;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "Previous":
                //  guard against going off the begining of the list
                e.NewStartRowIndex = Math.Max(0, e.Item.Pager.StartRowIndex - e.Item.Pager.MaximumRows);
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "Last":
                e.NewStartRowIndex = (int)Math.Floor((double)e.Item.Pager.TotalRowCount / e.Item.Pager.MaximumRows) * e.Item.Pager.MaximumRows;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
            case "First":
            default:
                // first page always starts at 0
                e.NewStartRowIndex = 0;
                e.NewMaximumRows = e.Item.Pager.MaximumRows;
                break;
        }
    }

    protected void CurrentPageChanged(object sender, EventArgs e)
    {
        TextBox txtCurrentPage = sender as TextBox;

        DataPager pager = UsedCarList.FindControl("pager") as DataPager;
        int startRowIndex = Math.Min((int.Parse(txtCurrentPage.Text) - 1) * pager.MaximumRows, pager.TotalRowCount - pager.MaximumRows);
        pager.SetPageProperties(startRowIndex, pager.MaximumRows, true);
    }

    #endregion


    protected void btnExport_Click(Object sender, EventArgs e)
    {
        ExportToExcel("AdminActivities.xls", ExcelSheet);
    }

    private void ExportToExcel(String strFileName, DataGrid dg)
    {
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("content-disposition", "attachment;filename=" + strFileName);
        Response.Charset = "utf-8";

        //Add header style
        ExcelSheet.HeaderStyle.Font.Bold = true;
        ExcelSheet.HeaderStyle.ForeColor = Color.White;
        ExcelSheet.HeaderStyle.BackColor = Color.Black;

        Response.ContentEncoding = System.Text.Encoding.Unicode;
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
        this.EnableViewState = false;
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        dg.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
}

public class Details
{
    public string UserName { get; set; }
    public string RoleName { get; set; }
    public string ChassisNO { get; set; }
}

