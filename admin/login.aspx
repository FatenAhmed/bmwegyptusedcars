<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="admin_login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>::.. Icon Creations Administration ..::</title>
<link rel="shortcut icon" href="favicon.ico" type="image/ico" />
<link rel="icon" href="favicon.ico" type="image/ico" />
    <%--<link rel="stylesheet" type="text/css" href="style/default.css" />--%>
    <link rel="stylesheet" type="text/css" href="style/admin_style.css" />
</head>
<body>
    <form id="loginform" runat="server" defaultfocus="Username" defaultbutton="btnLogin">
    <div id="userpass"  visible="false">
        <div class="IClogo"><a href="http://www.icon-creations.com/" target="_blank">Icon Creations</a></div>
        <div class="login">
    		<div class="welcome">
            	<%--<img class="clientLogo" src="images/img-admin/Logo.png" width="90" />--%>
                <h1>Administration Panel</h1>
            </div>
            <div class="credentials">
            	<h2>Please provide your admin credentials</h2>
                
                <div class="line">
                	<label for="username" class="label">Username</label>
                            <asp:TextBox ID="Username" runat="server" class="input_login"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UsernameRequired" runat="server" ControlToValidate="Username"
                                 CssClass="validate" Text="*"></asp:RequiredFieldValidator>
                     </div>
                <div class="line">
                	<label for="password" class="label">Password</label>
                            <asp:TextBox ID="Password" runat="server" class="input_login" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                              CssClass="validate" Text="*"></asp:RequiredFieldValidator>
                </div>
                <div class="line">
                            <asp:Button ID="btnLogin" runat="server" Text="LOGIN" OnClick="btnSubmit_Click" CssClass="send" />
               </div>
            </div>
        </div>
        <div style="margin: 10px 0px 0px 0px; font-size: 10px;" class="error">
            <asp:Literal ID="ErrorMsg" runat="server" EnableViewState="False" Visible="false"></asp:Literal>
        </div>    
    </div>

                
          
 <%--   <div class="footer">
        &copy; Copyright 2013 <strong><a href="http://www.icon-creations.com/" target="_blank" style="color: #FFFFFF">
            Icon Creations</a></strong> - All Rights Reserved
    </div>--%>
    </form>
</body>
</html>
