﻿<%@ Page Language="C#" MasterPageFile="~/admin/layout.master" AutoEventWireup="true"
    CodeFile="users.aspx.cs" Inherits="admin_users" Title="Icon Creations Administration - Users" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="PageContent" ContentPlaceHolderID="PageContentPlaceHolder" runat="Server">
    <div class="PageTitle">
        <span class="nav_title">Home Page</span><span class="nav_last">Members</span>
    </div>

    <asp:UpdatePanel ID="PagePanel" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="PageView" runat="server" ActiveViewIndex="0">
                <asp:View ID="PageListView" runat="server">
                    <table border="0" cellpadding="0" width="750">
                        <%--      <tr>
                            <td>
                                <asp:LinkButton ID="btnAdd1" runat="server" CausesValidation="false" CssClass="add"
                                    OnClick="btnAdd_Click" Text="Add New Member"></asp:LinkButton>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <asp:ListView ID="UsersList" runat="server" DataKeyNames="UserId" DataSourceID="ListDataSource"
                                    OnDataBound="UsersList_DataBound" OnItemDeleted="UsersList_ItemDeleted" OnSelectedIndexChanged="UsersList_SelectedIndexChanged">
                                    <LayoutTemplate>
                                        <table id="ListHeader" runat="server" border="0" cellpadding="0" cellspacing="0"
                                            class="grid">
                                            <tr>
                                                <th>#
                                                </th>
                                                <th>
                                                    <asp:LinkButton ID="FirstName" runat="server" CausesValidation="false" CommandArgument="FirstName"
                                                        CommandName="Sort" CssClass="nobg" Text="First Name"></asp:LinkButton>
                                                </th>
                                                <th width="60">Tasks
                                                </th>
                                                <th width="60">Reports
                                                </th>
                                                <th width="110">Working Hours
                                                </th>
                                                <th width="60">Vacation
                                                </th>
                                                <th width="128">Options
                                                </th>
                                            </tr>
                                            <tr id="itemPlaceholder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr id="row" runat="server" class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "altrow" %>'>
                                            <td class="rownum">
                                                <%# Container.DataItemIndex + 1 %>
                                            </td>
                                            <td>
                                                <%# Eval("FirstName")%> <%# Eval("LastName")%>
                                            </td>
                                            <td>
                                                <a href='<%# "TasksOfMember.aspx?memberId=" + Eval("UserId") %>'>Go</a>
                                            </td>
                                            <td>
                                                <a href='<%# "ReportsListGrouped.aspx?memberId=" + Eval("UserId") %>'>Go</a>
                                            </td>
                                            <td>
                                                <a href='<%# "WorkingHours.aspx?memberId=" + Eval("UserId") %>'>Go</a>
                                            </td>
                                            <td>
                                                <a href='<%# "VacationsList.aspx?memberId=" + Eval("UserId") %>'>Go</a>
                                            </td>
                                            <td class="tdImg">
                                                <asp:ImageButton ID="btnEdit" runat="server" AlternateText="Edit" CommandName="Select" BorderColor="#000"
                                                    ImageUrl="images/edit.gif" />
                                                <asp:ImageButton ID="btnDelete" runat="server" AlternateText="Delete" CommandName="Delete"
                                                    ImageUrl="images/delete.png" OnClientClick='<%# "showConfirm(this, \"user\", \"" + Eval("UserName") + "\"); return false;" %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <EmptyDataTemplate>
                                        <p class="empty">
                                            No Current Members Available
                                        </p>
                                    </EmptyDataTemplate>
                                </asp:ListView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:LinkButton ID="btnAdd2" runat="server" CausesValidation="false" CssClass="add_button"
                                    OnClick="btnAdd_Click" Text="Add New Member"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="PageFormView" runat="server">
                    <div align="right" style="width: 400px; margin-bottom: 5px">
                        <asp:LinkButton ID="lnkBack1" runat="server" CausesValidation="False" CssClass="back"
                            OnClick="lnkBack_Click" Text="Back To List"></asp:LinkButton>
                    </div>
                    <asp:FormView ID="UserForm" runat="server" CellPadding="0" DataKeyNames="UserId"
                        DataSourceID="FormDataSource" OnDataBound="UserForm_DataBound" OnItemInserted="UserForm_ItemInserted"
                        OnItemInserting="UserForm_ItemInserting" OnItemUpdated="UserForm_ItemUpdated"
                        OnItemUpdating="UserForm_ItemUpdating" OnModeChanging="UserForm_ModeChanging"
                        Width="400px">
                        <InsertItemTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" class="border abslt" width="100%">
                                <tr>
                                    <td class="formheader" colspan="2">Add New Member
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label" width="28%">First Name
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="FirstName" runat="server" Columns="38" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Text="*" ControlToValidate="FirstName"
                                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Last Name
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="LastName" runat="server" Columns="38" Text='<%# Bind("LastName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" Text="*" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Username
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Username" runat="server" Columns="38" Text='<%# Bind("UserName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UsernameRequired" runat="server" Text="*" ControlToValidate="Username"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Password
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Password" runat="server" Columns="38" Text='<%# Bind("Password") %>'
                                            TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" Text="*" ControlToValidate="Password"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Confirm Password
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="ConfirmPassword" runat="server" Columns="38" TextMode="Password"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                            Text="*" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="ComparePasswords" runat="server" ControlToCompare="Password"
                                            ControlToValidate="ConfirmPassword" Operator="Equal" Type="String" Text="Do not match"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Email
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Email" runat="server" Columns="38" Text='<%# Bind("Email") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                            Text="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="EmailRegularExpression" runat="server" ControlToValidate="Email"
                                            Text="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <%-- <tr>
                                    <td align="right" class="label">
                                        Role
                                    </td>
                                    <td class="input">
                                        <asp:DropDownList ID="Role" runat="server" AppendDataBoundItems="true" DataSourceID="RolesDS"
                                            DataTextField="RoleName" DataValueField="RoleId" 
                                             SelectedValue='<%# Bind("RoleId") %>'>
                                            <asp:ListItem Value="0">-- Select Role --</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="RoleRequired" runat="server" ControlToValidate="Role" Operator="NotEqual"
                                            Type="Integer" ValueToCompare="0" SetFocusOnError="true" Text="*"></asp:CompareValidator>

                                    </td>
                                </tr>--%>
                                <%-- <tr>
                                    <td align="right" class="label">
                                        Sections
                                    </td>
                                    <td class="input">
                                          <asp:DropDownList ID="Section" runat="server" AppendDataBoundItems="true" DataSourceID="SectionsDS"
                                            DataTextField="Name" DataValueField="Id" 
                                             SelectedValue='<%# Bind("SectionId") %>'>
                                            <asp:ListItem Value="0">-- Select Section --</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:CustomValidator ID="SectionRequired" runat="server" ControlToValidate="Section"
                                            ClientValidationFunction="validateSection" Display="Dynamic" SetFocusOnError="true"
                                            Text="Please select at least one section" ValidateEmptyText="true"></asp:CustomValidator>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td align="right" class="label">Lock Account
                                    </td>
                                    <td class="input">
                                        <asp:DropDownList ID="LockedOut" runat="server" SelectedValue='<%# Bind("IsLockedOut") %>'>
                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                            <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <asp:Button ID="btnInsert" runat="server" CommandName="Insert" CssClass="button"
                                            Text="Submit" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            CssClass="button right" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </InsertItemTemplate>
                        <EditItemTemplate>
                            <table border="0" cellpadding="0" cellspacing="0" class="border abslt" width="100%">
                                <tr>
                                    <td class="formheader" colspan="2">Edit Member
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label" width="28%">First Name
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="FirstName" runat="server" Columns="38" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Text="*" ControlToValidate="FirstName"
                                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Last Name
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="LastName" runat="server" Columns="38" Text='<%# Bind("LastName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" Text="*" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Username
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Username" runat="server" Columns="38" Text='<%# Bind("UserName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="UsernameRequired" runat="server" Text="*" ControlToValidate="Username"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">New Password
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Password" runat="server" Columns="38" Text='<%# Bind("Password") %>'
                                            TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Confirm Password
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="ConfirmPassword" runat="server" Columns="38" TextMode="Password"></asp:TextBox>
                                        <asp:CompareValidator ID="ComparePasswords" runat="server" ControlToCompare="Password"
                                            ControlToValidate="ConfirmPassword" Operator="Equal" Type="String" Text="Do not match"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="label">Email
                                    </td>
                                    <td class="input">
                                        <asp:TextBox ID="Email" runat="server" Columns="38" Text='<%# Bind("Email") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                                            Text="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="EmailRegularExpression" runat="server" ControlToValidate="Email"
                                            Text="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>

                                <%-- <tr>
                                    <td align="right" class="label">
                                        Role
                                    </td>
                                    <td class="input">
                                        <asp:DropDownList ID="Role" runat="server" AppendDataBoundItems="true" DataSourceID="RolesDS"
                                            DataTextField="RoleName" DataValueField="RoleId" 
                                             SelectedValue='<%# Bind("RoleId") %>'>
                                            <asp:ListItem Value="0">-- Select Role --</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="RoleRequired" runat="server" ControlToValidate="Role" Operator="NotEqual"
                                            Type="Integer" ValueToCompare="0" SetFocusOnError="true" Text="*"></asp:CompareValidator>

                                    </td>
                                </tr>--%>
                                <%-- <tr>
                                    <td align="right" class="label">
                                        Sections
                                    </td>
                                    <td class="input">
                                          <asp:DropDownList ID="Section" runat="server" AppendDataBoundItems="true" DataSourceID="SectionsDS"
                                            DataTextField="Name" DataValueField="Id" 
                                             SelectedValue='<%# Bind("SectionId") %>'>
                                            <asp:ListItem Value="0">-- Select Section --</asp:ListItem>
                                        </asp:DropDownList>
                                        <br />
                                        <asp:CustomValidator ID="SectionRequired" runat="server" ControlToValidate="Section"
                                            ClientValidationFunction="validateSection" Display="Dynamic" SetFocusOnError="true"
                                            Text="Please select at least one section" ValidateEmptyText="true"></asp:CustomValidator>
                                    </td>
                                </tr>--%>


                                <tr>
                                    <td align="right" class="label">Lock Account
                                    </td>
                                    <td class="input">
                                        <asp:DropDownList ID="LockedOut" runat="server" SelectedValue='<%# Bind("IsLockedOut") %>'>
                                            <asp:ListItem Value="True">Yes</asp:ListItem>
                                            <asp:ListItem Value="False" Selected="True">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="right">
                                        <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="button"
                                            Text="Update" />
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                            CssClass="button right" Text="Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                    </asp:FormView>
                    <div align="right" style="width: 400px; margin-top: 5px">
                        <asp:LinkButton ID="lnkBack2" runat="server" CausesValidation="False" CssClass="back"
                            OnClick="lnkBack_Click" Text="Back To List"></asp:LinkButton>
                    </div>
                </asp:View>
                <asp:View ID="NoAccessView" runat="server">
                    <h2 id="noAccess">You are not allowed to access this section</h2>
                </asp:View>
            </asp:MultiView>
            <mb:MessageBox ID="PopupMessage" runat="server"></mb:MessageBox>
            <cb:ConfirmBox ID="ConfirmMessage" runat="server"></cb:ConfirmBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="LoadingProgress" runat="server">
        <ProgressTemplate>
            <div id="progressFilter">
            </div>
            <div id="progressMessage">
                Processing&nbsp;<img alt="Processing" src="images/37-1.gif" align="absmiddle" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <script language="javascript">
        function validateSection(sender, args) {
            var role = document.getElementById('ctl00_PageContentPlaceHolder_UserForm_Role');
            if (role.selectedIndex == 2 && args.Value == "")
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    </script>

    <asp:ObjectDataSource ID="ListDataSource" runat="server" DeleteMethod="DeleteUser"
        SelectMethod="GetUsers" TypeName="BMWUsedCarsLib.UsersDAL" OnDeleted="ObjectDataSource_Updated">
        <DeleteParameters>
            <asp:Parameter Name="userId" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="FormDataSource" runat="server" DataObjectTypeName="BMWUsedCarsLib.User"
        InsertMethod="CreateUser" SelectMethod="GetUser" TypeName="BMWUsedCarsLib.UsersDAL"
        UpdateMethod="UpdateUser" OnInserted="ObjectDataSource_Updated" OnUpdated="ObjectDataSource_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="UsersList" Name="userId" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
  <%--  <asp:ObjectDataSource ID="SectionsDS" runat="server" TypeName="BMWUsedCarsLib.SectionsDAL"
        SelectMethod="GetSectionsAll"></asp:ObjectDataSource>--%>

    <%--<asp:ObjectDataSource ID="RolesDS" runat="server" TypeName="BMWUsedCarsLib.RolesDAL" SelectMethod="GetRolesAll">--%>
    <%--<asp:ObjectDataSource ID="RolesDS" runat="server" TypeName="RolesDAL" SelectMethod="GetRolesAll"></asp:ObjectDataSource>--%>
</asp:Content>
