using System;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BMWUsedCarsLib;

public partial class admin_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string password = Utils.CalculateMD5Hash(Password.Text.Trim());

        int errorCode;

        BMWUsedCarsLib.User user = UsersDAL.Authenticate(Username.Text.Trim(), password, 5, 15, out errorCode);

        if (errorCode > 0)
        {
            ErrorMsg.Visible = true;

            if (errorCode == 1 || errorCode == 4)
                ErrorMsg.Text = "Your login attempt was not successful. Please try again.";
            else if (errorCode == 2)
                ErrorMsg.Text = "The account has been locked out.";
            else if (errorCode == 3)
                ErrorMsg.Text = "The account has been locked out due to multiple invalid login attempts.";
        }
        else
        {
            Hashtable userdata = new Hashtable();
            userdata["UserId"] = user.UserId;
            userdata["FirstName"] = user.FirstName;
            userdata["LastName"] = user.LastName;
            userdata["Role"] = user.RoleId;

            string sections = string.Empty;

            if (user.RoleId != 1)
            {
                for (int i = 0; i < user.Sections.Count; i++)
                {
                    sections += user.Sections.GetKey(i) + ",";
                }
            }

            userdata["Sections"] = sections.TrimEnd(',');

            BackEndUser.SetCookie(userdata);

            FormsAuthentication.RedirectFromLoginPage(user.UserName, true);
        }
    }
}
