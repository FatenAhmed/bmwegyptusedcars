﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BMWUsedCarsLib;

public partial class admin_SearchCarsPhotos : System.Web.UI.Page
{
    public string img1Big, img1Thumb;
    public string img2Big, img2Thumb;
    public string img3Big, img3Thumb;
    public string img4Big, img4Thumb;
    public string img5Big, img5Thumb;
    public string img6Big, img6Thumb;
    public static string chassis = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) chassis = "";

        if (Request.QueryString["ch"] != null)
        {
            chassis = Request.QueryString["ch"].ToString();

            img1Big = chassis + "_0_b.jpg";
            img1Thumb = chassis + "_0_t.jpg";

            img2Big = chassis + "_1_b.jpg";
            img2Thumb = chassis + "_1_t.jpg";

            img3Big = chassis + "_2_b.jpg";
            img3Thumb = chassis + "_2_t.jpg";

            img4Big = chassis + "_3_b.jpg";
            img4Thumb = chassis + "_3_t.jpg";

            img5Big = chassis + "_4_b.jpg";
            img5Thumb = chassis + "_4_t.jpg";

            img6Big = chassis + "_5_b.jpg";
            img6Thumb = chassis + "_5_t.jpg";
        }
        else
        {
            Response.Redirect("Models.aspx");
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (Large1st.HasFile != false)
        {
            Large1st.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_0_b.jpg"));
        }
        if (Thumb1st.HasFile != false)
        {
            Thumb1st.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_0_t.jpg"));
        }

        if (Large2nd.HasFile != false)
        {
            Large2nd.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_1_b.jpg"));
        }
        if (Thumb2nd.HasFile != false)
        {
            Thumb2nd.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_1_t.jpg"));
        }

        if (Large3rd.HasFile != false)
        {
            Large3rd.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_2_b.jpg"));
        }
        if (Thumb3rd.HasFile != false)
        {
            Thumb3rd.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_2_t.jpg"));
        }

        if (Large4th.HasFile != false)
        {
            Large4th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_3_b.jpg"));
        }
        if (Thumb4th.HasFile != false)
        {
            Thumb4th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_3_t.jpg"));
        }

        if (Large5th.HasFile != false)
        {
            Large5th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_4_b.jpg"));
        }
        if (Thumb5th.HasFile != false)
        {
            Thumb5th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_4_t.jpg"));
        }

        if (Large6th.HasFile != false)
        {
            Large6th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_5_b.jpg"));
        }
        if (Thumb6th.HasFile != false)
        {
            Thumb6th.PostedFile.SaveAs(Server.MapPath("~/MyImages/" + chassis + "_5_t.jpg"));
        }

        Response.Redirect("Models.aspx");
    }
}