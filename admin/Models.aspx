﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/layout.master" AutoEventWireup="true" CodeFile="Models.aspx.cs" Inherits="admin_Models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
    <script src="Scripts/jquery-1.4.1.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContentPlaceHolder" runat="Server">
    <div class="PageTitle">
        <span class="nav_title">Home Page</span><span class="nav_last">SearchCars</span>
    </div>
    <asp:MultiView ID="PageView" runat="server" ActiveViewIndex="0">

        <asp:View ID="PageListView" runat="server">

            <table border="0" cellpadding="2" cellspacing="1">

                <tr>
                    <%--<td align="right" class="label" width="28%" style="float: right !important;">Search :</td>--%>

                    <td align="right" class="label" width="28%" style="float: right !important;">Model :</td>


                    <td class="input">
                        <asp:TextBox ID="SearchInModel" runat="server" Columns="25"></asp:TextBox>
                    </td>
                </tr>
                <tr>

                    <td align="right" class="label" width="28%" style="float: right !important;">Keywords :</td>

                    <td class="input">
                        <asp:TextBox ID="SearchInKeywords" runat="server" Columns="25"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="label" width="28%" style="float: right !important;">Series :</td>
                    <td class="input">
                        <select name="SeriesDDL" tabindex="3" class="sfmenu" id="SeriesDDL" runat="server">
                            <option value="">Select</option>
                            <option value="1 Series">1 Series</option>
                            <option value="2 Series">2 Series</option>
                            <option value="3 Series">3 Series</option>
                            <option value="5 Series">5 Series</option>
                            <option value="6 Series">6 Series</option>
                            <option value="7 Series">7 Series</option>
                            <option value="M3">M3</option>
                            <option value="M5">M5</option>
                            <option value="M6">M6</option>
                            <option value="X1">X1</option>
                            <option value="X3">X3</option>
                            <option value="X4">X4</option>
                            <option value="X5">X5</option>
                            <option value="X6">X6</option>
                            <option value="Z4">Z4</option>
                            <option value="I Series">I</option>
                            <option value="X7">X7</option>
                            <option value="8 Series">8 Series</option>
                        </select>
                        <%--<asp:DropDownList ID="CategoryId" runat="server" AppendDataBoundItems="true" DataTextField="Name" DataValueField="Id" DataSourceID="PageListCategories">
                            <asp:ListItem Value="0">Select Category</asp:ListItem>
                        </asp:DropDownList>--%>
                    </td>

                    <td>
                        <asp:LinkButton ID="Search" runat="server" CausesValidation="false" CssClass="add"
                            OnClick="btnSearch_Click" Text="Search"></asp:LinkButton></td>
                </tr>
            </table>

            <table border="0" cellpadding="2" cellspacing="1" width="750">

                <tr>
                    <td>
                        <asp:ListView ID="SearchCarList" runat="server" DataKeyNames="id" DataSourceID="ListDataSource"
                            OnDataBound="SearchCarList_DataBound" OnItemDataBound="SearchCarList_ItemDataBound" OnItemDeleted="SearchCarList_ItemDeleted"
                            OnSelectedIndexChanged="SearchCarList_SelectedIndexChanged" OnItemCommand="UsedCarList_ItemCommand">
                            <LayoutTemplate>
                                <table id="ListHeader" runat="server" border="0" cellpadding="0" cellspacing="0"
                                    class="grid">
                                    <tr>
                                        <th>#
                                        </th>
                                        <th class="hidecol">Show
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="Series" runat="server" CausesValidation="false" CommandArgument="Series"
                                                CommandName="Sort" Text="Series"></asp:LinkButton>
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="Model" runat="server" CausesValidation="false" CommandArgument="Model"
                                                CommandName="Sort" Text="Model"></asp:LinkButton>
                                        </th>
                                        <th>
                                            <asp:LinkButton ID="ChassisNo" runat="server" CausesValidation="false" CommandArgument="ChassisNo"
                                                CommandName="Sort" Text="ChassisNo"></asp:LinkButton>
                                        </th>
                                        <th width="128">Options
                                        </th>
                                        <th width="128">Photos
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server" />
                                    <tr class="pager">
                                        <td colspan="7">
                                            <div class="container">
                                                <asp:DataPager ID="pager" runat="server" PageSize="50">
                                                    <Fields>
                                                        <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                            <PagerTemplate>
                                                                <div class="command">
                                                                    <asp:ImageButton ID="btnFirst" runat="server" CommandName="First" ImageUrl="images/first.gif"
                                                                        AlternateText="First Page" ToolTip="First Page" />
                                                                    <asp:ImageButton ID="btnPrevious" runat="server" CommandName="Previous" ImageUrl="images/prev.gif"
                                                                        AlternateText="Previous Page" ToolTip="Previous Page" />
                                                                </div>
                                                                <div class="command">
                                                                    <asp:TextBox ID="txtSlider" runat="server" Text='<%# Container.TotalRowCount > 0 
																							? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) 
																							: 0 %>'
                                                                        AutoPostBack="true" OnTextChanged="CurrentPageChanged"
                                                                        Style="display: none" />
                                                                    <ajaxToolkit:SliderExtender ID="slider" runat="server" TargetControlID="txtSlider"
                                                                        Orientation="Horizontal" Minimum="1" Maximum='<%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows) %>'
                                                                        TooltipText='<%# "Page {0} of " + Math.Ceiling ((double)Container.TotalRowCount / Container.MaximumRows).ToString() + " (" + Container.TotalRowCount + " items)" %>' />
                                                                </div>
                                                                <div class="command">
                                                                    <asp:ImageButton ID="btnNext" runat="server" CommandName="Next" ImageUrl="images/next.gif"
                                                                        AlternateText="Next Page" ToolTip="Next Page" />
                                                                    <asp:ImageButton ID="btnLast" runat="server" CommandName="Last" ImageUrl="images/last.gif"
                                                                        AlternateText="Last Page" ToolTip="Last Page" />
                                                                </div>
                                                                <div class="info">
                                                                    Page <b>
                                                                        <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                    </b>of <b>
                                                                        <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                    </b>(<%# Container.TotalRowCount %>items)
                                                                </div>
                                                            </PagerTemplate>
                                                        </asp:TemplatePagerField>
                                                    </Fields>
                                                </asp:DataPager>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr class="row">
                                    <td class="rownum">
                                        <%# Container.DataItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="Hide" runat="server" Checked='<%# Eval("Show") %>' />
                                        <asp:CheckBox ID="OldHide" runat="server" Checked='<%# Eval("Show") %>' Visible="False" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Series" runat="server" Text='<%# Eval("Series") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Model" runat="server" Text='<%# Eval("Model") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ChassisNo" runat="server" Text='<%# Eval("ChassisNo") %>'></asp:Label>
                                    </td>
                                    <td class="tdImg">
                                        <asp:ImageButton ID="btnEdit" runat="server" AlternateText="Edit" CommandName="Select"
                                            ImageUrl="images/edit.gif" />
                                        <asp:ImageButton ID="btnDelete" runat="server" AlternateText="Delete" CommandName="Delete" CommandArgument='<%#Eval("ChassisNo") %>'
                                            ImageUrl="images/delete.png" OnClientClick='<%# "showConfirm(this, \"field\", \"" + Eval("Model") + "\"); return false;" %>' />
                                    </td>
                                    <td>
                                        <a href='<%# "SearchCarsPhotos.aspx?ch=" + Eval("ChassisNo") %>'>Photos</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="altrow">
                                    <td class="rownum">
                                        <%# Container.DataItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="Hide" runat="server" Checked='<%# Eval("Show") %>' />
                                        <asp:CheckBox ID="OldHide" runat="server" Checked='<%# Eval("Show") %>' Visible="False" />
                                    </td>
                                    <td>
                                        <asp:Label ID="Series" runat="server" Text='<%# Eval("Series") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="Model" runat="server" Text='<%# Eval("Model") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ChassisNo" runat="server" Text='<%# Eval("ChassisNo") %>'></asp:Label>
                                    </td>
                                    <td class="tdImg">
                                        <asp:ImageButton ID="btnEdit" runat="server" AlternateText="Edit" CommandName="Select"
                                            ImageUrl="images/edit.gif" />
                                        <asp:ImageButton ID="btnDelete" runat="server" AlternateText="Delete" CommandName="Delete" CommandArgument='<%#Eval("ChassisNo") %>'
                                            ImageUrl="images/delete.png" OnClientClick='<%# "showConfirm(this, \"field\", \"" + Eval("Model") + "\"); return false;" %>' />
                                    </td>
                                    <td>
                                        <a href='<%# "SearchCarsPhotos.aspx?ch=" + Eval("ChassisNo") %>'>Photos</a>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <EmptyDataTemplate>
                                <p class="empty">
                                    No Current SearchCar Available
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td class="padlt35" width="57">
                                    <asp:LinkButton ID="btnUpdateHide" runat="server" CausesValidation="false" CssClass="update"
                                        OnClick="UpdateHideValues" Text="Update" ToolEvent="Update Hide Values"></asp:LinkButton>
                                </td>
                                <td align="right">
                                    <a href="../submit.aspx" class="add">Add new</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="PageFormView" runat="server">
            <%--  <div align="right" style="width: 750px; margin-bottom: 5px">
                <asp:LinkButton ID="lnkBack1" runat="server" CausesValidation="False" CssClass="back"
                    OnClick="lnkBack_Click" Text="Back To List"></asp:LinkButton>
            </div>--%>
            <asp:FormView ID="adsform" runat="server" CellPadding="0" DataKeyNames="id"
                DataSourceID="FormDataSource" OnDataBound="adsform_DataBound" OnItemInserted="adsform_ItemInserted"
                OnItemInserting="adsform_ItemInserting" OnItemUpdated="adsform_ItemUpdated"
                OnItemUpdating="adsform_ItemUpdating" OnModeChanging="adsform_ModeChanging"
                Width="750px">
                <InsertItemTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" class="border" width="100%">

                        <colgroup>
                            <col width="60px" />
                            <col width="690" />
                        </colgroup>

                        <tbody>

                            <tr>
                                <td class="formheader" colspan="2">Add New SearchCar
                                </td>
                            </tr>


                        </tbody>
                    </table>
                </InsertItemTemplate>
                <EditItemTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" class="border" width="100%">
                        <tr>
                            <td class="formheader" colspan="2">Edit Search Car
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Series
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Series" runat="server" Columns="120" Text='<%# Bind("Series") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="SeriesRequired" runat="server" ControlToValidate="Series"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Model
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Model" runat="server" Columns="120" Text='<%# Bind("Model") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator ID="ModelRequired" runat="server" ControlToValidate="Model"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Color
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Color" runat="server" Columns="120" Text='<%# Bind("Color") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ColorRequired" runat="server" ControlToValidate="Color"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Milage
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Milage" runat="server" Columns="120" Text='<%# Bind("Milage") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="MilageRequired" runat="server" ControlToValidate="Milage"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Chassis NO
                            </td>
                            <td class="input">
                                <asp:TextBox ID="ChassisNO" runat="server" Columns="120" Text='<%# Bind("ChassisNO") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ChassisNORequired" runat="server" ControlToValidate="ChassisNO"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Thumbnail image
                            </td>
                            <td class="input">
                                <asp:FileUpload ID="ImagesThumb" runat="server" Columns="50"></asp:FileUpload>
                                <a runat="server" id="A2" href='<%# "~/" + Eval("ImagesThumb") %>' target="_blank">
                                    <img src="images/save.png" alt="Download File" border="0" align="absmiddle" />Open</a>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Doors Number
                            </td>
                            <td class="input">
                                <asp:TextBox ID="DoorsNumber" runat="server" Columns="120" Text='<%# Bind("DoorsNumber") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="DoorsNumberRequired" runat="server" ControlToValidate="DoorsNumber"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Engine Size
                            </td>
                            <td class="input">
                                <asp:TextBox ID="EngineSize" runat="server" Columns="120" Text='<%# Bind("EngineSize") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EngineSizeRequired" runat="server" ControlToValidate="EngineSize"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Engine Size Integer Value
                            </td>
                            <td class="input">
                                <asp:TextBox ID="EngineSizeInt" runat="server" Columns="120" Text='<%# Bind("EngineSizeInt") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EngineSizeIntRequired" runat="server" ControlToValidate="EngineSizeInt"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Year
                            </td>
                            <td class="input">
                                <asp:TextBox ID="IYear" runat="server" Columns="120" Text='<%# Bind("IYear") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="IYearRequired" runat="server" ControlToValidate="IYear"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Price
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Price" runat="server" Columns="120" Text='<%# Bind("Price") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PriceRequired" runat="server" ControlToValidate="Price"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Price Integer Value
                            </td>
                            <td class="input">
                                <asp:TextBox ID="PriceInt" runat="server" Columns="120" Text='<%# Bind("PriceInt") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PriceIntRequired" runat="server" ControlToValidate="PriceInt"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Specs
                            </td>
                            <td class="input">
                                <FCKeditorV2:FCKeditor ID="Specs" runat="server" Height="400px" ToolbarSet="Default"
                                    Value='<%# Bind("Specs") %>' Width="700px" AutoDetectLanguage="false" DefaultLanguage="en"
                                    ContentLangDirection="LeftToRight">
                                </FCKeditorV2:FCKeditor>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Governarate
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Governarate" runat="server" Columns="120" Text='<%# Bind("Governarate") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="GovernarateRequired" runat="server" ControlToValidate="Governarate"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Dealer
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Dealer" runat="server" Columns="120" Text='<%# Bind("Dealer") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="DealerRequired" runat="server" ControlToValidate="Dealer"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Keywords
                            </td>
                            <td class="input">
                                <asp:TextBox ID="Keywords" runat="server" Columns="120" Text='<%# Bind("Keywords") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="KeywordsRequired" runat="server" ControlToValidate="Keywords"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="label">Images NO
                            </td>
                            <td class="input">
                                <asp:TextBox ID="ImagesNO" runat="server" Columns="120" Text='<%# Bind("ImagesNO") %>'
                                    Style="direction: ltr;"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ImagesNORequired" runat="server" ControlToValidate="ImagesNO"
                                    SetFocusOnError="True" Text="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnUpdate" runat="server" CommandName="Update" CssClass="button"
                                    Text="Update" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="false" CommandName="Cancel"
                                    CssClass="button right" Text="Cancel" />
                                <asp:Button ID="Temp" runat="server" Text='<%# Bind("ImagesThumb") %>'
                                    Style="display: none; visibility: hidden;" />

                            </td>
                        </tr>
                    </table>
                </EditItemTemplate>
            </asp:FormView>
            <div align="right" style="width: 830px; margin-top: 5px">
                <asp:LinkButton ID="lnkBack2" runat="server" CausesValidation="False" CssClass="back"
                    OnClick="lnkBack_Click" Text="Back To List"></asp:LinkButton>
            </div>
        </asp:View>
        <asp:View ID="NoAccessView" runat="server">
            <h2 id="noAccess">You are not allowed to access this section</h2>
        </asp:View>
    </asp:MultiView>
    <mb:MessageBox ID="PopupMessage" runat="server"></mb:MessageBox>
    <cb:ConfirmBox ID="ConfirmMessage" runat="server"></cb:ConfirmBox>
    <asp:ObjectDataSource ID="ListDataSource" runat="server" DeleteMethod="DeleteSearchCar"
        EnablePaging="True" SelectMethod="GetSearchCarByPage" TypeName="BMWUsedCarsLib.SearchCarsDAL" OnDeleted="ObjectDataSource_Updated"
        SelectCountMethod="GetRecordsCount" SortParameterName="sortColumn">
        <SelectParameters>
            <asp:Parameter DefaultValue="True" Name="withHidden" Type="Boolean" />
        </SelectParameters>
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="FormDataSource" runat="server" DataObjectTypeName="BMWUsedCarsLib.SearchCar"
        InsertMethod="AddSearchCar" SelectMethod="GetSearchCar" TypeName="BMWUsedCarsLib.SearchCarsDAL"
        UpdateMethod="UpdateSearchCar" OnInserted="ObjectDataSource_Updated" OnUpdated="ObjectDataSource_Updated">
        <SelectParameters>
            <asp:ControlParameter ControlID="SearchCarList" Name="id" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="SearchOds" runat="server" SelectMethod="Search" DeleteMethod="DeleteItem" TypeName="SearchCarDAL">
        <SelectParameters>
            <asp:ControlParameter Name="modelText" ControlID="SearchInModel" PropertyName="Text" Type="String" />
            <asp:ControlParameter Name="keywordsText" ControlID="SearchInKeywords" PropertyName="Text" Type="String" />
            <asp:ControlParameter Name="series" ControlID="SeriesDDL" PropertyName="Value" Type="String" />

        </SelectParameters>
         <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
    </asp:ObjectDataSource>

    <%-- <script>
        $("#ctl00_Models").addClass('selected');
    </script>--%>
</asp:Content>

