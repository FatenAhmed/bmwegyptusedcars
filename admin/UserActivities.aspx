﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/layout.master" AutoEventWireup="true" CodeFile="UserActivities.aspx.cs" Inherits="admin_UserActivities" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContentPlaceHolder" runat="Server">
    <script src="Scripts/jquery-1.4.1.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageContentPlaceHolder" runat="Server">
    <div class="PageTitle">
        <span class="nav_title">Home Page</span><span class="nav_last">User Activities</span>
    </div>
    <asp:MultiView ID="PageView" runat="server" ActiveViewIndex="0">
        <asp:View ID="PageListView" runat="server">
            <table border="0" cellpadding="2" cellspacing="1" width="750">
                <tr style="background-color: #ffffff; height: 20px;">
                    <td>
                        <asp:LinkButton ID="LinkButton1" Font-Size="10" Font-Bold="true" runat="server" OnClick="btnExport_Click">Save as excel sheet</asp:LinkButton><br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ListView ID="UsedCarList" runat="server" DataKeyNames="id" DataSourceID="ListDataSource" ViewStateMode="Disabled"
                            OnDataBound="UsedCarList_DataBound" OnItemDataBound="UsedCarList_ItemDataBound">
                            <LayoutTemplate>
                                <table id="ListHeader" runat="server" border="0" cellpadding="0" cellspacing="0"
                                    class="grid">
                                    <tr>
                                        <th>#
                                        </th>
                                        <th>User</th>
                                        <th>Role</th>
                                        <th>Chassis Number</th>

                                    </tr>
                                    <tr id="itemPlaceholder" runat="server" />
                                    <tr class="pager">
                                        <td colspan="7">
                                            <div class="container">
                                                <asp:DataPager ID="pager" runat="server" PageSize="50">
                                                    <Fields>
                                                        <asp:TemplatePagerField OnPagerCommand="PagerCommand">
                                                            <PagerTemplate>
                                                                <div class="command">
                                                                    <asp:ImageButton ID="btnFirst" runat="server" CommandName="First" ImageUrl="images/first.gif"
                                                                        AlternateText="First Page" ToolTip="First Page" />
                                                                    <asp:ImageButton ID="btnPrevious" runat="server" CommandName="Previous" ImageUrl="images/prev.gif"
                                                                        AlternateText="Previous Page" ToolTip="Previous Page" />
                                                                </div>
                                                                <div class="command">
                                                                    <asp:TextBox ID="txtSlider" runat="server" Text='<%# Container.TotalRowCount > 0 
																							? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) 
																							: 0 %>'
                                                                        AutoPostBack="true" OnTextChanged="CurrentPageChanged"
                                                                        Style="display: none" />
                                                                    <ajaxToolkit:SliderExtender ID="slider" runat="server" TargetControlID="txtSlider"
                                                                        Orientation="Horizontal" Minimum="1" Maximum='<%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows) %>'
                                                                        TooltipText='<%# "Page {0} of " + Math.Ceiling ((double)Container.TotalRowCount / Container.MaximumRows).ToString() + " (" + Container.TotalRowCount + " items)" %>' />
                                                                </div>
                                                                <div class="command">
                                                                    <asp:ImageButton ID="btnNext" runat="server" CommandName="Next" ImageUrl="images/next.gif"
                                                                        AlternateText="Next Page" ToolTip="Next Page" />
                                                                    <asp:ImageButton ID="btnLast" runat="server" CommandName="Last" ImageUrl="images/last.gif"
                                                                        AlternateText="Last Page" ToolTip="Last Page" />
                                                                </div>
                                                                <div class="info">
                                                                    Page <b>
                                                                        <%# Container.TotalRowCount > 0 ? Math.Ceiling(((double)(Container.StartRowIndex + Container.MaximumRows) / Container.MaximumRows)) : 0 %>
                                                                    </b>of <b>
                                                                        <%# Math.Ceiling((double)Container.TotalRowCount / Container.MaximumRows)%>
                                                                    </b>(<%# Container.TotalRowCount %>items)
                                                                </div>
                                                            </PagerTemplate>
                                                        </asp:TemplatePagerField>
                                                    </Fields>
                                                </asp:DataPager>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr class="row">
                                    <td class="rownum">
                                        <%# Container.DataItemIndex + 1 %>
                                    </td>

                                    <td>
                                        <asp:Label ID="UserName" runat="server" Text='<%# Eval("User.UserName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="RoleName" runat="server" Text='<%# Eval("Role.RoleName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ChassisNo" runat="server" Text='<%# Eval("ChassisNo") %>'></asp:Label>
                                    </td>


                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="altrow">
                                    <td class="rownum">
                                        <%# Container.DataItemIndex + 1 %>
                                    </td>
                                    <td>
                                        <asp:Label ID="UserName" runat="server" Text='<%# Eval("User.UserName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="RoleName" runat="server" Text='<%# Eval("Role.RoleName") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="ChassisNo" runat="server" Text='<%# Eval("ChassisNo") %>'></asp:Label>
                                    </td>

                                </tr>
                            </AlternatingItemTemplate>
                            <EmptyDataTemplate>
                                <p class="empty">
                                    No Current UsedCar Available
                                </p>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="ExcelSheet" runat="server" CellPadding="6"
                            Style="display: none">
                            <Columns>
                            </Columns>
                            <HeaderStyle CssClass="th" />
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="NoAccessView" runat="server">
            <h2 id="noAccess">You are not allowed to access this section</h2>
        </asp:View>
    </asp:MultiView>
    <mb:MessageBox ID="PopupMessage" runat="server"></mb:MessageBox>
    <cb:ConfirmBox ID="ConfirmMessage" runat="server"></cb:ConfirmBox>
    <asp:ObjectDataSource ID="ListDataSource" runat="server" EnablePaging="True" SelectMethod="GetAllItems" TypeName="UserRolesDAL"
        SelectCountMethod="GetRecordsCount"></asp:ObjectDataSource>
    <asp:ObjectDataSource ID="FormDataSource" runat="server" DataObjectTypeName="UserRole" TypeName="UserRolesDAL"></asp:ObjectDataSource>

</asp:Content>

