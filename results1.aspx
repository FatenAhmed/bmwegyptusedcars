﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="results.aspx.cs" Inherits="results" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BMW Egypt</title>
<link type="text/css" rel="stylesheet"  href="css/main.css" />
<link rel="stylesheet" href="search_files/BMW_usedcars.css">
<link href="search_files/interestform.css" rel="stylesheet" type="text/css">
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<style>
.formtbl {
	width: 500px;
	max-width: 500px;
}
.formtbl_input {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.formtbl td {
	padding: 5px 0px 5px 0px;
	vertical-align: top;
}
.frmtitle {
	padding: 5px 25px 5px 0px;
	width: 175px;
	max-width: 200px;
}
.datanote {
	padding: 5px 0px 5px 0px;
}
.frmsubmit {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #999999;
	background-color: #0d456a;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px 0px 5px 0px;
	text-align: center;
}
.rightbanner {
	margin: 5px 0px 0px 0px;
}
</style>

</head>

<body>
<div style="width:100%; height:98px; background:url(images/top-nav2.png) no-repeat; position:absolute; top:0; left:0;"></div>

<br><br>

<div class="maincontainer">
<br>

	
		<h1></h1>	
        
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: WeWantYourBMW_Link_Tag
URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2011
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
</script><iframe src="index_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=1?" width="1" height="1" frameborder="0"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<style>
.formtbl {
	width: 500px;
	max-width: 500px;
}
.formtbl_input {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.formtbl td {
	padding: 5px 0px 5px 0px;
	vertical-align: top;
}
.frmtitle {
	padding: 5px 25px 5px 0px;
	/*width: 175px;
	\width: 175px;
	w\idth: 175px;*/
	max-width: 200px;
	border-left:solid 1px #666;
}

td { border:solid 1px #666; border-left:none; border-top:none; }

.datanote {
	padding: 5px 0px 5px 0px;
}
.frmsubmit {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #999999;
	background-color: #0d456a;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px 0px 5px 0px;
	text-align: center;
}
.rightbanner {
	margin: 5px 0px 0px 0px;
}
</style>

<script>
function emailvalidation(entered, alertbox)
{
with (entered)
{
apos=value.indexOf("@");
dotpos=value.lastIndexOf(".");
lastpos=value.length-1;
if (apos<1 || dotpos-apos<2 || lastpos-dotpos>3 || lastpos-dotpos<2) 
{if (alertbox) {alert(alertbox);} return false;}
else {return true;}
}
}

function emptyvalidation(entered, alertbox)
{
with (entered)
{
if (value==null || value=="")
{if (alertbox!="") {alert(alertbox);} return false;}
else {return true;}
}
}

function formvalidation(thisform)
{
with (thisform)
{
if (emptyvalidation(per_forename,"Please enter your name.")==false) {per_forename.focus(); return false;};
if (emailvalidation(per_email,"Please enter your email.")==false) {per_email.focus(); return false;};
if (emptyvalidation(param_value1,"Please enter your current BMW.")==false) {param_value1.focus(); return false;};
if (emptyvalidation(param_value3,"Please enter your preferred means of contact")==false) {param_value3.focus(); return false;};
if (emptyvalidation(BusinessID,"Please enter your preferred dealer.")==false) {BusinessID.focus(); return false;};
}
}
</script>



<div id="content">
<h1>Submited Cars</h1>
<form id="Form1" name="form1" class="sfForm" runat="server">


    <asp:GridView ID="GridView1" runat="server" class="formtbl" 
    style="max-width: 1000px; width: 1000px;" border="0" cellpadding="0" width="1000px" 
                AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField HeaderText="Registration Number" DataField="Id" />
            <asp:BoundField HeaderText="Series / Model" DataField="Series" />
            <asp:BoundField HeaderText="Year" DataField="NYear"/>
            <asp:BoundField HeaderText="Color" DataField="Color"/>
            <asp:BoundField HeaderText="Chassis" DataField="Chassis"/>
            <asp:BoundField HeaderText="Name" DataField="UName"/>
            <asp:BoundField HeaderText="Email" DataField="Email"/>
            <asp:BoundField HeaderText="Phone" DataField="Phone"/>
            <asp:BoundField HeaderText="Current BMW" DataField="CurrentBMW"/>
             <asp:BoundField HeaderText="Registration Number" DataField="RegisterNO"/>
            <asp:BoundField HeaderText="Preferred means of contact" DataField="MeansContact"/>
             <asp:BoundField HeaderText="Dealer" DataField="Dealer"/>
            <asp:BoundField HeaderText="Additional info" DataField="Additional" />
             <asp:BoundField HeaderText="No Post" DataField="NoPost"/>
              <asp:BoundField HeaderText="No Phone" DataField="NoPhone"/>
        </Columns>
    </asp:GridView>
    <asp:Label ID="Label1" runat="server" Text="fail" Visible="false"></asp:Label>
</form>
</div></div>

</body>
</html>

