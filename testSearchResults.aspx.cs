﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class testSearchResults : System.Web.UI.Page
{
  static  string series;
  static string model;
  static string keywords;
  static string color;
  static string engine_min;
  static string engine_max;
  static string year_min;
  static string year_max;
  static string governarate;
  static string dealer;
  static string sort;
  static string perPage;
  static string price_min;
  static string price_max;
  static string doorsnumber;
    DataSet Ds;

    #region Properties
    /*
    public static string GetSeries()
    {
        return series;
    }
    public static void SetSeries(string value)
    {
        series = value;
    }
    public static string GetModel()
    {
        return model;
    }
    public static void SetModel(string value)
    {
        model = value;
    }
    public static string GetKeyWords()
    {
        return keywords;
    }
    public static void SetKeyWords(string value)
    {
        keywords = value;
    }
    public static string GetColor()
    {
        return color;
    }
    public static void SetColor(string value)
    {
        color = value;
    }
    public static string GetEngineMinimum()
    {
        return engine_min;
    }
    public static void SetEngineMinimum(string value)
    {
        engine_min = value;
    }
    public static string GetEngineMaximum()
    {
        return engine_max;
    }
    public static void SetEngineMaximum(string value)
    {
        engine_max = value;
    }
    public static string GetYearMinimum()
    {
        return year_min;
    }
    public static void SetYearMinimum(string value)
    {
        year_min = value;
    }
    public static string GetYearMaximum()
    {
        return year_max;
    }
    public static void SetYearMaximum(string value)
    {
        year_max = value;
    }
    public static string GetGovernrorate()
    {
        return governarate;
    }
    public static void SetGovernorate(string value)
    {
        governarate = value;
    }
    public static string GetDealer()
    {
        return dealer;
    }
    public static void SetDealer(string value)
    {
        dealer = value;
    }
    public static string GetSort()
    {
        return sort;
    }
    public static void SetSort(string value)
    {
        sort = value;
    }
    public static string GetPriceMin()
    {
        return price_min;
    }
    public static void SetPriceMin(string value)
    {
        price_min = value;
    }
    public static string GetPriceMax()
    {
        return price_max;
    }
    public static void SetPriceMax(string value)
    {
        price_max = value;
    }
    public static string GetDoorsNumber()
    {
        return doorsnumber;
    }
    public static void SetDoorsNumber(string value)
    {
        doorsnumber = value;
    }*/
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.PreviousPage != null)
        {
            
            DropDownList dropSeries = (DropDownList)Page.PreviousPage.FindControl("drop_series");
            if (dropSeries != null) series = dropSeries.SelectedValue;
            else series = "any";
       
            DropDownList dropColor = (DropDownList)Page.PreviousPage.FindControl("drop_color");
            if (dropColor != null) color = dropColor.SelectedValue;
            else color = "any";

            DropDownList dropDoors = (DropDownList)Page.PreviousPage.FindControl("drop_doors");
            if (dropDoors != null) doorsnumber = dropDoors.SelectedValue;
            else doorsnumber = "any";

            DropDownList dropEngineMin = (DropDownList)Page.PreviousPage.FindControl("drop_engine_min");
            if (dropEngineMin != null) engine_min = dropEngineMin.SelectedValue;
            else engine_min = "0";

            DropDownList dropEngineMax = (DropDownList)Page.PreviousPage.FindControl("drop_engine_max");
            if (dropEngineMax != null) engine_max = dropEngineMax.SelectedValue;
            else engine_max = "9999999";

            DropDownList dropYearMin = (DropDownList)Page.PreviousPage.FindControl("drop_year_min");
            if (dropYearMin != null) year_min = dropYearMin.SelectedValue;
            else year_min = "1950";

            DropDownList dropYearMax = (DropDownList)Page.PreviousPage.FindControl("drop_year_max");
            if (dropYearMax != null) year_max = dropYearMax.SelectedValue;
            else year_max = "2050";

            HtmlInputText InputPriceMin = (HtmlInputText)Page.PreviousPage.FindControl("amount_min");
            if (InputPriceMin != null)
            {
                if (InputPriceMin.Value != "") price_min = ChkString(InputPriceMin.Value);
                else price_min = "0";
                HtmlInputText InputPriceMax = (HtmlInputText)Page.PreviousPage.FindControl("amount_max");
                if (InputPriceMax.Value != "") price_max = ChkString(InputPriceMax.Value);
                else price_max = "9999999";
            }
            else
            {
                TextBox txtboxPriceMin = (TextBox)Page.PreviousPage.FindControl("txt_price_min");
                if (txtboxPriceMin.Text != "") price_min = txtboxPriceMin.Text;
                else price_min = "0";
                TextBox txtboxPriceMax = (TextBox)Page.PreviousPage.FindControl("txt_price_max");
                if (txtboxPriceMax.Text != "") price_max = txtboxPriceMax.Text;
                else price_max = "9999999";
            }

            DropDownList dropCountry = (DropDownList)Page.PreviousPage.FindControl("drop_country");
            if (dropCountry != null) governarate = dropCountry.SelectedValue;
            else governarate = "any";

            DropDownList dropDealer = (DropDownList)Page.PreviousPage.FindControl("drop_dealer");
            if (dropDealer != null) dealer = dropDealer.SelectedValue;
            else dealer = "any";
        }
  
          string[] ParamsArray = { "@Series", "@Governarate", "@Dealer", "@DoorsNumber", "@Color", "Year_Min", "Year_Max", "Price_Min", "Price_Max", "Engine_Min", "Engine_Max" };
      string[] valuesArray = { series, governarate, dealer, doorsnumber, color, year_min, year_max, price_min, price_max, engine_min, engine_max };

        Database datab = new Database();

        Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-Thumbs");

        if (Ds.Tables.Count > 0)
        {
            HomeSearchPics.DataSource = Ds;
            if (Ds.Tables[0].Rows.Count == 1)
                td_noOfRows.InnerText = "Displaying 1 result";
            else
                td_noOfRows.InnerText = "Displaying " + Ds.Tables[0].Rows.Count.ToString() + " results";

            HomeSearchPics.DataBind();
        }
   
    }
    public string ChkString(string word)
    {
        string number = word.Trim();
        number = number.Replace(",", "");
        return number;
    }
} 
/*     series = Request.Form("drop_series");
        Response.Write(series);*/

/*   TextBox txtboxModel = (TextBox)Page.PreviousPage.FindControl("txtbox_model");
          model = txtboxModel.Text;
          TextBox txtboxKeywords = (TextBox)Page.PreviousPage.FindControl("txtbox_keyword");
          keywords = txtboxKeywords.Text;*/