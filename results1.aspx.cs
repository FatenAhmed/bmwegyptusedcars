﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class results : System.Web.UI.Page
{
    DataSet Ds;
    protected void Page_Load(object sender, EventArgs e)
    {
        string[] ParamsArray = { };
        string[] valuesArray = {};
        Database datab = new Database();
        Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-WantCars");

        if (Ds.Tables.Count > 0)
        {
            GridView1.DataSource = Ds;
            GridView1.DataBind();
        }
        else
        { Label1.Visible = true; }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
