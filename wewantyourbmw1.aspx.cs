﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class wewantyourbmw : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_send_Click(object sender, EventArgs e)
    {
        string series = input_series.Value;
        string year = input_year.Value;
        string color = input_color.Value;
        string chassis = input_chassis.Value;
        string name = input_name.Value;
        string email = input_email.Value;
        string phone = input_phone.Value;
        string current = input_current.Value;
        string register = input_registerno.Value;
        string means = input_means.Value;
        string additonal = input_additional.Value;
        string dealer = input_dealer.Value;
        string nopost = input_nopost.Value;
        string nophone = input_phone.Value;

        Database datab = new Database();

        string[] ParamsArray = new string[14] { "@Series", "@NYear", "@Color", "@Chassis", "@Name", "@Email", "@Phone",
            "@CurrentBMW", "@RegisterNO", "@MeansContact", "@Additional", "@Dealer", "@NoPost", "@NoPhone" };
        string[] valuesArray = new string[14] { series, year, color, chassis, name, email, phone, 
            current, register, means, additonal, dealer, nopost, nophone };
        string result = datab.doStoredProcedure2(valuesArray, ParamsArray, "Add-WantCars");

        if (result == "Success")
            input_additional.Value = "tmam";
        else
        {
            input_additional.Value = result;
        }
    }
}
