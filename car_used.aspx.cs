﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class car_used : System.Web.UI.Page
{
    string id;
    DataSet Ds;
    public string img3Exist, img4Exist, img5Exist, img6Exist = "0";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            id = Request.QueryString["Id"];
            string[] ParamsArray = { "@Id" };
            string[] valuesArray = { id };

            Database datab = new Database();

            Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-UsedCarDetails");

            if (Ds.Tables.Count > 0)
            {
                bigimg_1.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_0_b.jpg";
                bigimg_1.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_1.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_2.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_1_b.jpg";
                bigimg_2.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_2.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];

                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_b.jpg")))
                {
                    bigimg_3.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_b.jpg";
                    bigimg_3.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    bigimg_3.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    img3Exist = "1";
                }
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_b.jpg")))
                {
                    bigimg_4.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_b.jpg";
                    bigimg_4.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    bigimg_4.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    img4Exist = "1";
                }
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_b.jpg")))
                {
                    bigimg_5.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_b.jpg";
                    bigimg_5.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    bigimg_5.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    img5Exist = "1";
                }
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_b.jpg")))
                {
                    bigimg_6.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_b.jpg";
                    bigimg_6.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    bigimg_6.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                    img6Exist = "1";
                }

                li_img1.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_0_t.jpg";
                li_img2.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_1_t.jpg";

                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_t.jpg")))
                    li_img3.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_t.jpg";
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_t.jpg")))
                    li_img4.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_t.jpg";
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_t.jpg")))
                    li_img5.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_t.jpg";
                if (File.Exists(Server.MapPath("~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_t.jpg")))
                    li_img6.Attributes["src"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_t.jpg";

                li_img1.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img2.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img3.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img4.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img5.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img6.Attributes["alt"] = "~/UsedCars_Images/MyImages/" + Ds.Tables[0].Rows[0]["Series"];

                //if (Ds.Tables[0].Rows[0]["Model"].ToString() == "Sport")
                //    txt_model.InnerText = "X1 Sport";
                //else if (Ds.Tables[0].Rows[0]["Model"].ToString() == " ")
                //    txt_model.InnerText = "X1";
                //else 
                //txt_model.InnerText = Ds.Tables[0].Rows[0]["Model"].ToString();

                if (Ds.Tables[0].Rows[0]["Model"].ToString() == " ")
                    txt_model.InnerText = "X1";
                else
                    txt_model.InnerText = Ds.Tables[0].Rows[0]["Series"].ToString() + ' ' + Ds.Tables[0].Rows[0]["Model"].ToString();



                if (Ds.Tables[0].Rows[0]["EngineSize"].ToString() == "2000")
                    txt_engine.InnerText = "2.0 L";
                else
                    txt_engine.InnerText = Ds.Tables[0].Rows[0]["EngineSize"].ToString().ToUpper(); ;

                Div1.InnerHtml = "<strong>" +
                    Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"]
                    + "</strong>";
                label_strong.InnerHtml = "<strong>" +
                    Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"]
                    + "</strong>";
                txt_price.InnerText = Ds.Tables[0].Rows[0]["Price"].ToString() + " EGP";
                txt_year.InnerText = Ds.Tables[0].Rows[0]["IYear"].ToString();
                txt_color.InnerText = Ds.Tables[0].Rows[0]["Color"].ToString();
                txt_doors.InnerText = Ds.Tables[0].Rows[0]["DoorsNumber"].ToString();
                div_specs.InnerHtml = Ds.Tables[0].Rows[0]["Specs"].ToString();
                txt_milage.InnerText = Ds.Tables[0].Rows[0]["Milage"].ToString() + " KM";
                input_chassis.InnerText = Ds.Tables[0].Rows[0]["ChassisNO"].ToString();


                if (Ds.Tables[0].Rows[0]["Dealer"].ToString() == "Bavarian Auto - Katameya4")
                {
                    txt_dealer.InnerHtml = "Bavarian Auto Center,<br/>Area 29 Service Centers Zone,<br/>Industrial Zone Of Katameya,<br />Cairo - Egypt.<br />Tel: +2 - 272 - 722 41<br />	Fax: +2 – 272 – 72249<br />	";
                }
                else
                {
                    txt_dealer.InnerHtml = Ds.Tables[0].Rows[0]["Dealer"].ToString();
                }

            }
        }
    }
}
