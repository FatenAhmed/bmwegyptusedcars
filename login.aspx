﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BMW Egypt</title>
<link type="text/css" rel="stylesheet"  href="css/main.css" />
<link rel="stylesheet" href="search_files/BMW_usedcars.css">
<link href="search_files/interestform.css" rel="stylesheet" type="text/css">
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<style>
.formtbl {
	width: 500px;
	\width: 500px;
	w\idth: 500px;
	max-width: 500px;
}
.formtbl_input {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.formtbl td {
	padding: 5px 0px 5px 0px;
	vertical-align: top;
	border-bottom: 1px solid #F9F9F9;
}
.frmtitle {
	padding: 5px 25px 5px 0px;
	width: 175px;
	\width: 175px;
	w\idth: 175px;
	max-width: 200px;
}
.datanote {
	padding: 5px 0px 5px 0px;
}
.frmsubmit {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #999999;
	background-color: #0d456a;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px 0px 5px 0px;
	text-align: center;
}
.rightbanner {
	margin: 5px 0px 0px 0px;
}
</style>

</head>

<body>
<div style="width:100%; height:98px; background:url(images/top-nav2.png) no-repeat; position:absolute; top:0; left:0;"></div>

<br><br>

<div class="maincontainer">
<br>

	
		<h1></h1>	
        
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: WeWantYourBMW_Link_Tag
URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2011
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
</script><iframe src="index_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=1?" width="1" height="1" frameborder="0"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<style>
.formtbl {
	width: 500px;
	\width: 500px;
	w\idth: 500px;
	max-width: 500px;
}
.formtbl_input {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.formtbl td {
	padding: 5px 0px 5px 0px;
	vertical-align: top;
	border-bottom: 1px solid #F9F9F9;
}
.frmtitle {
	padding: 5px 25px 5px 0px;
	width: 175px;
	\width: 175px;
	w\idth: 175px;
	max-width: 200px;
}
.datanote {
	padding: 5px 0px 5px 0px;
}
.frmsubmit {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #999999;
	background-color: #0d456a;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px 0px 5px 0px;
	text-align: center;
}
.rightbanner {
	margin: 5px 0px 0px 0px;
}
</style>

<script>
function emailvalidation(entered, alertbox)
{
with (entered)
{
apos=value.indexOf("@");
dotpos=value.lastIndexOf(".");
lastpos=value.length-1;
if (apos<1 || dotpos-apos<2 || lastpos-dotpos>3 || lastpos-dotpos<2) 
{if (alertbox) {alert(alertbox);} return false;}
else {return true;}
}
}

function emptyvalidation(entered, alertbox)
{
with (entered)
{
if (value==null || value=="")
{if (alertbox!="") {alert(alertbox);} return false;}
else {return true;}
}
}

function formvalidation(thisform)
{
with (thisform)
{
if (emptyvalidation(per_forename,"Please enter your name.")==false) {per_forename.focus(); return false;};
if (emailvalidation(per_email,"Please enter your email.")==false) {per_email.focus(); return false;};
if (emptyvalidation(param_value1,"Please enter your current BMW.")==false) {param_value1.focus(); return false;};
if (emptyvalidation(param_value3,"Please enter your preferred means of contact")==false) {param_value3.focus(); return false;};
if (emptyvalidation(BusinessID,"Please enter your preferred dealer.")==false) {BusinessID.focus(); return false;};
}
}
</script>


<form runat="server" >
<div id="content">
<h1>Login</h1>


 <!-- Pass in fuseaction -->
		  <input name="fuseaction" value="formaction" type="Hidden">
		  <!-- leadtypeID : we want your bmw  -->
		  <input name="leadtypeID" value="62" type="hidden">		
		  <input name="originatorID" value="890" type="hidden">				  
		  <!-- StatusId 1 is 'Awaiting Dealer Response' --> 
		  <input name="leadstatusID" value="1" type="Hidden">
		  
		  <input name="returnURL" value="/configurator/index.cfm?fuseaction=bmw.wewantyourbmwthanks" type="hidden">
<table class="formtbl" border="0" cellpadding="0" cellspacing="0" width="680">
  <tbody>
  <tr>
    <td class="frmtitle">Username:*</td>
    <td>
    <input name="per_forename" class="formtbl_input" type="text" id="txt_name" runat="server">
        <asp:RequiredFieldValidator ID="txt_name_req" runat="server" ErrorMessage="*" ControlToValidate="txt_name"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr>
    <td class="frmtitle">Password:*</td>
    <td>
    <input name="per_forename" class="formtbl_input" type="password" id="txt_pass" runat="server">
    <asp:RequiredFieldValidator ID="txt_pass_req" runat="server" ErrorMessage="*" ControlToValidate="txt_pass"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
   <%-- <input name="Submit" value="Login" class="frmsubmit" type="submit">--%>
        <asp:Button ID="btn_submit" runat="server" class="frmsubmit" Text="Login" 
            onclick="btn_submit_Click" />
        <asp:Label ID="Label1" runat="server" Text="Login failed!" Visible="False" 
            Font-Bold="True" ForeColor="Red" ></asp:Label>
    </td>
  </tr>


    
	
	<!-- Begin Breadcrumb Trail --->

	

<!-- End Breadcrumb Trail -->
	<!---->
</tbody>

</table>



</div>
</form>
</div>


</body>
</html>
