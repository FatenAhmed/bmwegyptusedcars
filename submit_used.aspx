﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="submit_used.aspx.cs" Inherits="submit_used" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>BMW Egypt</title>
    <link type="text/css" rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="search_files/BMW_usedcars.css">
    <link href="search_files/interestform.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="jquery-1.11.2.min.js"></script>

    <script type="text/javascript" src="contentFrameResizer.js"></script>
    <script type="text/javascript" src="iframeIntegrationLib.js"></script>

    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
    <style>
        .formtbl {
            width: 500px;
            max-width: 500px;
        }

        .formtbl_input {
            width: 300px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }

        .formtbl td {
            padding: 5px 0px 5px 0px;
            vertical-align: top;
            border-bottom: 1px solid #F9F9F9;
        }

        .frmtitle {
            padding: 5px 25px 5px 0px;
            width: 175px;
            max-width: 200px;
        }

        .datanote {
            padding: 5px 0px 5px 0px;
        }

        .frmsubmit {
            width: 300px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            border: 1px solid #999999;
            background-color: #0d456a;
            color: #FFFFFF;
            font-weight: bold;
            padding: 5px 0px 5px 0px;
            text-align: center;
        }

        .rightbanner {
            margin: 5px 0px 0px 0px;
        }
    </style>

</head>

<body>
    <div style="width: 100%; height: 98px; background: url(images/top-nav2.png) no-repeat; position: absolute; top: 0; left: 0;"></div>

    <br />
    <br />

    <div class="maincontainer">
        <br />


        <h1></h1>

        <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: WeWantYourBMW_Link_Tag
URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2011
-->
        <script type="text/javascript">
            var axel = Math.random() + "";
            var a = axel * 10000000000000;
            document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
        </script>
        <iframe src="index_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
        <noscript>
            <iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=1?" width="1" height="1" frameborder="0"></iframe>
        </noscript>

        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->
        <style>
            .formtbl {
                width: 500px;
                \width: 500px;
                w\idth: 500px;
                max-width: 500px;
            }

            .formtbl_input {
                width: 300px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
            }

            .formtbl td {
                padding: 5px 0px 5px 0px;
                vertical-align: top;
                border-bottom: 1px solid #F9F9F9;
            }

            .frmtitle {
                padding: 5px 25px 5px 0px;
                width: 175px;
                max-width: 200px;
            }

            .datanote {
                padding: 5px 0px 5px 0px;
            }

            .frmsubmit {
                width: 300px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                border: 1px solid #999999;
                background-color: #0d456a;
                color: #FFFFFF;
                font-weight: bold;
                padding: 5px 0px 5px 0px;
                text-align: center;
            }

            .rightbanner {
                margin: 5px 0px 0px 0px;
            }
        </style>

        <script>
            function emailvalidation(entered, alertbox) {
                with (entered) {
                    apos = value.indexOf("@");
                    dotpos = value.lastIndexOf(".");
                    lastpos = value.length - 1;
                    if (apos < 1 || dotpos - apos < 2 || lastpos - dotpos > 3 || lastpos - dotpos < 2) { if (alertbox) { alert(alertbox); } return false; }
                    else { return true; }
                }
            }

            function emptyvalidation(entered, alertbox) {
                with (entered) {
                    if (value == null || value == "") { if (alertbox != "") { alert(alertbox); } return false; }
                    else { return true; }
                }
            }

            function formvalidation(thisform) {
                with (thisform) {
                    if (emptyvalidation(per_forename, "Please enter your name.") == false) { per_forename.focus(); return false; };
                    if (emailvalidation(per_email, "Please enter your email.") == false) { per_email.focus(); return false; };
                    if (emptyvalidation(param_value1, "Please enter your current BMW.") == false) { param_value1.focus(); return false; };
                    if (emptyvalidation(param_value3, "Please enter your preferred means of contact") == false) { param_value3.focus(); return false; };
                    if (emptyvalidation(BusinessID, "Please enter your preferred dealer.") == false) { BusinessID.focus(); return false; };
                }
            }
        </script>



        <div id="content">

            <form id="Form1" name="form1" class="sfForm" runat="server">
                <asp:MultiView ID="PageView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="ViewSubmit" runat="server">

                        <h1>Submit a Car</h1>


                        <div class="sfElementTitle">CAR DESCRIPTION</div>

                        <input name="MakeID" value="3" type="hidden" />
                        <input name="xMakeID" value="" type="hidden" />
                        <div class="sfElement">
                            <div class="sfCaption">Series:</div>
                            <div class="sfInput">

                                <select name="ModelID" tabindex="3" class="sfmenu" id="tab1" runat="server">
                                    <option value="1 Series">1 Series</option>
                                    <option value="2 Series">2 Series</option>
                                    <option value="3 Series">3 Series</option>
                                    <option value="4 Series">4 Series</option>
                                    <option value="5 Series">5 Series</option>
                                    <option value="6 Series">6 Series</option>
                                    <option value="7 Series">7 Series</option>
                                    <option value="M3">M3</option>
                                    <option value="M5">M5</option>
                                    <option value="M6">M6</option>
                                    <option value="X1">X1</option>
                                    <option value="X3">X3</option>
                                    <option value="X5">X5</option>
                                    <option value="X6">X6</option>
                                    <option value="Z4">Z4</option>
                                    <option value="I Series">I</option>
                                    <option value="X7">X7</option>
                                    <option value="8 Series">8 Series</option>
                                </select>
                                <input name="xModelID" value="" type="hidden" />

                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">Model:</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_model" runat="server" />
                                <%-- <asp:RequiredFieldValidator ID="model_req" runat="server" ErrorMessage="*" ControlToValidate="input_model"></asp:RequiredFieldValidator>
                                --%>
                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">Keyword:</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_keys" runat="server" />
                                <%--  <asp:RequiredFieldValidator ID="input_keys_req" runat="server" ErrorMessage="*" ControlToValidate="input_keys"></asp:RequiredFieldValidator>
                                --%>
                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">Colour:</div>
                            <div class="sfInput">

                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_color" runat="server" />
                                <asp:RequiredFieldValidator ID="input_color_req" runat="server" ErrorMessage="*" ControlToValidate="input_color"></asp:RequiredFieldValidator>
                                <input name="xColourID" value="" type="hidden" />

                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">No. of Doors:</div>
                            <div class="sfInput">
                                <select name="Doors" tabindex="6" class="sfmenu" id="tab_doors" runat="server">
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>


                        <div class="sfElement">
                            <div class="sfCaption">Chassis no.</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_chassis" runat="server" />
                                <asp:RequiredFieldValidator ID="input_chassis_req" runat="server" ErrorMessage="*" ControlToValidate="input_chassis"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">Milage</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_milage" runat="server" />
                                <asp:RequiredFieldValidator ID="input_milage_req" runat="server" ErrorMessage="*" ControlToValidate="input_milage"></asp:RequiredFieldValidator>
                            </div>
                        </div>


                        <div class="sfElementTitle">ENGINE SIZE</div>

                        <div class="sfElement">
                            <div class="sfInput">

                                <select name="Engine_Min" tabindex="10" class="sfmenu" id="tab_engine" runat="server">
                                    <option value="1.5 L">1.5 L</option>
                                    <option value="1.6 L">1.6 L</option>
                                    <option value="1.7 L">1.7 L</option>
                                    <option value="1.8 L">1.8 L</option>
                                    <option value="1.9 L">1.9 L</option>
                                    <option value="2.0 L">2.0 L</option>
                                    <option value="2.1 L">2.1 L</option>
                                    <option value="2.2 L">2.2 L</option>
                                    <option value="2.3 L">2.3 L</option>
                                    <option value="2.4 L">2.4 L</option>
                                    <option value="2.5 L">2.5 L</option>
                                    <option value="2.6 L">2.6 L</option>
                                    <option value="2.7 L">2.7 L</option>
                                    <option value="2.8 L">2.8 L</option>
                                    <option value="2.9 L">2.9 L</option>
                                    <option value="3.0 L">3.0 L</option>
                                    <option value="3.1 L">3.1 L</option>
                                    <option value="3.2 L">3.2 L</option>
                                    <option value="3.3 L">3.3 L</option>
                                    <option value="3.4L">3.4L</option>
                                    <option value="3.5L">3.5L</option>
                                    <option value="3.6L">3.6L</option>
                                    <option value="3.7L">3.7L</option>
                                    <option value="3.8L">3.8L</option>
                                    <option value="3.9L">3.9L</option>
                                    <option value="4.0L">4.0L</option>
                                    <option value="4.1L">4.1L</option>
                                    <option value="4.2L">4.2L</option>
                                    <option value="4.3L">4.3L</option>
                                    <option value="4.4L">4.4L</option>
                                    <option value="4.5L">4.5L</option>
                                </select>

                            </div>
                        </div>
                        <div class="sfElement">
                            <div class="sfCaption">ENGINE SIZE IN CC</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="input_engine_cc" runat="server" />
                                <asp:RequiredFieldValidator ID="input_engine_cc_req" runat="server" ErrorMessage="*" ControlToValidate="input_engine_cc"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="sfElementTitle">YEAR</div>

                        <div class="sfElement">
                            <div class="sfInput">

                                <select name="Year_Min" tabindex="13" class="sfmenu" id="tab_year" runat="server">
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <%-- <option value="2012">2017</option>
                                    <option value="2013">2018</option>
                                    <option value="2014">2019</option>
                                    <option value="2015">2020</option>
                                    <option value="2016">2021</option>--%>
                                </select>

                            </div>
                        </div>



                        <div class="sfElementTitle">PRICE L.E.</div>
                        <div class="sfElement">
                            <div class="sfInput">
                                <input name="Price_Min" id="input_price" runat="server" tabindex="16" class="sfmenu" size="10" type="text" />
                                <asp:RequiredFieldValidator ID="input_price_req" runat="server" ErrorMessage="*" ControlToValidate="input_price"></asp:RequiredFieldValidator>
                                <input name="Price_Min_integer" value="Minimum price entered was not an integer." type="hidden" />
                            </div>
                        </div>

                        <div class="sfElementTitle">Car Specifications</div>
                        <div class="sfElement">
                            <div class="sfInput">
                                <FCKeditorV2:FCKeditor ID="TestDetails" runat="server" Height="300px"
                                    ToolbarSet="Custom" Value="" Width="600px">
                                </FCKeditorV2:FCKeditor>
                            </div>
                        </div>


                        <div class="sfElementTitle">LOCATION</div>

                        <div class="sfElement">
                            <div class="sfCaption">Governarate:</div>
                            <div class="sfInput">

                                <select name="CountyID" tabindex="19" id="tab_govern" runat="server" onchange="business_countygarage(document.form1, true);" class="sfmenu">
                                    <option value="Cairo">Cairo</option>
                                    <option value="Giza">Giza</option>
                                    <option value="Alexanderia">Alexanderia</option>
                                    <option value="Suez">Suez</option>
                                    <option value="El Menia">El Menia</option>
                                    <option value="Luxor">Luxor</option>
                                    <option value="El Sharkia">El Sharkia</option>
                                </select>
                                <input name="xCountyID" value="0" type="hidden" />

                            </div>
                        </div>

                        <div class="sfElement">
                            <div class="sfCaption">Dealer:</div>
                            <div class="sfInput">

                                <select name="BusinessID" tabindex="20" class="sfmenu" id="tabs_dealer" runat="server">
                                    <option value="Bavarian Auto Company - Alex Branch">Bavarian Auto Company - Alex Branch</option>
                                    <option value="Bavarian Auto - Almaza">Bavarian Auto - Almaza</option>
                                    <option value="Bavarian Auto - Desert Road">Bavarian Auto - Desert Road</option>
                                    <option value="Bavarian Auto - Katameya4">Bavarian Auto - Katameya4</option>
                                    <option value="Bavarian Auto - Katameya5">Bavarian Auto - Katameya5</option>
                                    <option value="Bavarian Auto - Mohandessin">Bavarian Auto - Mohandessin</option>
                                    <option value="Bavarian Automotive company">Bavarian Automotive company</option>

                                </select>
                                <input name="xBusinessID" value="0" type="hidden">
                            </div>
                        </div>


                        <div class="sfElementTitle">Related Images</div>

                        <div class="sfElement">
                            <div class="sfInput">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:RequiredFieldValidator ID="FileUpload1_req" runat="server" ErrorMessage="*" ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>
                                <asp:FileUpload ID="FileUpload2" runat="server" />
                                <asp:RequiredFieldValidator ID="FileUpload2_req" runat="server" ErrorMessage="*" ControlToValidate="FileUpload2"></asp:RequiredFieldValidator>
                                <asp:FileUpload ID="FileUpload3" runat="server" />
                                <asp:FileUpload ID="FileUpload4" runat="server" />
                                <asp:FileUpload ID="FileUpload5" runat="server" />
                                <asp:FileUpload ID="FileUpload6" runat="server" />
                            </div>
                        </div>


                        <div class="sfSubmitButton">
                            <asp:Button ID="btn_submit" class="sfSubmitButton" runat="server" type="submit"
                                Text="Submit" OnClick="btn_submit_Click" />

                            <asp:Label ID="Label1" class="sfElementTitle" runat="server" Text="Succeeded"
                                Visible="False"></asp:Label>
                        </div>
                        <br />
                        <asp:Button ID="btn_goToDelete" runat="server" Text="Delete??"
                            OnClick="btn_goToDelete_Click" CausesValidation="false" />
                    </asp:View>

                    <asp:View ID="DeleteView" runat="server">

                        <h1>Delete a Car</h1>
                        <div class="sfElement">
                            <div class="sfCaption">Chassis no.</div>
                            <div class="sfInput">
                                <input name="searchtext" tabindex="4" class="sfmenu" id="inp_del_chassis" runat="server" />
                                <asp:RequiredFieldValidator ID="inp_del_chassis_req" runat="server" ErrorMessage="*" ControlToValidate="inp_del_chassis"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="sfSubmitButton">
                            <asp:Button ID="btn_delete" class="sfSubmitButton" runat="server" type="submit"
                                Text="Delete" OnClick="btn_delete_Click" />

                            <asp:Label ID="Label2" class="sfElementTitle" runat="server" Text="Deleted"
                                Visible="False"></asp:Label>
                        </div>

                        <br />
                        <asp:Button ID="btn_goToSubmit" runat="server" Text="Submit again??"
                            OnClick="btn_goToSubmit_Click" CausesValidation="false" />

                    </asp:View>

                </asp:MultiView>

            </form>
        </div>

    </div>


</body>
</html>
