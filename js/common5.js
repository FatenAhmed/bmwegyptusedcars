// Peter Crowley, WebZone Limited 17/01/2002

// MacroMedia stuff
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

// WebZone select fill stuff

function WZ_fillChild(theArray,thisField, selectedParentIndex, selectedChildValue, bValues, bHasChildren) {
	if (selectedParentIndex == 0) return; 
	if (bValues) jump=bHasChildren?3:2; else jump=bHasChildren?2:1;
    WZ_showField(theArray[(bValues?3:2)*selectedParentIndex-1], jump, thisField,selectedChildValue, bValues);
}

function WZ_fillParent(theArray,thisField, selectedValue, bValues) {
    WZ_showField(theArray, bValues?3:2, thisField, selectedValue, bValues);
}

function WZ_fillGrandChild(theArray,thisField, selectedGrandParentIndex, GrandParentWidth, selectedParentIndex, ParentWidth, selectedChildValue, thisWidth) {
	if (selectedParentIndex == 0) return; 
    WZ_showField(theArray[GrandParentWidth*selectedGrandParentIndex-1][ParentWidth*selectedParentIndex-1], thisWidth, thisField,selectedChildValue, false);
}

function WZ_showField(Obj, jump, thisField, selectedValue, bValues) {
	var bSelect, sValue;
	var iOption = 1;

	if (thisField.tagName != 'SELECT')
		return;
    thisField.options.length = 1;
    for (var iArray = 0; iArray < Obj.length-1;iArray+=jump) {
		if (selectedValue && (Obj[iArray] == selectedValue)) bSelect=true; else bSelect=false;
		if (bValues)
			sValue= Obj[iArray+1]; // value is different to text
		else
			sValue= Obj[iArray]; // value is the same as text
    	thisField.options[iOption] = new Option(sValue, Obj[iArray], false, bSelect);
    	iOption++;
    }

	thisField.disabled = false;
}

function WZ_fillMappedGrandChild(theArray,thisField, selectedGrandParentIndex, GrandParentWidth, selectedParentIndex, ParentWidth, selectedChildValue, thisWidth, oText, textWidth) {
	if (selectedParentIndex == 0) return; 
    WZ_showMappedField(theArray[GrandParentWidth*selectedGrandParentIndex-1][ParentWidth*selectedParentIndex-1], thisWidth, thisField,selectedChildValue, oText, textWidth);
}

function WZ_showMappedField(Obj, jump, thisField, selectedValue, oText, textJump) {
	var bSelect;
	var iOption = 1;
	var sText;

	if (thisField.tagName != 'SELECT')
		return;
	thisField.options.length = 1;
	oSelectOption = 0;
	
    for (var iArray = 0; iArray < Obj.length-1;iArray+=jump) {
		if (selectedValue && Obj[iArray] == selectedValue) bSelect=true; else bSelect=false;
		sText=Obj[iArray];
		for (var iText = 0; iText < oText.length-1;iText+=textJump) {
		  if (oText[iText] == sText) {
			oSelectOption = new Option(oText[iText+1],Obj[iArray],bSelect,bSelect);
			break;
		  }
		}
		if (oSelectOption) {
	    	thisField.options[iOption] = oSelectOption;
			iOption++;
		}
    }
	thisField.disabled = false;
}

function WZ_storefield(SelectField,HiddenField) {
	HiddenField.value = SelectField.options[SelectField.selectedIndex].value;
}

function WZ_openWin( windowURL, windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ) ; 
} 
function openWin( windowURL, windowName, windowFeatures ) { 
	return window.open( windowURL, windowName, windowFeatures ) ; 
} 


// Javascript for used car forms

// Fill a select triplet MakeID, ModelID, ColourID
// Need a matching xMakeID, xModeID and xColour to enable remeber on back feature.
// Note: Colours are mapped from a separate array

function init_makemodelcolour(currentForm) {
  if (window.Option) {
  with (currentForm) {
	if (MakeID.tagName != 'SELECT')
	  WZ_fillChild(MakeModelColourList, ModelID, 1, xModelID.value, true, true);
	else {
	  WZ_fillParent(MakeModelColourList, MakeID, xMakeID.value, true);
	  if (xMakeID.value > 0) {
		WZ_fillChild(MakeModelColourList, ModelID, MakeID.selectedIndex, xModelID.value, true, true);
	  }
	}
    colour_makemodelcolour(currentForm,xColourID.value);
  }
  }
}

function submit_makemodelcolour(currentForm) {
  with (currentForm) {
	if (MakeID.tagName == 'SELECT') WZ_storefield(MakeID, xMakeID);
	WZ_storefield(ModelID, xModelID);
	WZ_storefield(ColourID, xColourID);
  }
  s_fsform(currentForm)
}

function FillBaseColours()
{
	var c,f,o
	if (f=document.form1.elements["ColourID"])
	{
		for (c=1;c<BaseColourList.length;c+=2)
			f.options[f.options.length]=new Option(BaseColourList[c],BaseColourList[c-1])
	}
}

function colour_makemodelcolour(currentForm,colourValue) {
  with(currentForm) { 
  	if (MakeID.value > 0 && ModelID.value > 0) {
	  WZ_fillMappedGrandChild(MakeModelColourList, ColourID, (MakeID.tagName != 'SELECT')?1:MakeID.selectedIndex, 3, ModelID.selectedIndex, 3, colourValue, 1, BaseColourList, 2);
	} else
	  WZ_showField(BaseColourList, 2, ColourID, colourValue, true);
  }
}

// Fill a select triplet MakeID, ModelID, YearID
// Need a matching xMakeID, xModeID and xYear to enable remeber on back feature.

function init_makemodelyear(currentForm) {
  if (window.Option) {
  with (currentForm) {
	if (MakeID.tagName != 'SELECT')
	  WZ_fillChild(MakeModelYearList, ModelID, 1, xModelID.value, true, true);
	else {
	  WZ_fillParent(MakeModelYearList, MakeID, xMakeID.value, true); 
	  if (xMakeID.value > 0) {
		WZ_fillChild(MakeModelYearList, ModelID, MakeID.selectedIndex, xModelID.value, true, true);
		if (xModelID.value > 0) {
		  WZ_fillGrandChild(MakeModelYearList, Year, MakeID.selectedIndex, 3, ModelID.selectedIndex, 3, xYear.value, 1);
		}
	  }
	}
  }
  }
}

function submit_makemodelyear(currentForm) {
  with (currentForm) {
	if (MakeID.tagName == 'SELECT') WZ_storefield(MakeID, xMakeID);
	WZ_storefield(ModelID, xModelID);
	WZ_storefield(Year, xYear);
  }
}

// Fill a select couple MakeID and ModelID
// Need a matching xMakeID and xModeID to enable remeber on back feature.

function init_makemodel(currentForm) {
  if (window.Option) {
  with (currentForm) {
	if (MakeID.tagName != 'SELECT')
	  WZ_fillChild(MakeModelList, ModelID, 1, xModelID.value, true, false);
	else {
	  WZ_fillParent(MakeModelList, MakeID, xMakeID.value, true); 
	  if (xMakeID.value > 0) {
		WZ_fillChild(MakeModelList, ModelID, MakeID.selectedIndex, xModelID.value, true, false);
	  }
	}
  }
  }
}

function submit_makemodel(currentForm) {
  with (currentForm) {
	if (MakeID.tagName == 'SELECT') WZ_storefield(MakeID, xMakeID);
	WZ_storefield(ModelID, xModelID);
  }
  s_fsform(currentForm)
}

function s_fsform(formObj)
{
  var mk,md

  if (formObj.make)
  	mk=formObj.make
  else if (document.getElementById("s_fsform_make")) //IE FIX
  	mk=document.getElementById("s_fsform_make")
  else
  {
  	mk=document.createElement("input")
	mk.type="hidden"
	mk.name="make"
	mk.id="s_fsform_make"
	formObj.appendChild(mk)
  }

  if (formObj.model)
	md=formObj.model
  else if (document.getElementById("s_fsform_model")) //IE FIX
  	md=document.getElementById("s_fsform_model")
  else
  {
  	md=document.createElement("input")
	md.type="hidden"
	md.name="model"
	md.id="s_fsform_model"
	formObj.appendChild(md)
  }

	if (formObj.MakeID)
	{
		if (formObj.MakeID.selectedIndex)
			mk.value=formObj.MakeID.options[formObj.MakeID.selectedIndex].text
		else
			mk.value=""
	}
	if (formObj.ModelID)
	{
		if (formObj.ModelID.selectedIndex)
			md.value=formObj.ModelID.options[formObj.ModelID.selectedIndex].text
		else
			md.value=""
	}

		
}

// Javascript for dealer forms

// Fill a select pair CountyID, BusinessID
// Need a matching xCountyID and xBusinessID to enable remeber on back feature.

function init_countygarage(currentForm,showGarage) {
  if (window.Option) {
  with (currentForm) {
	WZ_fillParent(CountyGarageList, CountyID, xCountyID.value, true);
	if (xCountyID.value > 0) {
		WZ_fillChild(CountyGarageList, BusinessID, CountyID.selectedIndex, xBusinessID.value, true, false);
	} else {
	    if (showGarage)
			WZ_showField(GarageList, 2, BusinessID, xBusinessID.value, true);
	}
  }
  }
}

function business_countygarage(currentForm,showGarage) {
  if (window.Option) {
  with (currentForm) {
    if (CountyID.selectedIndex == 0 && showGarage)
		WZ_showField(GarageList, 2, BusinessID, xBusinessID.value, true);
	else
		WZ_fillChild(CountyGarageList, BusinessID, CountyID.selectedIndex, 1, true, false);
  }
  }
}

function submit_countygarage(currentForm) {
  with (currentForm) {
	WZ_storefield(CountyID, xCountyID);
	WZ_storefield(BusinessID, xBusinessID);
  }
}

// Javascript for new car forms

// Fill a select triplet make, Model, Body
// Need a matching xMake, xMode and xBody to enable remeber on back feature.
// Note: BodyType is mapped from a separate array

function init_makemodelbody(currentForm, MakeModelBodyList) {
  if (window.Option) {
	with (currentForm) {
	  WZ_fillParent(MakeModelBodyList, make, xMake.value, false);
	  if (xMake.value != "")
		WZ_fillChild(MakeModelBodyList, model, make.selectedIndex, xModel.value, false, true);
	  body_makemodelbody(currentForm, MakeModelBodyList, xBody.value);
	}
  }
}

function submit_makemodelbody(currentForm) {
  with (currentForm) {
	if (make.selectedIndex==0 && Body.selectedIndex==0) {
		alert('You are searching for a new car. Please make at least one selection in tbe new car table.');
		return false;
	} else {
		// Remember dynamic options for 'back'
		WZ_storefield(make, xMake);
		WZ_storefield(model, xModel);
		WZ_storefield(Body, xBody);
		return true;
	}
  }
  return false;
}

function body_makemodelbody(currentForm, MakeModelBodyList, bodyValue) {
  with(currentForm) {
	if (make.value != "" && model.value != "")
	  WZ_fillMappedGrandChild(MakeModelBodyList, Body, make.selectedIndex, 2, model.selectedIndex, 2, bodyValue, 1, BodyTypeList, 3);
	else
	  WZ_showField(BodyTypeList, 3, Body, bodyValue, true);
  }
}
