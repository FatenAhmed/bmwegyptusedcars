﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="wewantyourbmw.aspx.cs" Inherits="wewantyourbmw" %>

<html><head>
	<title>BMW Egypt</title>
    
	<link rel="stylesheet" href="index_files/BMW_style.css">
    
	<link href="index_files/interestform.css" rel="stylesheet" type="text/css">

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29788102-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>

<script type="text/javascript" src="index_files/flashobject.js"></script>
<!-- [42] external.bmw.ie/configurator/index.cfm -->
<script language="JavaScript">function openWin( windowURL, windowName, windowFeatures ) { return window.open( windowURL, windowName, windowFeatures ) ; 	} </script>
<!-- base href="http://external.bmw.ie/configurator/index.cfm" -->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>BMW Egypt - We Want Your BMW</title>
<link type="text/css" rel="stylesheet" href="index_files/wz_style.css">

</head>

<body>

<br><br>

<div class="maincontainer">
<br>

	
		<h1></h1>	
        
<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: WeWantYourBMW_Link_Tag
URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2011
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
</script><iframe src="index_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
<noscript>
<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=wewan046;ord=1?" width="1" height="1" frameborder="0"></iframe>
</noscript>

<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<style>
.formtbl {
	width: 500px;
	max-width: 500px;
}
.formtbl_input {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
}
.formtbl td {
	padding: 5px 0px 5px 0px;
	vertical-align: top;
	border-bottom: 1px solid #F9F9F9;
}
.frmtitle {
	padding: 5px 25px 5px 0px;
	width: 175px;
	max-width: 200px;
}
.datanote {
	padding: 5px 0px 5px 0px;
}
.frmsubmit {
	width: 300px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	border: 1px solid #999999;
	background-color: #0d456a;
	color: #FFFFFF;
	font-weight: bold;
	padding: 5px 0px 5px 0px;
	text-align: center;
}
.rightbanner {
	margin: 5px 0px 0px 0px;
}
</style>

<script>
function emailvalidation(entered, alertbox)
{
with (entered)
{
apos=value.indexOf("@");
dotpos=value.lastIndexOf(".");
lastpos=value.length-1;
if (apos<1 || dotpos-apos<2 || lastpos-dotpos>3 || lastpos-dotpos<2) 
{if (alertbox) {alert(alertbox);} return false;}
else {return true;}
}
}

function emptyvalidation(entered, alertbox)
{
with (entered)
{
if (value==null || value=="")
{if (alertbox!="") {alert(alertbox);} return false;}
else {return true;}
}
}

function formvalidation(thisform)
{
with (thisform)
{
if (emptyvalidation(per_forename,"Please enter your name.")==false) {per_forename.focus(); return false;};
if (emailvalidation(per_email,"Please enter your email.")==false) {per_email.focus(); return false;};
if (emptyvalidation(param_value1,"Please enter your current BMW.")==false) {param_value1.focus(); return false;};
if (emptyvalidation(param_value3,"Please enter your preferred means of contact")==false) {param_value3.focus(); return false;};
if (emptyvalidation(BusinessID,"Please enter your preferred Showroom.")==false) {BusinessID.focus(); return false;};
}
}
</script>



<div id="content">
<h1>We want Your BMW</h1>
<div class="headCopy">Due to recent unprecedented demand for BMW premium selection approved used cars, <strong>BMW Showrooms across the country are looking to purchase used BMW’s up to 5 years old</strong>.</div>

<form  id="contact" name="theform" runat="server">
<!--action="../contentv3/index.cfm" method="post" onSubmit="return formvalidation(this)"-->
 <!-- Pass in fuseaction -->
		  <input name="fuseaction" value="formaction" type="Hidden">
		  <!-- leadtypeID : we want your bmw  -->
		  <input name="leadtypeID" value="62" type="hidden">		
		  <input name="originatorID" value="890" type="hidden">				  
		  <!-- StatusId 1 is 'Awaiting Dealer Response' --> 
		  <input name="leadstatusID" value="1" type="Hidden">
		  
		  <input name="returnURL" value="/configurator/index.cfm?fuseaction=bmw.wewantyourbmwthanks" type="hidden">
<table class="formtbl" border="0" cellpadding="0" cellspacing="0" width="680">
  <tbody>
  <tr>
    <td class="frmtitle">Series/Model:*</td>
    <td>
    <input id="input_series" runat="server" name="per_forename" class="formtbl_input" type="text"/>
        <asp:RequiredFieldValidator ID="input_series_req" runat="server" ErrorMessage="*" ControlToValidate="input_series"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr>
    <td class="frmtitle">Year:*</td>
    <td>
    <input id="input_year" runat="server" name="per_forename" class="formtbl_input" type="text"/>
     <asp:RequiredFieldValidator ID="input_year_req" runat="server" ErrorMessage="*" ControlToValidate="input_year"></asp:RequiredFieldValidator>
    </td>
  </tr>
  <tr>
    <td class="frmtitle">Color:*</td>
    <td>
    <input  id="input_color" runat="server" name="per_forename" class="formtbl_input" type="text"/>
     <asp:RequiredFieldValidator ID="input_color_req" runat="server" ErrorMessage="*" ControlToValidate="input_color"></asp:RequiredFieldValidator>
     </td>
  </tr>
  <tr>
    <td class="frmtitle">Chassis:*</td>
    <td>
    <input  id="input_chassis" runat="server" name="per_forename" class="formtbl_input" type="text"/>
     <asp:RequiredFieldValidator ID="input_chassis_req" runat="server" ErrorMessage="*" ControlToValidate="input_chassis"></asp:RequiredFieldValidator>
     </td>
  </tr>
  <tr>
    <td class="frmtitle"><b>Contact Information</b></td>
    <td></td>
  </tr>
  <tr>
    <td class="frmtitle">Name:*</td>
    <td>
    <input  id="input_name" runat="server" name="per_forename" class="formtbl_input" type="text"/>
     <asp:RequiredFieldValidator ID="input_name_req" runat="server" ErrorMessage="*" ControlToValidate="input_name"></asp:RequiredFieldValidator>
     </td>
  </tr>
  <tr>
    <td class="frmtitle">Email:*</td>
    <td>
    <input  id="input_email" runat="server" name="per_email" class="formtbl_input" type="text"/>
     <asp:RequiredFieldValidator ID="input_email_req" runat="server" ErrorMessage="*" ControlToValidate="input_email"></asp:RequiredFieldValidator>
     </td>
  </tr>
  <tr>
    <td class="frmtitle">Phone:</td>
    <td><input id="input_phone" runat="server" name="per_workphone" class="formtbl_input" type="text"/></td>
  </tr>
  <tr>
    <td class="frmtitle">Current BMW:*</td>
	
    <td>
    <input id="input_current" runat="server" name="param_key1" value="" type="hidden"/>
    
    <input name="param_value1" class="formtbl_input" type="text"/>
        
    </td>
  </tr>
  <tr>
    <td class="frmtitle">Registration number:</td>
	<input id="input_registerno" runat="server" name="param_key2" value="" type="hidden"/>
    <td><input name="param_value2" class="formtbl_input" type="text"/></td>
  </tr>
  <tr>
    <td class="frmtitle">Preferred means of contact:*</td>
	<input id="input_means" runat="server" name="param_key3" value="Email" type="hidden"/>
    <td><select name="param_value3" class="formtbl_input">
	<option selected="selected" value="Email">Please select</option>   
	<option value="Email">Email</option>
	<option value="Phone">Phone</option></select></td>
  </tr>
  <tr>
    <td class="frmtitle">Additional Info:</td>
    <td><textarea id="input_additional" runat="server" name="track_desc" cols="6" rows="4" class="formtbl_input">&nbsp;</textarea></td>
  </tr>
  <tr>
    <td class="frmtitle">Preferred Showroom:*</td>
    <td>
    <select name="BusinessID" class="formtbl_input" id="input_dealer" runat="server">
        <option selected="selected" value="Bavarian Auto Company - Alex Branch">Please select</option>
        <option value="2278">Alexandria Showroom</option>    
        <option value="2281">Almaza Showroom</option>
        <option value="2346">Desert Road Showroom</option>
        <option value="2282">BMW Premium selection / Used car Showroom</option>
        <option value="2283">Mohandeseen Showroom</option>
        <option value="2235">Ring Road (headquarters) Showroom</option>
	</select>
    </td>
  </tr>
  <tr>
    <td colspan="2" class="datanote">We would like to keep you informed 
of our latest vehicles and other products and services that may be of 
interest, and we may occasionally ask for your assistance in market 
research to help improve our services to customers.<br><br>
            Your personal data may be shared for these purposes with 
other BMW Group companies and authorised BMW Group Showrooms. Further 
details are available in our privacy policy.<br><br>
            If you would prefer NOT to receive this information
            <br/><input name="param_key4" value="Would you prefer NOT to receive information" type="hidden"/>
            by post <input name="param_value4" id="input_nopost" runat="server" value="Do not send me information by post" type="Checkbox"/> by telephone <input name="param_value4"
              id="input_nophone" runat="server" value="Do not send me information by telephone" type="Checkbox"/> please tick the relevant box.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
   <!--  <input name="Submit" value="Request Valuation" class="frmsubmit" type="submit"/> -->
        <asp:Button ID="btn_send" runat="server" Text="Request Valuation" 
            class="frmsubmit" type="submit" onclick="btn_send_Click" />
    </td>
   
  </tr>
     <tr>
     <td>
         <asp:Label ID="lbl_result" runat="server" Text="Your Request has been sent..." Visible="false" ForeColor="Red" Font-Bold="true"></asp:Label>
         </td>
         </tr>

    
	
	<!-- Begin Breadcrumb Trail --->

	

<!-- End Breadcrumb Trail -->
	<!---->
</tbody></table></form></div></div></body></html>