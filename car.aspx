﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="car.aspx.cs" Inherits="car" %>

<html><head>
	<title>BMW Premium Selection</title>
    
    <link rel="stylesheet" href="car_files/BMW_usedcars.css">
    
	<link href="car_files/interestform.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script type="text/javascript" src="contentFrameResizer.js"></script>
<script type="text/javascript" src="iframeIntegrationLib.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-29788102-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

</script>

<script type="text/javascript" src="car_files/flashobject.js"></script><!-- [43] external.bmw.ie/usedcars/index.cfm --><!-- Load common CSS file --><link href="car_files/sausage.css" rel="stylesheet"><!-- Load common JS file --><script language="JavaScript" src="car_files/wz_functions.js"></script><!-- base href="http://external.bmw.ie/usedcars/index.cfm" --><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"></head>
<script src="js/jquery.tools.min.js"></script>

<style>
.panes div { display:none; }
.panes { margin-right:1px !important;}
.dp_details { margin-bottom:50px !important;}
</style>
<body>
  <!--
    Start of DoubleClick Floodlight Tag: Please do not remove
    Activity name of this tag: BMW_Search_Tag
    URL of the webpage where the tag is expected to be placed: http://www.bmw.ie/ie/en/usedvehicles/2010/overview.html
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 03/11/2011
    -->
    <script type="text/javascript">
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    document.write('<iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[1 Series];u2=[Coupe];u4=[2011];u3=[Tipperary];ord=' + a + '?" width="1" height="1" frameborder="0"></iframe>');
    </script><iframe src="car_files/activityi.htm" frameborder="0" height="1" width="1"></iframe>
    <noscript>
    <iframe src="http://fls.doubleclick.net/activityi;src=2821309;type=landi015;cat=bmw_s080;u1=[1 Series];u2=[Coupe];u4=[2011];u3=[Tipperary];ord=1?" width="1" height="1" frameborder="0"></iframe>
    </noscript>
    <!-- End of DoubleClick Floodlight Tag: Please do not remove -->


<div class="maincontainer">

	
   <%-- <h1>BMW 1 Series 118d Sport (2011)</h1>	--%>
   <div style="text-align:left" class="dp_imagetitle" id="Div1" runat="server">
			<strong>BMW 1 Series 118d Sport (2011)</strong>
		</div>
   
<script type="text/javascript" language="JavaScript">
function wz_swapImage2(d,s,n)
{
	
	try {eval("i_"+n)}
	catch(er) {eval("i_"+n+"=new Image();i_"+n+".src='"+s+"'")}
	document.getElementById(d).src=s
}
</script>
<script>
// perform JavaScript after the document is scriptable.
$(function() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	$("ul.tabs").tabs("div.panes > div");
});
</script>		

	<div class="dp_Pics" runat="server">
		
	
		 
		   <div class="panes" runat="server" style="margin-right:1px;">
            <div>		
            	<img id="bigimg_1" class="dp_PicLarge" name="bigpic" title="BMW  Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
            <div>		
            	<img id="bigimg_2" class="dp_PicLarge" name="bigpic" title="BMW 1 Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
            <div>		
            	<img id="bigimg_3" class="dp_PicLarge" name="bigpic" title="BMW 1 Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
            <div>		
            	<img id="bigimg_4" class="dp_PicLarge" name="bigpic" title="BMW 1 Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
            <div>		
            	<img id="bigimg_5" class="dp_PicLarge" name="bigpic" title="BMW 1 Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
            <div>		
            	<img id="bigimg_6" class="dp_PicLarge" name="bigpic" title="BMW 1 Series 118d Sport (2011)" alt="BMW 1 Series 118d Sport (2011)" runat="server">
            </div>
        </div>
		
		
			<div class="dp_PicThumbs" runat="server">
			

			
			 <ul class="tabs">
                <li><a href="#"><img id="li_img1" class="dp_PicThumb" width="136" runat="server"/></a></li>
                <li><a href="#"><img id="li_img2" class="dp_PicThumb" width="136" runat="server"/></a></li>
                <li><a href="#"><img id="li_img3" class="dp_PicThumb" width="136" runat="server"/></a></li>
                <li><a href="#"><img id="li_img4" class="dp_PicThumb" width="136" runat="server"/></a></li>
                <li><a href="#"><img id="li_img5" class="dp_PicThumb" width="136" runat="server"/></a></li>
                <li><a href="#"><img id="li_img6" class="dp_PicThumb" width="136" runat="server"/></a></li>
            </ul>
			
			</div>
			
		
		
		<div style="clear:both;text-align:center" class="dp_imagetitle" id="label_strong" runat="server">
			<strong>BMW 1 Series 118d Sport (2011)</strong>
		</div>
		
			
	</div>
   
 

		
		


<div class="dp_details">



	


<div class="dp_row1">

	<div class="dp_pair">
		<div class="dp_label">
			Model:
		</div>
		<div class="dp_data" id="txt_model" runat="server">
			
		</div>
	</div>

</div>
		

	


<div class="dp_row2">

	<div class="dp_pair">
		<div class="dp_label">
			Engine:
		</div>
		<div class="dp_data" id="txt_engine" runat="server">
		</div>
	</div>

</div>
		

	


<div class="dp_row1">

	<div class="dp_pair">
		<div class="dp_label">
			Price:
		</div>
		<div class="dp_data" id="txt_price" runat="server">
		</div>
	</div>

</div>
		

	


<div class="dp_row2">

	<div class="dp_pair">
		<div class="dp_label">
			Location:
		</div>
		<div class="dp_data" id="txt_dealer" runat="server">
		
		Bavarian Auto Center,<br/>
Katameya Investment Zone Area # 1,<br/>
Zone # 10a Katameya Ring-Road Direction Maadi – Heliopolis 1800 meter from City Center Mall – Carrfour Katameya,<br />
Cairo - Egypt.

			<br />
		
            Tel: +2 - 272 - 722 41
			
					<br />
					Fax: +2 – 272 – 72249
						<br />		
		</div>
	</div>

</div>
		

	


<div class="dp_row1">

	<div class="dp_pair">
		<div class="dp_label">
			Year:
		</div>
		<div class="dp_data" id="txt_year" runat="server">
		</div>
	</div>

</div>
		

	


<div class="dp_row2">

	<div class="dp_pair">
		<div class="dp_label">
			Exterior-Interior color:
		</div>
		<div class="dp_data" id="txt_color" runat="server">
		</div>
	</div>

</div>
		

	




<div class="dp_row1">

	<div class="dp_pair">
		<div class="dp_label">
			Chassis no.:
		</div>
		<div class="dp_data" id="input_chassis" runat="server">
		</div>
	</div>

</div>
		
<div class="dp_row2">

	<div class="dp_pair">
		<div class="dp_label">
			Milage:
		</div>
		<div class="dp_data" id="input_milage" runat="server">
		</div>
	</div>

</div>
		
	


<div class="dp_row1">

	<div class="dp_pair">
		<div class="dp_label">
			Doors:
		</div>
		<div class="dp_data" id="txt_doors" runat="server">
		</div>
	</div>

</div>
		

	


<div class="dp_row2">

	<div class="dp_pair">
		<div class="dp_label">
			Comments:
		</div>
		<div class="dp_data" id="div_specs" runat="server">
			Coral Red Boston leather upholstery, 18'' W-spoke style 263 alloy 
wheels, Basic Service Incl Pack, Black trim. high-gloss, BMW 
Professional radio with single CD, Exterior trim. High-gloss Shadowline,
 Headlining. Anthracite, Leather interior, Park Distance Control (PDC). 
rear, Run-flat tyres, Sun protection glass, USB audio interface, 
Multi-function controls for s'wheel, Sport Steering Wheel, Run-flat 
tyres, Exterior parts in body colour, Floor mats. velour, ISOFIX Child 
Seat System, Sport seats. front, Foglights. front, Air Conditioning, 
First Aid Kit &amp; Triangle, Head restraints. rear and folding.
		</div>
	</div>

</div>
		

	


		


</div>
<!--
 <div class="dp_links">
 <ul>

	<li><a href="#">See other cars from BMW</a></li>

	<li><a href="#">See other BMW 1 Series</a></li>

	<li><a href="#">See other cars in this price range (+/- 5%)</a></li>

</ul>
</div>
-->

</form>
</div>
                        
</body></html>
<!---->