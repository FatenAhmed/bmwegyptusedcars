﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class car : System.Web.UI.Page
{
    string id;
    DataSet Ds;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            id = Request.QueryString["Id"];
            string[] ParamsArray = { "@Id"};
            string[] valuesArray = { id};

            Database datab = new Database();

            Ds = datab.doStoredProcedure(valuesArray, ParamsArray, "Get-CarDetails");

            if (Ds.Tables.Count > 0)
            {
            /*    img_big.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_0_b.jpg";
                a1.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_0_b.jpg";
                a2.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_1_b.jpg";
                a3.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_2_b.jpg";
                a4.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_3_b.jpg";
                a5.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_4_b.jpg";
                a6.Attributes["href"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Model"] + "_5_b.jpg";*/

                bigimg_1.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_0_b.jpg";
                bigimg_1.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_1.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_2.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_1_b.jpg";
                bigimg_2.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_2.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_3.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_b.jpg";
                bigimg_3.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_3.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_4.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_b.jpg";
                bigimg_4.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_4.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_5.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_b.jpg";
                bigimg_5.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_5.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_6.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_b.jpg";
                bigimg_6.Attributes["title"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];
                bigimg_6.Attributes["alt"] = Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"];

                li_img1.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_0_t.jpg";
                li_img2.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_1_t.jpg";
                li_img3.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_2_t.jpg";
                li_img4.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_3_t.jpg";
                li_img5.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_4_t.jpg";
                li_img6.Attributes["src"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["ChassisNO"] + "_5_t.jpg";
                li_img1.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img2.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img3.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img4.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img5.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];
                li_img6.Attributes["alt"] = "~/MyImages/" + Ds.Tables[0].Rows[0]["Series"];

                if (Ds.Tables[0].Rows[0]["Model"].ToString() == "Sport")
                    txt_model.InnerText = "X3 Sport";
                else if (Ds.Tables[0].Rows[0]["Model"].ToString() == " ")
                    txt_model.InnerText = "X1";
                else 
                txt_model.InnerText = Ds.Tables[0].Rows[0]["Model"].ToString();


                if (Ds.Tables[0].Rows[0]["EngineSize"].ToString() == "2000")
                    txt_engine.InnerText = "2.0 L";
                else
                    txt_engine.InnerText = Ds.Tables[0].Rows[0]["EngineSize"].ToString().ToUpper(); ;

                Div1.InnerHtml = "<strong>" +
                    Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"]
                    + "</strong>";
                label_strong.InnerHtml = "<strong>" + 
                    Ds.Tables[0].Rows[0]["Series"] + " " + Ds.Tables[0].Rows[0]["Model"] + " " + Ds.Tables[0].Rows[0]["IYear"]
                    + "</strong>";
                txt_price.InnerText = Ds.Tables[0].Rows[0]["Price"].ToString() + " EGP";
                txt_year.InnerText = Ds.Tables[0].Rows[0]["IYear"].ToString();
                txt_color.InnerText = Ds.Tables[0].Rows[0]["Color"].ToString();
                txt_doors.InnerText = Ds.Tables[0].Rows[0]["DoorsNumber"].ToString();
                div_specs.InnerHtml = Ds.Tables[0].Rows[0]["Specs"].ToString();
                input_chassis.InnerText = Ds.Tables[0].Rows[0]["ChassisNO"].ToString();
            }
        }
    }
}
